= അന്ധത =

Blindness


വസ്തുക്കളുടെ നിറം, സ്വഭാവം, ആകൃതി മുതലായവ കാണാന്‍ കഴിയാതിരിക്കുന്ന അവസ്ഥ. അന്ധത പ്രധാനമായും രണ്ടുതരത്തിലുണ്ട്: ജന്മസിദ്ധ വൈകല്യങ്ങള്‍ ഉളവാക്കുന്ന അന്ധതയും ജനനാന്തരം ഉണ്ടാകുന്ന അന്ധതയും. ഗ്ളോക്കോമ, തിമിരം, ദൃഷ്ടിപടല(retina)ത്തിനുണ്ടാകുന്ന രോഗങ്ങള്‍ എന്നിവയാണ് അന്ധതയുടെ പ്രധാനകാരണങ്ങള്‍. 


== ജന്മസിദ്ധവൈകല്യങ്ങള്‍ ഉളവാക്കുന്ന അന്ധത ==

ഗര്‍ഭത്തിന്റെ ആദ്യത്തെ മൂന്നു മാസങ്ങളില്‍ മാതാവിനെ ബാധിക്കുന്ന ജര്‍മന്‍ മീസില്‍സ് അഥവാ റുബെല്ല ശിശുവിന്റെ നേത്രകാചം അതാര്യമാക്കി അന്ധതയുളവാക്കുന്നു.

വിവിധ ഉപാപചയ തകരാറുകളും നേത്രരൂപീകരണത്തിലെ വൈകല്യങ്ങളും ജന്മസിദ്ധ അന്ധതയ്ക്ക് കാരണമാകാറുണ്ട്.

=== തിമിരം ===

പ്രമേഹരോഗികളായ ചില സ്ത്രീകളുടെ കുട്ടികളില്‍ ജന്മനാ തിമിരം ഉള്ളതായി കണ്ടുവരുന്നു. ഇത്തരത്തിലുള്ള അന്ധത ശസ്ത്രക്രിയമൂലം മാറ്റാവുന്നതാണ്.

=== റെറ്റിനോബ്ളാസ്റ്റോമ ===

Retinoblastoma

ക്രോമസോമല്‍ വൈകല്യം മൂലം ശിശുക്കളില്‍ ഉണ്ടാകുന്ന ഈ രോഗത്തിനു കാരണം ദൃഷ്ടി പടലത്തിന്റെ മസ്തിഷ്കാനുബന്ധ കലകളില്‍ (neuroglia) ഉണ്ടാകുന്ന ട്യൂമറാണ്. ജനിക്കുമ്പോള്‍തന്നെ ഈ രോഗം ഉണ്ടെങ്കില്‍ പോലും രണ്ടു വയസ്സാകുന്നതോടുകൂടി മാത്രമേ രോഗലക്ഷണങ്ങള്‍ പ്രകടമാകുന്നുള്ളു. മിക്കവാറും ഒരു കണ്ണിലേ രോഗം ഉണ്ടാകാറുള്ളു. രോഗിയുടെ ശ്വേതമണ്ഡലം (കോര്‍ണിയ) വലുതായിവരികയും അത് പൂച്ചയുടെ കണ്ണുപോലെ മഞ്ഞനിറമാവുകയും തുടര്‍ന്ന് കാഴ്ച നഷ്ടപ്പെടുകയും ചെയ്യുന്നു. കണ്ണിനകത്ത് വലിവ് വര്‍ധിക്കുന്നതിന്റെ ഫലമായി കോര്‍ണിയ ഉന്തിവരുന്നതിന് ബുഫ്താല്‍മോസ് (Buphthalmos) എന്നു പറയുന്നു. കാളയുടെ കണ്ണുപോലെ തോന്നിക്കുന്നതിനാല്‍ ഇതിനെ ഓക്സ്-ഐ (Ox-Eye) എന്നും വിളിക്കാറുണ്ട്. രോഗം ബാധിച്ച കണ്ണ് ശസ്ത്രക്രിയമൂലം എടുത്തുകളഞ്ഞശേഷം എക്സ്റേ-തെറാപ്പി നടത്താവുന്നതാണ്. വ്യാധി തലച്ചോറിനകത്തേക്കു വ്യാപിക്കുന്നതിനാല്‍ ഒരു കണ്ണ് എടുത്തുകളഞ്ഞാലും വീണ്ടും മറ്റേ കണ്ണില്‍ രോഗം ബാധിക്കാനിടയുണ്ട്. നാലുവര്‍ഷത്തിനകം ഈ രോഗം വീണ്ടും വരാതിരുന്നാല്‍ രോഗം മാറിയെന്ന് അനുമാനിക്കാം. ഗുരുതരമായി ഈ രോഗം ബാധിച്ചാല്‍ സാധാരണഗതിയില്‍ ഒരു വര്‍ഷം തികയുന്നതിനുമുന്‍പ് രോഗി മരിക്കാനിടയുണ്ട്.

=== റിട്രോലെന്റല്‍ ഫൈബ്രോപ്ളാസിയ ===

Retrolental Fibroplasia

കണ്ണിനകത്തെ കാചത്തിന്റെ (lens) പിന്‍ഭാഗത്ത് പോറലുകള്‍ വീണ് കാഴ്ച നഷ്ടപ്പെടുന്ന രോഗം. അകാലജനിത (premature) ശിശുക്കള്‍ക്ക് അനിയന്ത്രിതമായി ഓക്സിജന്‍ കൊടുക്കുന്നതുകൊണ്ട് അവരുടെ കണ്ണുകളില്‍ ഈ രോഗമുണ്ടാകുന്നു. ഇങ്ങനെയുള്ള ശിശുക്കളില്‍, ജനിച്ച് 1&frac12; മാസത്തിനുള്ളില്‍ കണ്ണിനകത്തെ രക്തക്കുഴലുകള്‍ വലുതായി വരുന്നു.

=== അണുബാധ ===

മാതാവിന് സിഫിലിസ്, ഗൊണോറിയ എന്നീ രോഗങ്ങളുണ്ടെങ്കില്‍ ശിശുവിനും ഈ രോഗങ്ങള്‍ ഉണ്ടാകും. ജന്മനാ ഉണ്ടാകുന്ന സിഫിലിസിനോടനുബന്ധിച്ച് ശ്വേതമണ്ഡലത്തില്‍ അണുബാധയുണ്ടാകും. അതു ബാധിച്ച് കണ്ണില്‍ ചുവപ്പും പഴുപ്പും വരികയും കാഴ്ച നഷ്ടപ്പെടുകയും ചെയ്യും. ഗര്‍ഭകാലത്ത് അമ്മയ്ക്ക് ഈ രോഗങ്ങള്‍ക്കുള്ള ചികിത്സ ചെയ്താല്‍ ഈ അസുഖം ഒഴിവാക്കാനാവും. കുട്ടികളിലുള്ള ചികിത്സ സാധാരണ ഫലപ്രദമല്ല.

== ജനനാനന്തരം ഉണ്ടാകുന്ന അന്ധത ==

ജന്മനാ സിഫിലിസ് ബാധിച്ചിരിക്കുന്ന ചില കുട്ടികള്‍ക്ക് പത്തുപന്ത്രണ്ടു വയസ്സാകുമ്പോള്‍ കോര്‍ണിയയുടെ അകത്തു വെള്ളനിറം വരുന്നതിന് ഇന്റര്‍സ്റ്റീഷ്യല്‍ കെരറ്റൈറ്റിസ് എന്നു പറയുന്നു. പെനിസിലിന്‍ കുത്തിവയ്ക്കുന്നതുകൊണ്ട് രോഗശാന്തിയുണ്ടാകും. കോര്‍ണിയല്‍ ഗ്രാഫ്റ്റിങ് ചെയ്യുന്നപക്ഷം കാഴ്ച വീണ്ടെടുക്കുകയും ചെയ്യാം.

പോഷകാഹാരങ്ങളുടെ അഭാവത്താലും അന്ധതയുണ്ടാകാറുണ്ട്. രാത്രി കാഴ്ചയുണ്ടായിരിക്കാന്‍ ജീവകം-എ വളരെ അത്യാവശ്യമാണ്. കണ്ണിന്റെ ശ്വേതമണ്ഡലത്തില്‍ ശല്കങ്ങള്‍ ഉണ്ടാകുന്നതിന് കെരറ്റോ മലേഷ്യ എന്നും അതു കോര്‍ണിയയിലേക്കു വ്യാപിക്കുമ്പോള്‍ അവയെ ബിറ്റോട്സ് സ്പോട്സ് എന്നും പറയുന്നു. മുലകുടി മാറുന്നതോടെ മുലപ്പാലിനുപകരം ധാരാളം പശുവിന്‍പാല്‍ കൊടുക്കാതെ പകരം കപ്പ, ചോറ് മുതലായവ മാത്രം ആഹാരമായി കൊടുക്കുമ്പോള്‍ ജീവകം-എ വേണ്ടത്ര ലഭിക്കാതെ വരികയും തന്‍മൂലം കാഴ്ച നഷ്ടപ്പെടുകയും ചെയ്യുന്നു. പാല്‍, മുട്ട, മീനെണ്ണ മുതലായവ ആഹാരമായി കൊടുത്താല്‍ ആരംഭത്തില്‍ തന്നെ ഈ രോഗം തടയാം.

===  മസൂരിരോഗം ===

ഉഷ്ണമേഖലാരാജ്യങ്ങളില്‍ അന്ധതയ്ക്കുള്ള ഒരു പ്രധാനകാരണം മസൂരിരോഗമാണ്. മസൂരിരോഗം മൂലം കോര്‍ണിയയിലും കുമിളകള്‍ ഉണ്ടാകുന്നു. തത്ഫലമായി കോര്‍ണിയയുടെ തൊലി ഇളകിപ്പോകുന്നു; സുതാര്യത നഷ്ടപ്പെടുകയും കാഴ്ചയ്ക്ക് തകരാറു സംഭവിക്കുകയും ചെയ്യും. പകര്‍ച്ചവ്യാധി ഉണ്ടാകാതിരിക്കാനുള്ള കുത്തിവയ്പ് എടുക്കുന്നതാണിതിനുള്ള പ്രതിവിധി. മസൂരിരോഗനിര്‍മാര്‍ജന പദ്ധതിമൂലം ഇന്ന് ഇത്തരം അന്ധത വളരെ വിരളമായിട്ടുണ്ട്.

=== ട്രക്കോമ ===

കണ്‍പോളകള്‍, ശ്വേതമണ്ഡലം, നേത്രവൃതി എന്നിവയിലുണ്ടാകുന്ന വൈറല്‍ ബാധ. ക്ളമീഡിയ ട്രക്കോമാറ്റിസ് (Chlamydia Trachomatis) എന്ന വൈറസാണ് രോഗകാരണം. കണ്‍പോളയ്ക്കകത്തും നേത്രവൃതിയിലും ധാരാളം കുരുക്കളുണ്ടാകുന്നു. കണ്ണിനകത്തെ രക്തക്കുഴലുകള്‍ ചുവന്നു തടിച്ചുവരികയും കണ്ണിനു വലിയ വേദന അനുഭവപ്പെടുകയും ചെയ്യും. സള്‍ഫോണാമൈഡ് ചികിത്സയാണ് പ്രതിവിധി. രോഗശുശ്രൂഷയുടെയും ചികിത്സയുടെയും അഭാവത്തില്‍ ഇത് അന്ധതയ്ക്ക് കാരണമാകും. ജനങ്ങള്‍ തിങ്ങിപ്പാര്‍ക്കുന്നതും ജലദൌര്‍ലഭ്യമുള്ളതുമായ സ്ഥലങ്ങളിലാണ് ഈ രോഗം അധികവും കണ്ടുവരുന്നത്. നോ: ട്രക്കോമ

=== രക്തസമ്മര്‍ദം ===

അതിരക്തസമ്മര്‍ദം മൂലം ദൃഷ്ടിപടലത്തിനു ക്ഷതമേല്‍ക്കാനും കണ്ണിനകത്തെ രക്തക്കുഴലുകള്‍ പൊട്ടാനും സാധ്യതയുണ്ട്. തത്ഫലമായി ചിലപ്പോള്‍ കാഴ്ച നിശ്ശേഷം ഇല്ലാതായിത്തീരുന്നു.

=== തിമിരം ===

നേത്രകാചം അതാര്യമാകുന്ന അവസ്ഥയാണിത്. മധ്യവയസ്സാകുന്നതോടെ കാചത്തിന്റെ സുതാര്യത പല കാരണങ്ങളാലും നഷ്ടപ്പെടാം. അതുകൊണ്ട് കാഴ്ചയ്ക്കു മാന്ദ്യം സംഭവിക്കുന്നു. ശരീരത്തിന്റെ ആരോഗ്യം അനുസരിച്ച് കാഴ്ചക്കുറവില്‍ ഏറ്റക്കുറച്ചിലുണ്ടാകുന്നു. തിമിരം പൂര്‍ണമാകുമ്പോള്‍ കാചം ശസ്ത്രക്രിയമൂലം മാറ്റിയാല്‍ കാഴ്ചവീണ്ടും ലഭിക്കുന്നതാണ്. പ്രമേഹബാധയുള്ളവരുടെ കണ്ണുകള്‍ക്കാണ് തിമിരം വേഗത്തില്‍ ബാധിക്കുന്നത്. മദ്യം, പുകയില, ചായ, കാപ്പി എന്നീ പദാര്‍ഥങ്ങള്‍ ധാരാളമായി ഉപയോഗിക്കുന്നതിന്റെ ഫലമായും കാഴ്ച നഷ്ടപ്പെടാം.

=== ഗ്ളോക്കോമ ===

Glaucoma 

അന്ധതയുടെ ഒരു പ്രധാന കാരണം ഗ്ളോക്കോമ എന്ന രോഗമാണ്. കണ്ണിനകത്തെ മര്‍ദം നിയന്ത്രിച്ചു നിര്‍ത്തുന്നത് നേത്രോദ (aqueous humour) ത്തിന്റെ മര്‍ദമാണ്. നേത്രോദത്തിലുണ്ടാകുന്ന മര്‍ദവര്‍ധനവിനനുസൃതമായി നേത്രമജ്ജ(vitreous humour)യിലും മര്‍ദ വര്‍ധനവുണ്ടാകുന്നു. തത്ഫലമായി ദൃഷ്ടി പടലത്തിനും നേത്രനാഡി (optic nerve) ക്കും ക്ഷതം ഉണ്ടാകുകയും കാഴ്ചശക്തി കുറയുകയും ചെയ്യുന്നു. തീവ്ര ഗ്ളോക്കോമ (acute glaucoma) പെട്ടന്നുണ്ടാകുന്ന ഒരു സ്ഥിതിവിശേഷമാണ്. തീക്ഷ്ണമായ വേദനയോടൊപ്പം കാഴ്ചയ്ക്ക് മങ്ങലും അനുഭവപ്പെടുന്നു. പ്രകാശത്തെ വലയം ചെയ്ത് മഴവില്‍ വര്‍ണങ്ങള്‍ കാണുന്നതായും തോന്നും. അടിയന്തിര ചികിത്സ ലഭിക്കാതിരുന്നാല്‍ അന്ധത ബാധിക്കാനിടയുണ്ട്.

കാഴ്ച ക്രമേണ കുറഞ്ഞുവരുന്ന ക്രോണിക് ഗ്ളോക്കോമയ്ക്കു തുടക്കത്തില്‍ വലിയ വേദന ഉണ്ടാകുകയില്ല. കണ്ണട ഉപയോഗിക്കുന്നവര്‍ക്ക് ഇടയ്ക്കിടയ്ക്കു കണ്ണട പുതുക്കേണ്ടിവരും. ക്രമേണ പാര്‍ശ്വവീക്ഷണം ഇല്ലാതായി കാഴ്ച ഒരു കുഴലില്‍കൂടി നോക്കിയാലുള്ള രൂപത്തില്‍ കുറയുന്നു. കണ്ണിന്റെ വലിവ് വര്‍ധിക്കുകയും കാഴ്ച വീണ്ടും കുറയുകയും ചെയ്യും. രോഗം നേരത്തെ കണ്ടുപിടിച്ചാല്‍ പ്രത്യേകതരം ശസ്ത്രക്രിയകള്‍കൊണ്ട് രോഗത്തിനു കുറെയൊക്കെ ശമനമുണ്ടാക്കാം.

=== ദൃഷ്ടിപടല വിയോജനം ===

Retinal detachment

പെട്ടെന്നുള്ള അന്ധതയ്ക്ക് മറ്റൊരു കാരണമാണിത്. കൊറോയ്ഡ് (choroid) എന്ന ആവരണത്തില്‍ നിന്നു ദൃഷ്ടിപടലം വേര്‍പ്പെട്ടു പോകുന്നു. തലയ്ക്കോ കണ്ണിനോ പെട്ടെന്നുണ്ടാകുന്ന ആഘാതം മൂലമോ ഹ്രസ്വദൃഷ്ടി മൂലമോ ഇത് സംഭവിക്കാം. ശസ്ത്രക്രിയയാണ് പ്രതിവിധി.

=== മെലനോമ ===

മെലനോമ എന്ന മാരകമായ അര്‍ബുദം കണ്ണിനകത്ത് ഉണ്ടാകാറുണ്ട്. കണ്ണിനകത്തെ വിവിധ ഭാഗങ്ങളായ കോറോയ്ഡ്, സീലിയറി ബോഡി, ഐറിസ് എന്നിവിടങ്ങളില്‍ ഈ രോഗം ഉണ്ടാകാം. ഒരു മൊട്ടുപോലെ ആരംഭിക്കുന്ന അര്‍ബുദം ഒരു ചെറിയ കൂണുപോലെ വളര്‍ന്ന് ദൃഷ്ടിപടല വിയോജനം ഉണ്ടാകുന്നു.

=== ധമനീവൈകല്യങ്ങള്‍ ===

കണ്ണുകളിലെ രക്തക്കുഴലുകളുടെ അന്യൂറിസം, കവേര്‍ണസ് സൈനസ് ത്രോംബോസിസ് (Cavernous sinus thrombosis) എന്നീ ധമനീവൈകല്യങ്ങള്‍ ഉണ്ടാകുമ്പോള്‍ കണ്ണ് അടയ്ക്കാന്‍ കഴിയാത്തനിലയില്‍ പുറത്തേക്കു തള്ളിനില്ക്കുന്നു. കണ്ണിനു വലിയ വേദനയും വലിവും അനുഭവപ്പെടുകയും കാഴ്ച നഷ്ടപ്പെടുകയും ചെയ്യും. കണ്ണിലെ സിരകളെയോ കരോട്ടിഡ് ധമനിയേയോ കെട്ടിവയ്ക്കുന്ന ശസ്ത്രക്രിയകൊണ്ട് രോഗത്തിന്റെ കാഠിന്യം കുറയ്ക്കാവുന്നതാണ്.

=== സിംപതെറ്റിക് ഒഫ്താല്‍മിയ ===

നേത്രകാചത്തിന് ക്ഷതം ഉണ്ടാകുന്നതിന്റെ ഫലമായി കാചത്തിനകത്തെ പ്രോട്ടീന്‍ പുറത്തുപോകുന്നു. ക്ഷതം കൊണ്ട് കാചത്തിലെ പ്രോട്ടീന്‍ പുറത്തുവന്നു രോഗമില്ലാത്ത കണ്ണിനകത്തു പ്രതിപ്രവര്‍ത്തനം ഉണ്ടാക്കുന്നു. ഇതിന്റെ ഫലമായി രണ്ടു കണ്ണിലേയും കാഴ്ച നഷ്ടപ്പെടുന്നു. ഇതിന് സിംപതെറ്റിക് ഒഫ്താല്‍മിയ (Sympathetic Ophthalmia) എന്നു പറയും.

ക്ഷതം പറ്റുന്നതിന്റെ ഫലമായി കണ്ണിന്റെ അകത്തുള്ള ഭാഗങ്ങള്‍ക്കും വിവിധ കലകള്‍ക്കും ശോഥം സംഭവിക്കുക പതിവാണ്. തത്ഫലമായി അന്ധത ഉണ്ടാകാവുന്നതാണ്. ശസ്ത്രക്രിയവഴി ആ കണ്ണ് എടുത്തുമാറ്റാത്തപക്ഷം ഈ സ്ഥിതിവിശേഷം മറ്റേ കണ്ണിലേക്കുകൂടി പടരുവാനും കൂടുതല്‍ ഗുരുതരമായ ഫലങ്ങള്‍ ഉളവാക്കുവാനും ഇടയുണ്ട്.

=== അപകടങ്ങള്‍ മൂലമുണ്ടാകുന്ന ക്ഷതങ്ങള്‍ ===

മൂര്‍ച്ചയുള്ള വസ്തുക്കള്‍ കൊണ്ട് കണ്ണിന് അപകടങ്ങള്‍ ഉണ്ടാകാറുണ്ട്. ചില ഉപകരണങ്ങള്‍ കൈകാര്യം ചെയ്യുമ്പോള്‍ അശ്രദ്ധ മൂലം കണ്ണിനകത്ത് കുത്തി മുറിവേല്‍ക്കാനിടയുണ്ട്. നെല്ലു കുത്തുകാര്‍ക്കിടയില്‍ നെല്‍ക്കതിര്‍ കണ്ണിനകത്ത് തുളച്ചു കയറി അപകടം ഉണ്ടാകുന്നത് സാധാരണമാണ്. ഇങ്ങനെയുണ്ടാകുന്ന മുറിവുകളില്‍ അണുബാധയുണ്ടായി കണ്ണിന്റെ കാഴ്ച നഷ്ടപ്പെടാനിടയുണ്ട്. ഇതിനുള്ള ചികിത്സ വൈകുന്തോറും കാഴ്ച കിട്ടാനുള്ള സാധ്യത കുറയുന്നു. 

== വര്‍ണാന്ധത ==

Color blindness 

ചുവപ്പ്, മഞ്ഞ, നീല എന്നീ നിറങ്ങള്‍ തിരിച്ചറിയുവാന്‍ കഴിയാതെ വരുന്ന അവസ്ഥയാണിത്. കോണു(cone)കളുടെ പ്രവര്‍ത്തനംകൊണ്ടാണ്  വര്‍ണങ്ങളെ തിരിച്ചറിയാന്‍ സാധിക്കുന്നത്.  ചുവപ്പ്, പച്ച, നീല നിറങ്ങള്‍, കോണ്‍ കോശങ്ങളിലെ മൂന്നു വ്യത്യസ്ത വര്‍ണകങ്ങളെ പ്രതിപ്രവര്‍ത്തനത്തിനു വിധേയമാക്കുന്നതാണ് വര്‍ണക്കാഴ്ച ലഭ്യമാക്കുന്നത്. ജന്മനാ ഈ വര്‍ണകങ്ങള്‍ ഇല്ലാതെ വരികയോ ഏതെങ്കിലും വിധത്തില്‍ ഇവയ്ക്ക് വൈകല്യങ്ങള്‍ ഉണ്ടാവുകയോ ചെയ്യുന്നതിന്റെ ഫലമാണ് വര്‍ണാന്ധത.

ചുവപ്പും പച്ചയും തിരിച്ചറിയാതിരിക്കുന്ന അവസ്ഥയാണ് അധികമായി കണ്ടുവരുന്നത്. ഇതിനെ ശോണ-ഹരിതാന്ധത (Red-green Blindness) എന്നു പറയുന്നു. നീലയും മഞ്ഞയും തിരിച്ചറിയാതിരിക്കുന്ന അവസ്ഥയുമുണ്ട്. പല വര്‍ണങ്ങളിലുള്ള അനേകം കാര്‍ഡുകളില്‍ നിന്ന് ചുവപ്പു കാര്‍ഡും പച്ചക്കാര്‍ഡും തിരഞ്ഞെടുപ്പിച്ചാണ് (Ishihara's test) വര്‍ണാന്ധത ഉണ്ടോ ഇല്ലയോ എന്നു പരിശോധിക്കുന്നത്.

പുരുഷന്മാരില്‍ ആണ് അധികവും വര്‍ണാന്ധത കണ്ടുവരുന്നത്. സ്ത്രീകളില്‍ വളരെ വിരളമായേ ഇതു കണ്ടുവരുന്നുള്ളു. വര്‍ണാന്ധത ലിംഗസഹലഗ്നം (sex-linked) ആണെന്നു കരുതപ്പെടുന്നു.


(ഡോ. നളിനി വാസു, സ.പ.)
[[category:വൈദ്യശാസ്ത്രം]]