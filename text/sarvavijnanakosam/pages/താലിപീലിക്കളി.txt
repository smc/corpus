=താലിപീലിക്കളി=
കേരളത്തിലെ ഒരു നാടന്‍കളി. പെണ്‍കുട്ടികളാണ് ഇതു കളിക്കുന്നത്. ഒരു സംഘം വണിക്കുകള്‍ ഒരു വീട്ടില്‍ച്ചെന്നു പെണ്ണുചോദിക്കുന്നതും അവര്‍ക്കു പെണ്ണു കൊടുക്കാന്‍ വീട്ടുകാര്‍ വിസമ്മതിക്കുന്നതും ഒടുവില്‍ വണിക്കുകള്‍ പെണ്ണിനെ തട്ടിക്കൊണ്ടു പോകുന്നതും ഇതിവൃത്തമായുള്ള ഒരു പാട്ടുപാടിയാണ് ഈ കളി നടത്തുന്നത്. രണ്ടു വരിയായി കുട്ടികള്‍ നിന്നശേഷം കളി ആരംഭിക്കുന്നു. വരന്റെ സംഘം പരസ്പരം കൈകോര്‍ത്തു പിടിച്ച് വധൂ സംഘത്തിനടുത്തേക്ക് നീങ്ങിക്കൊണ്ട് ഇങ്ങനെ പാടുന്നു.

'താലിപീലി പെണ്ണുണ്ടോ

ചെറുതാമരയാളേ പെണ്ണുണ്ടോ-'

തുടര്‍ന്ന് വരന്റെ സംഘം പല വാഗ്ദാനങ്ങള്‍ നല്കുന്നു. അത്  ഇങ്ങനെ ആരംഭിക്കുന്നു.

'ഒരാനേം തോട്ടീം തരാം

പെണ്ണിനെ തര്വോ ......'

അപ്പോള്‍ വധൂസംഘം ഇങ്ങനെ തിരിച്ചു പാടുന്നു.

'ഒരാനേം തോട്ടീം തന്നാല്‍ 

പെണ്ണിനെത്തരില്ല വാണിഭരേ.....'

അങ്ങനെ 'പത്താനേം തോട്ടീം തരാം' എന്നുവരെ ചോദ്യോത്തരം നീളുന്നു. ഒടുക്കം- 

'അടുക്കളേല്‍ കടക്കും

കലം തട്ടിയുടയ്ക്കും

പെണ്ണിനെ പിടിക്കും'

എന്നു പാടി വരന്റെ സംഘം മുന്നോട്ടു നീങ്ങി പെണ്ണിനെ തട്ടിയെടുക്കുന്നതോടെ കളി അവസാനിക്കുന്നു. ഓണം, തിരുവാതിര പോലുള്ള ഉത്സവവേളകളില്‍ പെണ്‍കുട്ടികള്‍ ഉല്ലാസഭരിതരായി നടത്തിപ്പോന്ന ഈ കളി ഇപ്പോള്‍ കേരളത്തില്‍ നാമാവശേഷമായിക്കഴിഞ്ഞിരിക്കുന്നു.