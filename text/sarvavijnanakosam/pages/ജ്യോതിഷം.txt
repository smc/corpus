==ജ്യോതിഷം==

===Astrology===

ഗ്രഹനില അനുസരിച്ച് ജീവിതത്തിലെ ഭൂത-വര്‍ത്തമാന-ഭാവി ഫലങ്ങള്‍ പ്രവചിക്കുന്ന വിജ്ഞാനശാഖ. നക്ഷത്രമായ സൂര്യന്‍, ഭൂമിയുടെ ഉപഗ്രഹമായ ചന്ദ്രന്‍, കുജന്‍ (ചൊവ്വ-Mars), ബുധന്‍ (Mercury), ഗുരു (വ്യാഴം-Jupiter), ശുക്രന്‍ (Venus), ശനി (മന്ദന്‍-Saturn) എന്നീ ഗ്രഹങ്ങളെയും (planets) രാഹു, കേതു മുതലായ ചില സാങ്കല്പിക ബിന്ദുക്കളെയും ചേര്‍ത്ത് ജ്യോതിഷത്തില്‍ നവഗ്രഹങ്ങള്‍ എന്നു പറയുന്നു. ഭൂമിയെ കേന്ദ്രബിന്ദുവാക്കി നോക്കുമ്പോള്‍ ഈ ഗ്രഹങ്ങളുടെ ആപേക്ഷിക നിലയ്ക്കാണ് ഗ്രഹനില എന്നു പറയുക.

ശാസ്ത്രവും ഐതിഹ്യങ്ങളും പുരാണങ്ങളും അന്ധവിശ്വാസങ്ങളും മറ്റും ഇന്ന് ജ്യോതിഷത്തില്‍ ബന്ധപ്പെട്ടുകാണുന്നുണ്ടെങ്കിലും ഇതില്‍ ശാസ്ത്രീയ സത്യം അന്തര്‍ഭവിച്ചിട്ടുണ്ടെന്ന്  ചിലര്‍ വിശ്വസിക്കുന്നു. കാലം എന്ന പ്രതിഭാസത്തിന് അനുസൃതമായി അനുസ്യൂതം പരിണാമം സംഭവിച്ചുകൊണ്ടിരിക്കുന്നതാണ് നമ്മുടെ പ്രപഞ്ചം. ഇതുപോലെതന്നെ ഗ്രഹനിലയിലും സദാ മാറ്റങ്ങള്‍ സംഭവിച്ചുകൊണ്ടിരിക്കുന്നു. അതിനാല്‍ ഗ്രഹനിലയും അതോടനുബന്ധിച്ചുള്ള അനുഭവങ്ങളും കാലത്തിന്റെ ഗുണധര്‍മങ്ങള്‍ (functions of time) ആണെന്നു പറയാം. അതുകൊണ്ട് നമ്മുടെ അനുഭവങ്ങള്‍ക്ക് ഗ്രഹനിലയുമായും ബന്ധമുണ്ടാകേണ്ടതാണ് എന്ന് ജ്യോതിഷം അനുശാസിക്കുന്നു. പ്രവചനത്തിനു ഗ്രഹങ്ങളുടെ സ്ഥാനനിര്‍ണയം ആവശ്യമായതിനാല്‍ ജ്യോതിശ്ശാസ്ത്രം (Astronomy) എന്ന ശാസ്ത്രശാഖയുമായും ജ്യോതിഷം ബന്ധപ്പെട്ടിരിക്കുന്നു. 

'''ചരിത്രം'''. മാനവചരിത്രത്തോളം പഴക്കം  ജ്യോതിഷത്തിനുമുണ്ടെന്നു കരുതാം. വേദങ്ങളിലും പുരാണങ്ങളിലും മതഗ്രന്ഥങ്ങളിലുമെല്ലാം ജ്യോതിഷം പരാമര്‍ശിക്കപ്പെടുന്നുണ്ട്. ഇതിന്റെ ഉദ്ഭവം ഇന്നും അജ്ഞാതമാണ്. ബി.സി. 3000-നു മുമ്പ് മെസപ്പോട്ടേമിയയിലാണ് ജ്യോതിഷം ഉണ്ടായതെന്ന് അഭിപ്രായമുണ്ട്. അവിടെ നിന്നും ഗ്രീസ്, ഇന്ത്യ, ചൈന എന്നിവിടങ്ങളിലേക്ക് വ്യാപിച്ചതായിരിക്കാമെന്നു കരുതുന്നു.

ബി.സി. 5-ാം ശ.-ലാണു ചൈനയില്‍ ജ്യോതിഷം പ്രചരിച്ചു തുടങ്ങിയതെന്നു കരുതുന്നു. അറബിനാടുകളിലും ഗ്രീസിലും ജ്യോതിഷം പ്രചരിച്ചിരുന്നു. ജ്യോതിഷത്തില്‍ വളരെയേറെ പുരോഗതിയും പ്രചാരവും പ്രാചീന ഭാരതത്തിലായിരിക്കണം ഉണ്ടായത്.  ലോകത്തിന്റെ വിവിധഭാഗങ്ങളില്‍ പ്രചാരത്തില്‍ വന്ന ഈ വിജ്ഞാനശാഖ പ്രാദേശികമായ ചില വ്യത്യാസങ്ങള്‍ ഉണ്ടെന്നുള്ളതൊഴിച്ചാല്‍, അടിസ്ഥാനപരമായി ഒരേ മൌലിക തത്ത്വത്തില്‍ അധിഷ്ഠിതമാണ്. ഗ്രഹനില വിവരിക്കുന്ന രീതിയിലും ആ ഗ്രഹനിലയില്‍ നിന്നു പ്രവചനം നടത്തുന്ന രീതിയിലും അത്യധികം സാദൃശ്യമാണുള്ളത്. ഗ്രഹങ്ങളുടെ സാങ്കല്പിക സഞ്ചാരപഥത്തെ (രാശിചക്രം) പന്ത്രണ്ടു സമഭാഗങ്ങളായി (രാശി) വിഭജിച്ച് രാശികള്‍ക്കും ഗ്രഹങ്ങള്‍ക്കും ചില ദേവതകളോടും പ്രതിഭാസങ്ങളോടും വസ്തുക്കളോടും രോഗങ്ങളോടും മറ്റും ബന്ധം കല്പിച്ചു പ്രവചനം നടത്തുക, സപ്തഗ്രഹങ്ങളില്‍ ഓരോന്നിനും ഒരു ദിവസം വീതം കണക്കാക്കി ഒരാഴ്ചയില്‍ ക്രമത്തിന് രവി, ചന്ദ്രന്‍, കുജന്‍, ബുധന്‍, ഗുരു, ശുക്രന്‍, ശനി എന്നീ ഗ്രഹങ്ങള്‍ക്ക് ഓരോ ദിനം സങ്കല്പിക്കുക എന്നിങ്ങനെ പല സംഗതികളിലും സാദൃശ്യം കാണുന്നുണ്ട്. ഇത് യാദൃച്ഛികമല്ല. നിശ്ചിതമായ മൗലിക തത്ത്വങ്ങളെ അടിസ്ഥാനപ്പെടുത്തിത്തന്നെയാണ് ഈ ക്രമീകരണങ്ങളെല്ലാം. ഗ്രഹങ്ങളെ പഞ്ചഭൂതവുമായി ബന്ധപ്പെടുത്തുന്നതിലും സാര്‍വത്രികത കാണുന്നുണ്ട്. ഗുണങ്ങള്‍ എന്നോ തത്ത്വങ്ങള്‍ എന്നോ പറയാവുന്ന 'യാങ്', 'യിന്‍' എന്നീ രണ്ടു വിഭാഗങ്ങളിലായി എല്ലാ വസ്തുക്കളെയും തരംതിരിക്കുന്ന ചൈനീസ് രീതി പ്രസിദ്ധമാണ്. ഗ്രഹങ്ങളെയും ഈ തത്ത്വങ്ങളില്‍ ഉള്‍പ്പെടുത്തിയിരുന്നു. ഏതു കര്‍മത്തിനും അനുയോജ്യമായ സമയം നിര്‍ണയിച്ചിരുന്നതും ഗ്രഹനിലയുടെ അടിസ്ഥാനത്തിലാണ്. അറബിജ്യോതിഷത്തിലും ഈ 'മുഹൂര്‍ത്ത' നിര്‍ണയം കാണുന്നുണ്ട്. ജനനസമയത്തെ ഗ്രഹനിലയെ ആസ്പദമാക്കി ജാതകം ആവിഷ്കരിച്ചത് ഗ്രീക്കുകാര്‍ ആയിരിക്കാം. ഭാരതീയ ജ്യോതിഷത്തില്‍ ഗ്രീക്കുകാരുടെ ശാസ്ത്രം വളരെ പ്രഭാവം ചെലുത്തിയിരുന്നു. വരാഹമിഹിരന്‍ രചിച്ച ബൃഹജ്ജാതകത്തില്‍ യവനന്മാരുടെ ജ്യോതിഷത്തെപ്പറ്റി സ്പഷ്ടമായി പറഞ്ഞിട്ടുണ്ട്. യവനജാതകം എന്ന പേരില്‍ സ്ഫുജിധ്വജന്റെ പ്രസിദ്ധമായ ഗ്രന്ഥം ഇംഗ്ലീഷിലും മലയാളത്തിലും വ്യാഖ്യാനസഹിതം പ്രകാശനം ചെയ്തിട്ടുണ്ട്. മീനരാജന്‍ രചിച്ച ജ്യോതിഷഗ്രന്ഥമാണ് വൃദ്ധയവനജാതകം.

ജ്യോതിഷവും ജ്യോതിശ്ശാസ്ത്രവും തമ്മില്‍ അഭേദ്യമായ ബന്ധമാണുള്ളത്. ഭാരതത്തില്‍ ഇവ രണ്ടും ഒന്നായി പണ്ട് പരിഗണിച്ചിരുന്നു എന്നു കരുതാം. ജ്യോതിഷം വേദാംഗമാകുന്നു. അതുകൊണ്ടുതന്നെ അതിനു വേദങ്ങള്‍ക്കെന്നപോലെ അംഗീകാരവും  ലഭിച്ചിരുന്നു. 'വേദചക്ഷുസ്സ്' എന്നു ജ്യോതിഷം വിശേഷിപ്പിക്കപ്പെടുന്നുണ്ട്. 

'''ജ്യോതിഷവിഭാഗങ്ങള്‍.'''' ആറു വിഭാഗങ്ങളാണ് ജ്യോതിഷത്തിലുള്ളത്-ഗോളം, ഗണിതം, ജാതകം, നിമിത്തം, പ്രശ്നം, മുഹൂര്‍ത്തം എന്നിവ. ഗോളവും ഗണിതവും പ്രായേണ ജ്യോതിശ്ശാസ്ത്രം തന്നെയാണ്. ഗ്രഹങ്ങളുടെ സ്ഥിതിവിവരണങ്ങളും അവയുടെ ചലനനിയമങ്ങളും ഗ്രഹനില നിര്‍ണയിക്കാനുള്ള വിധികളും മറ്റും ഇവയില്‍ പ്രതിപാദിക്കപ്പെടുന്നു. ഗ്രഹനിലയുടെ അടിസ്ഥാനത്തില്‍ അനുമാനിക്കപ്പെടുന്ന സംഗതികളാണ് മറ്റു വിഭാഗങ്ങളില്‍ കാണുന്നത്. 

'''ഗ്രഹനില'''. ഭൂമിയിലെ ഒരു നിരീക്ഷകന് സൂര്യനും ചന്ദ്രനും മറ്റു ഗ്രഹങ്ങളും ഭൂമിയെ ചുറ്റി കറങ്ങിക്കൊണ്ടിരിക്കുന്നതായി അനുഭവപ്പെടുന്നു. ഈ ഗ്രഹപഥത്തെ രാശിചക്രം എന്നു പറയുന്നു. വൃത്താകാരമായ ഈ രാശിചക്രത്തില്‍ സ്ഥാനവിവരണത്തിന് ഒരു ആരംഭബിന്ദു ആവശ്യമാണ്. ഭാരതീയ ജ്യോതിഷത്തില്‍ അതിവിദൂരതയിലെ നക്ഷത്രങ്ങളെയാണ് ചക്രത്തിലെ ആരംഭബിന്ദു നിര്‍ണയിക്കാനായി ഉപയോഗപ്പെടുത്തുന്നത്. നക്ഷത്രങ്ങള്‍ ഭൂമിയെ ചുറ്റി സഞ്ചരിക്കാത്തവയാണ് എന്നതാണ് ഇതിനു കാരണം. അശ്വമുഖം പോലുള്ള അശ്വതി നക്ഷത്രസമൂഹത്തില്‍ നിന്ന് ഭൂമിയിലേക്കു വരയ്ക്കുന്ന രേഖ രാശിചക്രത്തെ ഖണ്ഡിക്കുന്ന ബിന്ദുവാണ് രാശിചക്രത്തിലെ ആരംഭബിന്ദുവായി കണക്കാക്കപ്പെടുന്നത്. ഈ ബിന്ദുവില്‍ നിന്ന് ചക്രത്തെ 12 സമഭാഗങ്ങളായി വിഭജിക്കുന്നു; അതായത് 30&deg; വീതമുള്ള 12 വൃത്തഖണ്ഡങ്ങള്‍. ഭൂമിയില്‍ നിന്നു നോക്കുമ്പോള്‍ കാണുന്ന ഈ 12 ദിശകളാണ് 12 രാശികള്‍. ആദ്യത്തേത് മേടം രാശി (Aries). തുടര്‍ന്ന് ഇടവം (Taurus), മിഥുനം (Gemini), കര്‍ക്കടകം (Cancer), ചിങ്ങം (Leo), കന്നി (Virgo), തുലാം (Libra), വൃശ്ചികം (Scorpio), ധനു (Sagitarius), മകരം (Capricon), കുഭം (Aquarius), മീനം (Pisces) എന്നീ രാശികള്‍. മീനം അവസാനിക്കുന്ന ബിന്ദുവാണ് മേടത്തിന്റെ ആദ്യബിന്ദു (0&deg;=360&deg;).

ഗ്രഹനില വിവരിക്കാന്‍ ഓരോ ഗ്രഹവും ആരംഭബിന്ദുവില്‍ നിന്ന് എത്ര അകലെ (കോണ്‍ അളവില്‍) നില്ക്കുന്നു എന്ന് വ്യക്തമാക്കിയാല്‍ മതി. ഓരോ ഗ്രഹത്തിന്റെയും സ്ഥാനം എത്രഡിഗ്രിയില്‍ വരുന്നു എന്നു പറയുന്നതിനെയാണ് സ്ഫുടം (longitude) എന്നു പറയുന്നത്. ഈ സ്ഥാനങ്ങളില്‍ നിന്ന് ഗ്രഹം ഏതു രാശിയില്‍ നില്ക്കുന്നു എന്നു മനസ്സിലാക്കാം. 

'''ലഗ്നം '''(Ascendant). ഭൂമി സ്വന്തം അച്ചുതണ്ടിനെ ആധാരമാക്കി ഒരു ദിവസംകൊണ്ട് ഒരു ഭ്രമണം നടത്തുന്നു. അതുകൊണ്ടാണ് ദിനരാത്രങ്ങള്‍ ഉണ്ടാകുന്നത്. ഭൂമിയുടെ ഈ ഭ്രമണത്താല്‍ രാശിചക്രവും അതിലെ ഗ്രഹങ്ങളും ഒരു ദിവസംകൊണ്ട് ഒരു തവണ കറങ്ങുന്നതായി തോന്നും-കിഴക്കു നിന്നു പടിഞ്ഞാറോട്ട്. അങ്ങനെ ഓരോ ദിവസവും സൂര്യോദയം മുതല്‍ അടുത്ത സൂര്യോദയം വരെ, രാശികള്‍ ഓരോന്നായി സൂര്യോദയസ്ഥാനത്ത് ഉദിച്ചുയരുന്നു. ഒരു പ്രത്യേക സമയത്ത് ഏതു രാശിയാണോ സൂര്യോദയസ്ഥാനത്തു നില്ക്കുന്നത് അതാണ് ലഗ്നം അഥവാ ഉദയലഗ്നം (Rising sign). നമ്മുടെ മാസങ്ങള്‍ സൂര്യന്റെ സഞ്ചാരത്തെ അനുസരിച്ചുള്ളവയാണ്. സൂര്യന്‍ മേടം രാശിയില്‍ സഞ്ചരിച്ചുകൊണ്ടിരിക്കുന്ന  കാലമാണു മേടമാസം. അതുപോലെ മറ്റു മാസങ്ങളും. 

'''ഭാവങ്ങള്‍.''' ലഗ്നം ഏതു രാശിയാണോ അത് ഒന്നാം ഭാവം (First house), അടുത്തത് രണ്ടാം ഭാവം, അങ്ങനെ പന്ത്രണ്ടു ഭാവങ്ങള്‍. അപ്പോള്‍ ഓരോ ഗ്രഹവും മേഷാദിരാശികളില്‍ ഒന്നില്‍ നില്‍ക്കുന്നതോടൊപ്പം ഒരു 'ഭാവ'ത്തിലും നില്ക്കുന്നു. ഗ്രഹനില എന്നാല്‍ ലഗ്നത്തിന്റെയും ഓരോ ഗ്രഹത്തിന്റെയും സ്ഥാനങ്ങള്‍ കുറിക്കുന്ന വിവരണമാണ്. അതായത് ഒരു നിശ്ചിതസമയത്ത് ലഗ്നത്തിന്റെയും ഓരോ ഗ്രഹത്തിന്റെയും സ്ഥിതി ഏതേതു രാശികളിലാണ് എന്നുള്ള വിവരണം. ഒരാളുടെ ജനനസമയത്തെ ഗ്രഹനിലയെയാണ് ജാതകം (horoscope) എന്നു പറയുന്നത്. 

'''ഗ്രഹനിലയുടെ ചിത്രീകരണം'''. രാശികളെയും അതില്‍ ഓരോ ന്നിലും ഉള്ള ഗ്രഹങ്ങളെയും ചിത്രീകരിക്കുന്ന രീതിയില്‍ ചില പ്രാദേശിക വ്യത്യാസങ്ങള്‍ കാണുന്നുണ്ട്. വൃത്താകാരമായ രാശിചക്രത്തെ അതേപോലെ ചിത്രീകരിച്ച് ലഗ്നത്തിന്റെയും ഓരോ ഗ്രഹത്തിന്റെയും സ്ഥാനങ്ങള്‍ ആരംഭബിന്ദുവില്‍നിന്ന് ഇത്ര ഡിഗ്രി-മിനിറ്റ്-സെക്കന്‍ഡ് (ഭാഗ-കല-വികല) എന്നു കുറിക്കുന്ന രീതിയാണ് പാശ്ചാത്യര്‍ ഉപയോഗിച്ചുവരുന്നത്. രാശിചക്രത്തെ ചതുരാകൃതിയില്‍ കുറിക്കുന്ന രീതിയാണ് ഭാരതത്തില്‍ അവലംബിച്ചിരിക്കുന്നത്. ഇന്ത്യയില്‍ത്തന്നെ ദക്ഷിണഭാഗത്തുള്ള രീതിക്കും ഉത്തരഭാഗത്തുള്ള രീതിക്കും ചില വ്യത്യാസങ്ങളുണ്ട്. ദക്ഷിണേന്ത്യന്‍ രാശികളുടെ സ്ഥാനം സ്ഥിരമാണ്; അവയില്‍ ലഗ്നത്തെയും ഓരോ ഗ്രഹത്തെയും അതതു രാശികളില്‍ ഗ്രഹത്തിന്റെ പ്രതീകങ്ങളോ അവയെ സൂചിപ്പിക്കുന്ന അക്ഷരങ്ങളോ കൊണ്ടു സൂചിപ്പിക്കുന്നു.

[[ചിത്രം:Jyothishm.png|300px]]

ഒരു മാതൃകാ ഗ്രഹനില നോക്കുക

[[ചിത്രം:Jyothih45.png|300px]]
 
ഈ ഗ്രഹനിലയില്‍ ലഗ്നഭാവം മിഥുനം രാശിയിലാണ്. ലഗ്നത്തില്‍ മിഥുനം രാശിയില്‍ ശുക്രന്‍ സ്ഥിതിചെയ്യുന്നു. കര്‍ക്കടകം രാശിയില്‍ രണ്ടാം ഭാവത്തില്‍ രവി ബുധന്മാര്‍ നില്ക്കുന്നു-ഇത്യാദി. ചില ഭാവങ്ങള്‍ ശൂന്യങ്ങളാകുന്നു.

ഗ്രഹനിലയില്‍നിന്ന് ഫലപ്രവചനം നടത്തുന്നത് ഗ്രഹങ്ങളും ഭാവങ്ങളും രാശികളും തമ്മിലുള്ള ബന്ധങ്ങളില്‍ നിന്നാണ്. ഓരോ ഗ്രഹവും ഭാവവും സൂചിപ്പിക്കുന്ന കാര്യങ്ങളില്‍ നിന്നാണ് നിഗമനങ്ങള്‍ ലഭിക്കുന്നത്. 

'''ഭാവങ്ങള്‍ സൂചിപ്പിക്കുന്ന കാര്യങ്ങള്‍:'''

ലഗ്ന ഭാവം	:	ദേഹസൗഷ്ഠവം, സ്ഥിതി, ശ്രേയസ്സ്, ശിരസ്സ്, യശസ്സ്, ജയാപജയങ്ങള്‍, സ്വാസ്ഥ്യം മുതലായവ.

രണ്ടാം ഭാവം	:	സ്വത്ത്, കുടുംബം, വാക്ക്, കണ്ണ്, വിദ്യ, മുഖം ഇത്യാദി.

മൂന്നാം ഭാവം	:	സഹോദരന്‍, അയല്‍പക്കം, ധൈര്യം, ദൗത്യം, സഹായം, ഗണിതം, ബാഹുക്കള്‍ മുതലായവ.

നാലാം ഭാവം	:	മാതാവ്, ബന്ധുക്കള്‍, സുഖം, ജന്മഗൃഹം, ഇരിപ്പിടം, വാഹനം, ഹൃദയം, ഭൂസ്വത്ത് ഇത്യാദി.

അഞ്ചാം ഭാവം :	സന്താനം, സൗമനസ്യം, മന്ത്രം, മന്ത്രി, ഉദരം, ബുദ്ധി, വിവേകം, രാജാനുകൂല്യം മുതലായവ.

ആറാം ഭാവം	:	രോഗം, ചോരന്‍, ശത്രു, വിഘ്നം ഇത്യാദി.

ഏഴാം ഭാവം	:	യാത്ര, ഭാര്യ, ഭര്‍ത്താവ്, വിവാഹം, നഷ്ടാര്‍ഥലാഭം മുതലായവ.

എട്ടാം ഭാവം	:	സര്‍വനാശം, രഹസ്യം, വിപത്ത്, മരണം, കല ഹം, ദാരിദ്ര്യം, തടസ്സം, ഭൃത്യന്‍ മുതലായവ.

ഒന്‍പതാം ഭാവം :	പിതാവ്, ഗുരുക്കന്മാര്‍, ഉപാസന, ഗുരുത്വം, പൗത്രന്‍, ദയ, ധര്‍മം ഇത്യാദി.

പത്താം ഭാവം	:	കര്‍മം, ബഹുമാനം, കീര്‍ത്തി, സ്ഥാനമാനങ്ങള്‍, ആശ്രയം, ആജ്ഞ.

പതിനൊന്നാം ഭാവം	:	ലാഭം, അഭീഷ്ടലബ്ധി, ജ്യേഷ്ഠസഹോദരങ്ങള്‍, ചരിത്രം, വരവ്.

പന്ത്രണ്ടാംഭാവം :	പാപം, വ്യയം, പാദം, അധഃപതനം, സ്ഥാനഭ്രംശം, വൈകല്യം, ബന്ധനം ഇത്യാദി.

'''ഗ്രഹങ്ങളുടെ കാരകത്വങ്ങള്‍:'''

രവി	: പിതാവ്, ധൈര്യം, ഹോമകാര്യങ്ങള്‍, രാജസേവ, ആത്മസൗഖ്യം, പ്രതാപം, ശിവന്‍, അധീശത, അസ്ഥി, ഉത്സാഹം തുടങ്ങിയവ.

ചന്ദ്രന്‍ : മാതാവ്, മനസ്സ്, ജലം, സ്ത്രീ, ആഡംബരം, കൃഷി, ഐശ്വര്യം, പഴവര്‍ഗങ്ങള്‍, പുഷ്പം, സസ്യം, സമുദ്രോത്പന്നങ്ങള്‍, രക്തം, തനുസുഖം, ദുര്‍ഗാ ഇത്യാദി.

കുജന്‍	: സഹോദരന്‍, ധൈര്യം, സാഹസം, അഗ്നി, ജ്ഞാതികള്‍, കലഹം, മജ്ജ, ചോരന്‍, സേനാധിപത്യം, ആയുധം, ക്ഷതം, യുദ്ധം ഇത്യാദി.

ബുധന്‍ : പാണ്ഡിത്യം, വാക്ക്, കല, രാജകുമാരന്‍, ഹാസ്യം, വിദ്യ, വിനോദം, വൈഷ്ണവകാര്യങ്ങള്‍, വിദ്വജ്ജനം, ക്ഷമ, സത്യം, വിവേകം, ബുദ്ധി, ചര്‍മം ഇത്യാദി.

ഗുരു	: ജ്ഞാനം, ഇന്ദ്രിയനിയന്ത്രണം, ധനം, ദയ, വേദാന്തചിന്ത, മാഹാത്മ്യം, ജ്യേഷ്ഠസഹോദരങ്ങള്‍, ശാസ്ത്രങ്ങള്‍, പ്രതിഭ, ആചാരം, ശ്രേഷ്ഠത മുതലായവ.

ശുക്രന്‍ : ഭോഗം, ആഡംബരം, സമ്പത്ത്, വാഹനം, ഉത്സവം, കവിത്വം, സരസ സംഭാഷണം, വസ്ത്രം, ആഭരണം, ഭാര്യ, അറിവ്, നാല്ക്കാലികള്‍ ഇത്യാദി.

ശനി	: ആയുസ്സ്, കൃഷിസാധനങ്ങള്‍, മരണം, ദാരിദ്യ്രം, രോഗം, വഞ്ചന, അശുചിത്വം, ഭയം, ദുഃഖം, ഭൃത്യന്‍, കാരാഗൃഹം തുടങ്ങിയവ. 

'''രാഹുവും കേതുവും.''' ഇവ രണ്ടും സാങ്കല്പിക ബിന്ദുക്കളാണ്. വിഷുവത്വൃത്തവും ക്രാന്തിവൃത്തവും സന്ധിക്കുന്ന രണ്ടു ബിന്ദുക്കളാണിവ. ഇവയെ ഛായാഗ്രഹങ്ങള്‍ എന്നു പറയുന്നു. രവിചന്ദ്രാദിഗ്രഹങ്ങള്‍ സഞ്ചരിക്കുന്ന ദിശയ്ക്കു വിപരീതമായിട്ടാണ് ഇവയുടെ സഞ്ചാരം. രണ്ടും തമ്മില്‍ 180&deg; അന്തരം (ആറു രാശികള്‍) ഉണ്ടായിരിക്കും. ഇവ രണ്ടും ഏറെക്കുറെ ശനിയോടും (രാഹു) ചൊവ്വയോടും (കേതു) ഉപമിക്കപ്പെടുന്നു. 

'''ശുഭഗ്രഹങ്ങളും പാപഗ്രഹങ്ങളും'''. ഗ്രഹങ്ങളെ ശുഭനെന്നും പാപനെന്നും രണ്ടു വിഭാഗങ്ങളായി വിഭജിച്ചിരിക്കുന്നു. പൊതുവേ ഗുരു, ശുക്രന്‍, ബുധന്‍, ചന്ദ്രന്‍ ഇവര്‍ ശുഭന്മാരും മറ്റുള്ളവ പാപന്മാരുമാണ്. പാപനോടു ചേര്‍ന്നു നില്ക്കുന്ന ബുധനും പാപനാണ്. പക്ഷബലമില്ലാത്ത ചന്ദ്രനും പാപനാണ്. 

'''നക്ഷത്രം'''. രാശിചക്രത്തെ 12 രാശികളായി വിഭജിച്ചതുപോലെതന്നെ, അതിനെ 27 സമഭാഗങ്ങളായി വിഭജിച്ച് ഓരോന്നിനും ആ ദിശയില്‍ കാണപ്പെടുന്ന ഒരു നക്ഷത്രത്തിന്റെ പേരും നല്കപ്പെട്ടു. അപ്പോള്‍ ഒരു നക്ഷത്രത്തിന്  [[ചിത്രം:JySR1.png]] അകലമുണ്ട്. ആരംഭബിന്ദുമുതല്‍ ഓരോ 13&deg; 20' അംശങ്ങളെ ക്രമത്തിന് അശ്വതി (0&deg;-13&deg; 20'), ഭരണി (13&deg;20' - 26&deg; 40'), കാര്‍ത്തിക, രോഹിണി, മകയിരം, തിരുവാതിര, പുണര്‍തം, പൂയം, ആയില്യം, മകം, പൂരം, ഉത്രം, അത്തം, ചിത്തിര, ചോതി, വിശാഖം, അനിഴം, കേട്ട, മൂലം, പൂരാടം, ഉത്രാടം, തിരുവോണം, അവിട്ടം, ചതയം, പൂരുരുട്ടാതി, ഉത്തൃട്ടാതി, രേവതി എന്നിങ്ങനെ നാമകരണം ചെയ്തിരിക്കുന്നു. അതതു ദിശയില്‍ കാണപ്പെടുന്ന നക്ഷത്രങ്ങളുടെ പേരുകളാണ് ഓരോന്നിനും നല്കപ്പെട്ടിരിക്കുന്നത്. ചന്ദ്രന്റെ ഒരു ദിവസത്തെ ശരാശരി സഞ്ചാരം ഏകദേശം ഒരു നക്ഷത്രദൈര്‍ഘ്യമായ 13&deg; 20' ആണ്. ഒരു രാശിക്ക് 30&deg; ആയതിനാല്‍ ഓരോ രാശിയിലും [[ചിത്രം:JySR2.png]] നക്ഷത്രങ്ങള്‍ വീതം നില്ക്കും. ഉദാ. മേടം രാശിയില്‍ അശ്വതിയും (0&deg; മുതല്‍ 13&deg; 20' വരെ), ഭരണിയും (13&deg; 20' മുതല്‍ 26&deg; 40' വരെ) കഴിഞ്ഞ് ശേഷിക്കുന്ന 3&deg; 20' കാര്‍ത്തിക നക്ഷത്രത്തിന്റെ ആദ്യത്തെ കാല്‍ [[ചിത്രം:JYSR3.png]] കൂടി ഉള്‍പ്പെടുന്നു. കാര്‍ത്തികയുടെ മുക്കാല്‍ ഭാഗം (10&deg;) ഇടവം രാശിയിലെ ആദ്യത്തെ 10&deg; വരെ വ്യാപിക്കുന്നു. ഇപ്രകാരം 27 നക്ഷത്രങ്ങള്‍ വിന്യസിക്കപ്പെടുന്നു. ഓരോ ഗ്രഹത്തിന്റെയും സ്ഥാനം ഓരോ രാശിയില്‍  നിശ്ചയിക്കപ്പെടുന്നതുപോലെതന്നെ ഓരോ ഗ്രഹവും ഒരു നക്ഷത്രത്തിലായിരിക്കും. ചന്ദ്രന്‍ ഏതു നക്ഷത്രത്തിലാണോ നില്ക്കുന്നത് ആ നക്ഷത്രമാണ് ആ സമയത്തെ നക്ഷത്രം എന്നു പറയുന്നത്. സൂര്യന്‍ ഒരു നക്ഷത്രത്തില്‍ക്കൂടി സഞ്ചരിക്കുന്ന കാലത്തിന് ആ നക്ഷത്രത്തിന്റെ പേരുള്ള 'ഞാറ്റുവേല' എന്നു പറയുന്നു.

'''രാശികളും ഗ്രഹങ്ങളും തമ്മിലുള്ള ബന്ധങ്ങള്‍.''' മേടം മുതല്‍ മീനം വരെയുള്ള ഓരോ രാശിയുടെയും 'ആധിപത്യം' ഓരോ ഗ്രഹത്തിനും നല്കപ്പെടുന്നു. മേടം, വൃശ്ചികം എന്നീ രാശികള്‍ക്ക് അധിപന്‍ കുജന്‍; ഇടവം, തുലാം എന്നിവയ്ക്ക് ശുക്രന്‍; മിഥുനം, കന്നി എന്നിവയ്ക്ക് ബുധന്‍; ധനു-മീനങ്ങള്‍ക്ക് ഗുരു; മകര-കുംഭങ്ങള്‍ക്ക് ശനി; കര്‍ക്കടകത്തിനു ചന്ദ്രന്‍; ചിങ്ങത്തിനു രവി-ഇപ്രകാരമാണ് രാശ്യധിപന്മാര്‍. സ്വരാശിയില്‍ നില്ക്കുമ്പോള്‍ ഗ്രഹങ്ങള്‍ക്കു ബലം കൂടുതലാണ്. എന്നാല്‍ ഒരു ഗ്രഹം ഏറ്റവും ബലവാനാകുന്നത് അതിന്റെ 'ഉച്ച' സ്ഥാനത്താണ്. ഉച്ചബിന്ദുവിന്റെ 180&deg; അകലെ അതു നീചനുമാകുന്നു.

[[ചിത്രം:JySr4.png|400px]]

ഗ്രഹങ്ങള്‍ തമ്മില്‍ ബന്ധുശത്രുത്വങ്ങളും കല്പിക്കപ്പെടുന്നുണ്ട്. ഒരു നിശ്ചിത നിയമത്തിന്റെ അടിസ്ഥാനത്തിലാണിത്.

[[ചിത്രം:JyothiSR002.png|400px]]

'''ഗ്രഹങ്ങളുടെ ദൃഷ്ടി.'''' ഒരു ഗ്രഹം ഏതു രാശിയില്‍ നില്ക്കുന്നുവോ അതിന്റെ ഏഴാം രാശിയിലേക്കു ദൃഷ്ടി ചെയ്യുന്നു (aspect). ശനിക്കു മൂന്നിലേക്കും പത്തിലേക്കും കുജനു നാലിലേക്കും എട്ടിലേക്കും ഗുരുവിന് അഞ്ചിലേക്കും ഒന്‍പതിലേക്കും വിശേഷാല്‍ പൂര്‍ണ ദൃഷ്ടിയുണ്ട്. പൊതുവേ ശുഭഗ്രഹങ്ങളുടെ ദൃഷ്ടി ശുഭസൂചകവും പാപഗ്രഹങ്ങളുടെ ദൃഷ്ടി ദോഷസൂചകവുമാകുന്നു. ഫലനിര്‍ണയത്തില്‍ ദൃഷ്ടികള്‍ക്കു വളരെ പ്രാധാന്യമുണ്ട്.  പാശ്ചാത്യ ജ്യോതിഷത്തില്‍ ദൃഷ്ടികള്‍ക്കാണ് കൂടുതല്‍ പ്രാധാന്യമുള്ളത്. പക്ഷേ, അവരുടെ ദൃഷ്ടിക്രമങ്ങള്‍ക്ക് ഭാരതത്തിലെ ജ്യോതിഷരീതിയില്‍ നിന്ന് വ്യത്യാസമുണ്ട്.

'''ഫലാദേശം'''. പൊതുവേ ജാതകചിന്തയില്‍ ഗ്രഹങ്ങളുടെ കാരകത്വങ്ങള്‍, ഭാവാധിപത്യങ്ങള്‍, ബലം, ദൃഷ്ടി, മറ്റു ഗ്രഹങ്ങളുമായുള്ള യോഗം ഇത്യാദികള്‍ പരിഗണിക്കപ്പെടുന്നു. ഓരോ ഭാവത്തിന്റെയും ഫലങ്ങള്‍ ആ ഭാവം, അവിടെ നില്ക്കുന്ന ഗ്രഹങ്ങള്‍, അവിടെ നോക്കുന്ന ഗ്രഹങ്ങള്‍, ഭാവാധിപന്‍, കാരകന്‍ എന്നിങ്ങനെയുള്ളവയില്‍ നിന്ന് അനുമാനിക്കാനുള്ള സാമാന്യവിധികള്‍ ഉണ്ട്. ജ്യോതിഷഗ്രന്ഥങ്ങളില്‍ നിന്ന് ഈ വിശകലനരീതികള്‍ മനസ്സിലാക്കാം.

ഭാവേശ ഭാവഗത ഭാവനിരീക്ഷകൈസ്തൈര്‍-

ഭാവ സ്വഭാവ വപുരാദി ഗുണാ വിചിന്ത്യാ

ഭാവാനുഭൂതിരിഹ ഭാവ വിലഗ്നപത്യോഃ

സംബന്ധതോ ഭവതി ചാരവശാല്‍ ദശാദൗ

(ജാതകാദേശം - അധ്യാ. 10 ശ്ലോ. 4)

ജാതകചിന്തയുടെ രഹസ്യം ഈ ശ്ലോകത്തില്‍ അന്തര്‍ഭവിച്ചിരിക്കുന്നു. ആദ്യംതന്നെ ഒരു ഭാവം (കാര്യം) ഈ ജാതകപ്രകാരം സാധ്യതയുണ്ടോ എന്നു നിരൂപിച്ചശേഷം അങ്ങനെ ആ കാര്യം ലഭ്യതയുള്ളതാണെങ്കില്‍ എന്നാണ് അനുഭവപ്പെടുക എന്നു കണ്ടുപിടിക്കുന്നു. അതിന് ലഗ്നാധിപനും ഭാവാധിപനും ഏതു കാലത്ത് അന്യോന്യം ബന്ധപ്പെടുന്നുവോ അപ്പോള്‍ത്തന്നെ ആ കാര്യസിദ്ധിക്കു യോജിച്ച 'ദശാപഹാരാദികള്‍' കൂടി വരുന്നു എങ്കില്‍ ആ സമയം ഭാവസിദ്ധിയുണ്ടാകും. ഈ കാലനിര്‍ണയരീതിയാണ് ഭാരതീയ ജ്യോതിഷത്തിന്റെ പ്രത്യേകത. 

'''ദശാപഹാരാദികള്‍.''' ദശ എന്നാല്‍ അവസ്ഥ എന്നു പറയാം. ജീവിതത്തില്‍ ഓരോ ഗ്രഹത്തിന്റെയും പ്രഭാവം (അതായത് ആ ഗ്രഹം സൂചിപ്പിക്കുന്ന ഫലം) അനുഭവപ്പെടുന്ന കാലം ഉണ്ട്. ഈ കാലമാണ് ആ ഗ്രഹത്തിന്റെ ദശ. ദശാപ്രഭേദങ്ങള്‍ അനേകവിധത്തില്‍ പൂര്‍വാചാര്യന്മാര്‍ നിര്‍ദേശിച്ചിട്ടുണ്ട് - നക്ഷത്രദശ, കാലചക്രദശ, അംശകദശ എന്നിങ്ങനെ. ഇവയില്‍ ഏറ്റവും പ്രചാരത്തിലുപയോഗിച്ചുവരുന്നത് നക്ഷത്രദശാസമ്പ്രദായമാണ്. ഇതിനു വിംശോത്തരിദശ എന്നു പറയുന്നു. ഓരോ ഗ്രഹത്തിനും നിശ്ചിത വര്‍ഷങ്ങള്‍ കല്പിക്കപ്പെടുന്നു. ആകെ എല്ലാ ഗ്രഹങ്ങള്‍ക്കും കൂടി 120 വര്‍ഷമാണ്. അതുകൊണ്ട് നക്ഷത്രദശയ്ക്ക് വിംശോത്തരിദശ എന്നു പറയുന്നു. ജന്മനക്ഷത്രം അതായത് ജനനകാലത്ത് ചന്ദ്രന്‍ നില്ക്കുന്ന നക്ഷത്രം അനുസരിച്ചാണ് ദശാക്രമം. 27 നക്ഷത്രങ്ങളെ തുല്യമായി 9 ഗ്രഹങ്ങളുടെ ആധിപത്യത്തില്‍ വിഭജിക്കുന്നു. 

[[ചിത്രം:Jyothis34.png|400PX]]

ജന്മനക്ഷത്രാധിപന്റെ ദശാകാലം ആദ്യം വരുന്നു; തുടര്‍ന്ന് മുകളില്‍ കൊടുത്തിട്ടുള്ള ക്രമത്തിന് ഓരോ ഗ്രഹത്തിന്റെയും ദശകള്‍ വരുന്നു. ബുധദശ കഴിഞ്ഞാല്‍ കേതുദശ വരും. ഇവയില്‍ ജന്മനക്ഷത്രാധിപന്റെ ദശാകാലം പൂര്‍ണമായി ലഭിക്കുന്നില്ല. തുടര്‍ന്നുള്ളവ പൂര്‍ണമായി ലഭിക്കുകയും ചെയ്യും. ജന്മകാലത്ത് ചന്ദ്രന്‍ നില്ക്കുന്ന നക്ഷത്രത്തില്‍ ഇനി എത്ര സഞ്ചരിക്കേണ്ടതുണ്ടോ അതിന് ആനുപാതികമായാണ് ജന്മദശാകാലം. അശ്വതി നക്ഷത്രത്തില്‍ ജനിക്കുന്ന ഒരാളുടെ ചന്ദ്രന്‍ മേടം രാശിയില്‍ 5&deg; 20'-ല്‍ നില്ക്കുന്നു എന്നിരിക്കട്ടെ. അശ്വതി നക്ഷത്രത്തിന്റെ അന്ത്യം മേടം രാശിയില്‍ 13&deg; 20'-ലാണ്. അതിനാല്‍ ചന്ദ്രന് അശ്വതിയില്‍ സഞ്ചരിക്കാനുള്ള അകലം =13&deg;20'-5&deg; 20' = 8&deg;.

13&deg; 20' (ഒരു നക്ഷത്ര ദൈര്‍ഘ്യം) ബാക്കി ഉണ്ടായിരിക്കുമെങ്കില്‍ അശ്വതി നക്ഷത്രാധിപനായ കേതുവിന്റെ ദശാകാലം (7 വര്‍ഷം) പൂര്‍ണമായി ലഭിക്കും. ഇവിടെ നക്ഷത്രത്തില്‍ 'ചെല്ലാനുള്ളത്' 8&deg; ആയതിനാല്‍ ഈ ഉദാഹരണത്തില്‍ ജനനം മുതല്‍ കേതുദശാകാലം [[ചിത്രം:Jyothisham01.png|100px]] = 4 വര്‍ഷം 2 മാസം 12 ദിനം വരെ മാത്രമേ ഉണ്ടായിരിക്കൂ. ഇതിന് ജന്മശിഷ്ടദശ അഥവാ ഗര്‍ഭശിഷ്ടദശ എന്നു പറയുന്നു.  തുടര്‍ന്നുള്ള ദശകള്‍ പൂര്‍ണമായി ക്രമത്തിന് അനുഭവപ്പെടുന്നു. ഒരു ഗ്രഹത്തിന്റെ ദശാകാലത്തെ ദശാവര്‍ഷങ്ങളുടെ അതേ അനുപാതത്തില്‍ ഒന്‍പതു ഗ്രഹങ്ങള്‍ക്കായി വീതിക്കുന്നു. ഇതാണ് അപഹാരങ്ങള്‍ അഥവാ ഭുക്തി എന്നറിയപ്പെടുന്നത്. ഒരു ദശയില്‍ ആദ്യത്തെ അപഹാരം അതേ ഗ്രഹത്തിനുതന്നെ. ഇതിനു 'സ്വാപഹാരം' എന്നു പറയുന്നു. തുടര്‍ന്ന് മുന്‍ക്രമത്തിന് ഓരോ ഗ്രഹത്തിന്റെയും അപഹാരങ്ങള്‍ വരുന്നു. ഈ രീതിയില്‍ അപഹാരകാലങ്ങളെ അതേ അനുപാതത്തില്‍ വീണ്ടും വിഭജിക്കുമ്പോള്‍ ഓരോ ഗ്രഹത്തിന്റെയും 'ഛിദ്ര'കാലം കിട്ടുന്നു. ഛിദ്രത്തെ 'സൂക്ഷ്മ'ങ്ങളായും സൂക്ഷ്മങ്ങളെ 'പ്രാണ'ങ്ങളായും ദശാവര്‍ഷാനുപാതത്തില്‍ വീണ്ടും വിഭജിക്കുന്നുണ്ട്. ഈ രീതിയില്‍ ജീവിതകാലത്തിലെ ഓരോ സമയത്തും ഏതെല്ലാം ഗ്രഹങ്ങളുടെ പ്രഭാവം ഉണ്ടാകുമെന്ന് ദശാദികള്‍വഴി ചിന്തിക്കപ്പെടുന്നു. ഈ ദശാഫലങ്ങളാണ് ഓരോ കാലത്തും അനുഭവപ്പെടുക. എന്നാല്‍ ദശാനാഥന്‍, അപഹാരനാഥന്‍ മുതലായ ഗ്രഹങ്ങള്‍ സൂചിപ്പിക്കുന്ന കാര്യങ്ങള്‍ അനുഭവപ്പെടണമെങ്കില്‍ ലഗ്നാധിപനും ഭാവാധിപനും തമ്മില്‍ യോഗം, അന്യോന്യദൃഷ്ടി, ലഗ്നാധിപന്‍ ജാതകത്തില്‍ നില്ക്കുന്ന രാശിയില്‍ ഭാവാധിപന്‍ ആ കാലത്തു സഞ്ചരിക്കുക ഇത്യാദി ബന്ധങ്ങള്‍ കൂടി ഒത്തുവരേണ്ടതാകുന്നു.

'''വര്‍ഗങ്ങള്‍.''' കൃത്യമായ ഫലനിര്‍ണയത്തിന് ഓരോ ഗ്രഹവും നില്ക്കുന്ന രാശി ഏതെന്നുള്ള വിവരം മാത്രം പോരാ. ആ രാശിയിലെ 30&deg;-യില്‍ എവിടെ ഗ്രഹം നിന്നാലും ഫലം ഒന്നുതന്നെയല്ല. രാശിയിലെ സ്ഥാനഭേദമനുസരിച്ച് ഓരോ ഗ്രഹത്തിന്റെയും ഫല വ്യത്യാസങ്ങള്‍ നിര്‍ണയിക്കുന്നതിന് രാശിയെ വിഭജിച്ച് ഓരോ ഭാഗത്തിനും അനുയോജ്യമായ ഒരു ഗ്രഹത്തെ അധിപനായി കല്പിക്കപ്പെടുന്നു. ഇതിനെയാണ് വര്‍ഗങ്ങള്‍ എന്നു പറയുന്നത്. അനേകവിധത്തില്‍ വിഭജനം ഉണ്ടെങ്കിലും ബൃഹജ്ജാതക കര്‍ത്താവായ വരാഹമിഹിരന്‍ നിര്‍ദേശിച്ചിരിക്കുന്ന ആറു വര്‍ഗങ്ങള്‍(ഷഡ്വര്‍ഗങ്ങള്‍)ക്ക് ആണ് കൂടുതല്‍ പ്രാധാന്യം. രാശി, ഹോര, ദ്രേക്കാണം, നവാംശം, ദ്വാദശാംശം, ത്രിശാംശം എന്നിവയാണ് ഷഡ്വര്‍ഗങ്ങള്‍. ഹോര എന്നത് രാശിയുടെ പകുതിയാണ്. അപ്പോള്‍ ഒരു രാശിയില്‍ രണ്ടു ഹോര. ആദ്യഹോരയുടെ ആധിപത്യം ഒറ്റ രാശികളില്‍ (മേടം, മിഥുനം, ചിങ്ങം, തുലാം, ധനു, കുംഭം) സൂര്യനും രണ്ടാമത്തേതിനു ചന്ദ്രനുമാണ്. ഇരട്ട രാശികളില്‍ (മറ്റുള്ള രാശികള്‍) ആദ്യ ഹോരാധിപന്‍ ചന്ദ്രന്‍; രണ്ടാമത്തേതിനു സൂര്യന്‍. ഇതുപോലെ രാശിയുടെ മൂന്നിലൊരു ഭാഗത്തെയാണ് ദ്രേക്കാണം എന്നു പറയുന്നത്. ഒന്‍പതിലൊരംശം ആണ് നവാംശം. ഓരോ വര്‍ഗത്തിലും ആധിപത്യം നിശ്ചയിച്ചിട്ടുള്ളത് നിശ്ചിത നിയമങ്ങളനുസരിച്ചാകുന്നു. ഒരു ഗ്രഹം നില്ക്കുന്ന സ്ഥാനമനുസരിച്ച് ആ ഗ്രഹത്തിന്റെ വര്‍ഗാധിപന്മാര്‍ വ്യത്യാസപ്പെടുന്നു. വര്‍ഗാധിപന്മാര്‍ക്ക് ഒരു ഗ്രഹത്തിന്റെ സൂചനകളില്‍ സാരമായ വ്യത്യാസങ്ങള്‍ വരുത്താന്‍ സാധിക്കുന്നു. വര്‍ഗാധിപന്മാര്‍ എല്ലാം പാപഗ്രഹങ്ങളാകുന്നത് ശുഭഗ്രഹത്തിനുപോലും ഹാനിയുണ്ടാക്കുന്നു.

'''പൊരുത്തശോധന'''. ഭാരതത്തില്‍ വിവാഹത്തിനു വധൂവരന്മാര്‍ തമ്മില്‍ പൊരുത്തപ്പെട്ടുവരുമോ എന്നു ജാതകങ്ങള്‍ തമ്മില്‍ പൊരുത്തപരിശോധന നടത്തുന്നതു പതിവാണ്. രണ്ടുപേരുടെയും ജാതകങ്ങള്‍ തമ്മില്‍ 'ചേര്‍ച്ച' നോക്കുന്നതിനു പല വിധികളും ആവിഷ്കരിക്കപ്പെട്ടിട്ടുണ്ട്. വിവാഹത്തിനു മാത്രമല്ല, ജോലിക്കാരെ നിശ്ചയിക്കാനും വ്യാപാരത്തില്‍ കൂട്ടാളിയെ നിശ്ചയിക്കാനും സഹവാസത്തിനും വരെ എല്ലാം ജാതകംകൊണ്ടു പൊരുത്തശോധന നടത്താറുണ്ട്. 

'''രോഗവിചാരം'''. ജാതകം വിശകലനം ചെയ്ത് ഒരാള്‍ക്ക് ഉണ്ടാകാനിടയുള്ള രോഗങ്ങളെപ്പറ്റി മുന്‍കൂട്ടി വിവരങ്ങള്‍ ലഭിക്കുന്നു. സാധാരണമായി രോഗഭാവം എന്നത് ആറാം ഭാവമാണ്. ആറാം ഭാവം, ആറാം ഭാവാധിപന്‍, ആ ഗ്രഹം നില്ക്കുന്ന രാശി, വര്‍ഗാധിപന്മാര്‍, ആറിലേക്കു ദൃഷ്ടിചെയ്യുന്നവര്‍ എന്നിങ്ങനെയുള്ള ഘടകങ്ങള്‍ പരിശോധിച്ചാല്‍ രോഗവിവരങ്ങളും ആ രോഗം ഏതു കാലത്ത് അനുഭവപ്പെടാനിടയുണ്ടെന്നും നമുക്കു മനസ്സിലാക്കാം. മാത്രമല്ല, രോഗം മാറുമോ എന്നും മറ്റുമുള്ള ചിന്തകളും ജ്യോതിഷപരമായി ചിന്തിക്കാവുന്നതാണ്. ഇതുപോലെതന്നെ ജീവിതമാര്‍ഗം, സ്വഭാവം, വിദ്യാഭ്യാസം തുടങ്ങിയ കാര്യങ്ങളെക്കുറിച്ചും വിശദമായ വിവരങ്ങള്‍ ഗ്രഹനിലയില്‍ നിന്നു മനസ്സിലാക്കാവുന്നതാണ്.

'''മുഹൂര്‍ത്തം'''. ജ്യോതിഷത്തിലെ ആറുവിഭാഗങ്ങളില്‍ ഒന്നാണിത്. മുഹൂര്‍ത്തം എന്നാല്‍ സാമാന്യമായി നാം മനസ്സിലാക്കുന്നത് ഉചിതവും ശുഭകരവുമായ സമയം എന്നാണ്. ഏതൊരു കര്‍മത്തിനും അനുയോജ്യമായ സമയം കണ്ടെത്തുകയാണ് 'മുഹൂര്‍ത്ത'ത്തിലെ വിഷയം. വൈദിക കര്‍മങ്ങള്‍, വിദ്യാരംഭം, നൂതന സംരംഭങ്ങളാരംഭിക്കുക, കൃഷ്യാരംഭം, ഗൃഹനിര്‍മാണാരംഭം, ദേവപ്രതിഷ്ഠ, വിവാഹം, ചോറൂണ്, നാമകരണം ഇത്യാദി സകലവിധ കര്‍മങ്ങള്‍ക്കും ശുഭകരമായ സമയം നിര്‍ദേശിക്കുന്നതാണ് മുഹൂര്‍ത്ത വിഭാഗത്തിലെ പ്രതിപാദ്യം. ഗ്രഹനില അനുസരിച്ചാണിതു നടത്തുന്നത്. 

'''പ്രശ്നം'''. ജ്യോതിഷത്തിലെ അതിപ്രധാനമായ വിഭാഗമാണ് 'പ്രശ്നം'. ജനനസമയമോ മറ്റു വിവരങ്ങളോ ഒന്നും കൂടാതെ തന്നെ ഏതു കാര്യത്തെപ്പറ്റിയും പ്രവചനം നടത്തുന്ന രീതിയാണ് 'പ്രശ്നം'. ചോദ്യത്തില്‍ നിന്നുതന്നെ ഉത്തരം കണ്ടുപിടിക്കുന്ന തന്ത്രം. ചോദ്യം നടത്തുന്ന ആള്‍, ആ വ്യക്തിയുടെ ആ സമയത്തെ സ്ഥിതി, അവസ്ഥ, ചോദ്യത്തിലുള്ള അക്ഷരങ്ങള്‍, അപ്പോഴത്തെ ഗ്രഹനില ഇത്യാദികള്‍ വിശകലനം ചെയ്ത് ചോദ്യകര്‍ത്താവിന്റെ വര്‍ത്തമാന-ഭൂത-ഭാവി കാലങ്ങളിലെ കാര്യങ്ങളെല്ലാം അനുമാനിക്കപ്പെടുന്നു. പൃഛാസമയത്ത് ചോദ്യകര്‍ത്താവ് ഏതു രാശിയെ ആശ്രയിച്ചിരുന്നുവോ ആ രാശിയാണ് 'ആരൂഢം'. ഇവിടെ രാശികല്പന നടത്തുന്നത്, ജ്യൗതിഷിയുടെ ചുറ്റുമായി 12 രാശികള്‍ സങ്കല്പിച്ച്-മേടവും ഇടവവും കിഴക്കിലും മിഥുനം അഗ്നികോണിലും കര്‍ക്കടകവും ചിങ്ങവും തെക്കും, തെക്കു പടിഞ്ഞാറെ കോണില്‍ (കന്നിമൂല) കന്നിയും, പടിഞ്ഞാറുഭാഗത്ത് തുലാം വൃശ്ചികം രാശികളും വായുകോണില്‍ ധനുവും വടക്ക് മകരകുംഭങ്ങളും വടക്കുകിഴക്കേ കോണില്‍ മീനവും-അതില്‍ ഏതു രാശിയില്‍ പൃഛകന്‍ സ്ഥിതി ചെയ്തുകൊണ്ട് പൃഛ നടത്തുന്നുവോ അതാണ് ആരൂഢം. പൃഛാസംബന്ധമായ എല്ലാം ഈ ആരൂഢത്തില്‍ നിന്ന് അനുമാനിക്കാവുന്നതാണ്.

'ആരോഹത്വാത് പൃഛകേന

രാശിരാരൂഢ ഉച്യതേ

തസ്മിന്‍ സമ്യക് പരിജ്ഞാതേ

സര്‍വ്വം തേനൈവ ചിന്ത്യതാം'

(പ്രശ്നമാര്‍ഗം-അധ്യാ. 2 ശ്ലോ. 10)

പൃഛകന്‍ ശരീരത്തില്‍ ഏതു ഭാഗത്തു സ്പര്‍ശിച്ചുകൊണ്ടു പൃഛ നടത്തുന്നുവോ ആ അവയവത്തെ പ്രതിനിധീകരിക്കുന്ന രാശി ആരൂഢമായും പ്രശ്നവിചിന്തനം നടത്തുന്നു. പ്രഷ്ടാവ് ജ്യൗതിഷിയും കാഴ്ചവയ്ക്കുന്ന വെറ്റിലകളുടെ സംഖ്യയും അവ ഓരോന്നിന്റെയും ഗുണദോഷങ്ങളും പരിശോധിച്ചും പ്രശ്നം പറയാം (താംബൂല പ്രശ്നം). പ്രഷ്ടാവ് ജ്യൌതിഷിയോട് പറയുന്ന ആദ്യാക്ഷരം കൊണ്ടും ഒരു രാശി ആരൂഢമായി പ്രശ്നം പറയാം. തറയില്‍ രാശിചക്രം വരച്ച് അതില്‍ ദക്ഷിണാമൂര്‍ത്തിയെ സങ്കല്പിച്ച് പ്രഷ്ടാവിനെക്കൊണ്ട് സ്വര്‍ണം ഏതെങ്കിലും ഒരു രാശിയില്‍ വയ്പിച്ച് ആ രാശി ആരൂഢമായും പ്രശ്നം നടത്തുന്നു (സ്വര്‍ണാരൂഢം). ഇവയ്ക്കു പുറമേ, കവടികള്‍ ഉപയോഗിച്ചും ആരൂഢം എടുക്കുന്നു. 108 കവടികള്‍ ആണ് കണക്ക്. ജ്യൗതിഷി ഈശ്വരനെ ധ്യാനിച്ച് കവടികള്‍ ഒന്നിച്ച് ഒരു പലകയില്‍ വച്ച് അവയെ കൈകൊണ്ടു 'തടവി', ഒരുപിടി വാരി എടുത്തു വയ്ക്കുന്നു. അതില്‍ നിന്നു പന്ത്രണ്ടുവീതം മാറ്റിയാല്‍ ശേഷിക്കുന്നത് എത്ര സംഖ്യയാണോ, മേടം മുതല്‍ അത്രാമത്തെ രാശി ആരൂഢം. കവടികള്‍ കൊണ്ട് മറ്റൊരു രീതിയിലും പ്രശ്നം പറയാം. ഇതിന് അഷ്ടമംഗല്യപ്രശ്നം എന്നു പറയുന്നു. കേരളത്തിലാണ് പ്രശ്നം ഏറ്റവും പുരോഗതി പ്രാപിച്ചിട്ടുള്ളത്. പ്രശ്നത്തില്‍ ഗ്രഹങ്ങളുടെ കാരകത്വങ്ങള്‍ക്കു വളരെ പ്രാധാന്യമുണ്ട്. 

ജാതകത്തിലും പ്രശ്നത്തിലും ഗ്രഹങ്ങള്‍ക്കു കാരകത്വങ്ങള്‍ കല്പിക്കുന്നത് പഞ്ചഭൂതസിദ്ധാന്തത്തിന്റെ അടിസ്ഥാനത്തിലാണ്. ആകാശഭൂതത്തിനു ഗുരുവും വായുവിനു ശനിയും അഗ്നിക്കു കുജനും (രവിയും) ജലത്തിനു ശുക്രനും (ചന്ദ്രനും)പൃഥ്വിക്ക് ബുധനും കാരകന്മാരാണ്. അതുപോലെ സത്വഗുണത്തിന് രവി, ചന്ദ്രന്‍, ഗുരു എന്നീ ഗ്രഹങ്ങളും രജോഗുണത്തിന് ബുധശുക്രന്മാരും തമോഗുണത്തിന് കുജമന്ദന്മാരുമാണു പ്രതിനിധികള്‍. ഈ അടിസ്ഥാനത്തിലാണ് ഇതര കാരകത്വങ്ങള്‍ കല്പിക്കപ്പെടുന്നത്. നോ. ജാതകം; ഗ്രഹനില

(ഡോ. കെ.പി. ധര്‍മരാജ അയ്യര്‍)