= അകിടുവീക്കം =

Mastitis

കറവപ്പശുക്കളില്‍ ഉണ്ടാകാറുള്ള ഒരു രോഗം. എല്ലാ രാജ്യങ്ങളിലുമുള്ള പശുക്കള്‍ക്ക് ഈ രോഗം ബാധിക്കാറുണ്ട്. ചെമ്മരിയാടുകളിലും കോലാടുകളിലും ഈ രോഗം ഉണ്ടാകാം. കൂടുതല്‍ കറവയുള്ള പശുക്കളിലാണ് ഈ രോഗം അധികമായി കണ്ടുവരുന്നത്.
  
ഒന്നോ അതിലധികമോ തരം രോഗാണുക്കളുടെ ആക്രമണം മൂലം രോഗമുണ്ടാകുന്നു. രോഗബാധയ്ക്കു കാരണമാകുന്ന പ്രധാനപ്പെട്ട രോഗാണുക്കള്‍ ഇവയാണ്.
(i) സ്ട്രെപ്റ്റോകോക്കസ് അഗലാക്ടിയേ (Streptococcus agalactiae), (ii) സ്ട്രെപ്റ്റോകോക്കസ് ഡിസ്അഗലാക്ടിയേ (S.disagalatiae), (iii) സ്ട്രെപ്റ്റോകോക്കസ് യൂബെറിസ് (S.ubeiris), (iv) സ്ട്രെപ്റ്റോകോക്കസ് പയോജനിസ് (S.pyogenes), 

V) സ്ഫൈലോകോക്സൈ (Sphylococci), (VI) മൈക്രോബാക്റ്റീരിയം ടൂബര്‍ക്കുലോസിസ് (Microbacterium tuberculosis), (VII) ഫ്യൂസിഫോര്‍മിസ് നെക്രോഫോറസ് (Fusiformes necrophorus).
 
ഇവയില്‍ സ്ട്രെപ്റ്റോകോക്കസ് അഗലാക്ടിയേ എന്ന രോഗാണുവാണ് 80 ശ.മാ.-ത്തിലധികം രോഗബാധയ്ക്കും കാരണം. രോഗത്തെ തീവ്രതയനുസരിച്ച് ഉഗ്രം (acute), മിതോഗ്രം (Suvacute), മന്ദം (chronic) എന്നിങ്ങനെ മൂന്നായി തരംതിരിച്ചിട്ടുണ്ട്.

[[Image:akidu2.png|thumb|500x300px|left|രോഗബാധിതമായ അകിട്]]

അകിടിലും മുലക്കാമ്പുകളിലുമുണ്ടായേക്കാവുന്ന മുറിവുകളിലൂടെയാണ് രോഗാണുക്കള്‍ ഉള്ളിലേക്കു കടക്കുന്നത്. അകിടില്‍ നീരുവന്നു വീര്‍ക്കുകയാണ് ആദ്യലക്ഷണം. ക്രമേണ അകിടിലെ സംയോജകപേശികള്‍ വര്‍ധിച്ച് അകിടു കല്ലിച്ചുപോകുന്നു. ഇത്തരം അകിടുവീക്കത്തിനു ചിലദിക്കുകളില്‍ 'കല്ലകിട്' എന്നു പറയാറുണ്ട്. പാലില്‍ ആദ്യമായിക്കാണുന്ന മാറ്റം (സൂക്ഷിച്ചുനോക്കിയാല്‍ പോലും വളരെ വിഷമിച്ചു മാത്രമേ മനസിലാക്കാന്‍ കഴിയൂ) കുറച്ചു പാടത്തരികളുടെ ആവിര്‍ഭാവമാണ്. ക്രമേണ പാല് മഞ്ഞനിറമാകുകയും മഞ്ഞവെള്ളവും പിരിഞ്ഞ പീരയുമായി മാറുകയും ചെയ്യും. ചിലപ്പോള്‍ ചോരയും കണ്ടേക്കാം.
 
സ്ഫൈലോകോക്സൈ രോഗാണുക്കള്‍ 5 ശ.മാ.-ത്തോളം അകിടുവീക്കങ്ങള്‍ക്കു കാരണമാകുന്നു. അകിട് ആദ്യഘട്ടത്തില്‍ ചുവന്നു ചൂടുള്ളതായിരിക്കും; പാല് ആദ്യം വെള്ളം പോലെയും രക്തം കലര്‍ന്നതും ദുര്‍ഗന്ധമുള്ളതും ആയിരിക്കും. ഒന്നു രണ്ടു ദിവസങ്ങള്‍ക്കകം അകിട് പഴുക്കുകയും പാലിനു പകരം ചലം വരികയും ചെയ്യും.
 
രോഗത്തിന്റെ ബാഹ്യസ്വഭാവവും രോഗകാരണങ്ങളായ അണുപ്രാണികളും വ്യത്യസ്തങ്ങളാകാമെങ്കിലും അകിട് വീങ്ങുകയും പാലില്‍ മാറ്റങ്ങളുണ്ടാവുകയുമാണ് അകിടുവീക്കത്തിന്റെ അടിസ്ഥാന ലക്ഷണങ്ങള്‍.

അകിടു വൃത്തിയായി സൂക്ഷിക്കുക, അകിടില്‍ മുറിവും പോറലും വരാതെ നോക്കുക, തൊഴുത്തും പരിസരങ്ങളും ശുചിയായി വയ്ക്കുക എന്നിവ രോഗപ്പകര്‍ച്ചയ്ക്കുള്ള സാധ്യതകളെ കുറയ്ക്കും. കറവക്കാരുടെ കൈകള്‍ കറവയ്ക്കുമുമ്പും പിമ്പും രോഗാണുനാശിനികളെക്കൊണ്ടു കഴുകുന്നതിലും കറവ കഴിഞ്ഞാല്‍ അകിടു കഴുകി വൃത്തിയായി സൂക്ഷിക്കുന്നതിലും ശ്രദ്ധിക്കണം.

അകിടുവീക്കം നിയന്ത്രിക്കുന്നതിന് മേല്‍പറഞ്ഞ ശുചിത്വം ഒരു പ്രധാന ഘടകമാണ്.
 
രോഗമുള്ള പശുക്കളെ പ്രത്യേകം മാറ്റി നിര്‍ത്തി കറക്കുകയോ അവസാനം കറക്കുകയോ ചെയ്യുന്നതാണ് നല്ലത്. അല്ലെങ്കില്‍ മറ്റു പശുക്കള്‍ക്ക് രോഗം പകരാനുള്ള സാധ്യത കൂടുതലാണ്. അകിടിലെ പാല്‍ മുഴുവനും കറക്കാതെ കെട്ടി നില്‍ക്കുകയാണെങ്കില്‍ അകിടുവീക്കം ഉണ്ടാകാനുള്ള സാധ്യതയുണ്ട്. കറവ വറ്റുന്ന സമയത്ത് പ്രത്യേകമായി നിര്‍മിച്ചിട്ടുള്ള മരുന്നുകള്‍ കാമ്പിനുള്ളില്‍ ഉപയോഗിക്കുന്നതുവഴി അകിടുവീക്കം ഒരു പരിധിവരെ നിയന്ത്രിക്കാന്‍ കഴിയും.
 
മിക്ക പശുക്കളിലും പ്രസവത്തോടനുബന്ധിച്ചോ അതിന് ഒരാഴ്ച മുമ്പോ പിമ്പോ ആണ് അകിടുവീക്കം കൂടുതലായി കാണുന്നത്. ഈ സമയത്ത് തൊഴുത്തും പരിസരവും വളരെ വൃത്തിയായി സൂക്ഷിക്കേണ്ടതുണ്ട്. വിസര്‍ജ്ജ്യങ്ങള്‍ യഥാസമയം മാറ്റാതെ വരുമ്പോള്‍ അതിനു പുറത്ത് പശു കിടക്കാനിടയാകുകയും മുലക്കാമ്പുകള്‍ വഴി രോഗാണുക്കള്‍ കടന്ന് രോഗമുണ്ടാകുകയും ചെയ്യും. എ,ഇ എന്നീ ജീവകങ്ങള്‍, മറ്റു ധാതുലവണങ്ങള്‍ എന്നിവ നല്‍കുന്നത് രോഗസാധ്യത കുറയ്ക്കുന്നതായി പഠനങ്ങള്‍ തെളിയിച്ചിട്ടുണ്ട്.
 
രോഗമുണ്ടെന്നു സംശയം തോന്നുന്ന പശുക്കളെ ഉടന്‍തന്നെ വിദഗ്ധമായ ചികിത്സയ്ക്കു വിധേയമാക്കണം. പെനിസിലിന്‍, സ്ട്രെപ്റ്റോമൈസിന്‍, ആറിയോമൈസിന്‍, ജെന്റാമൈസിന്‍, ക്ളോറാംഫിനിക്കോള്‍, എന്റോഫ്ളോക്സാഡിന്‍, അമോക്സിസില്ലിന്‍, ക്ളോക്സാസില്ലിന്‍ മുതലായ ആന്റിബയോട്ടിക്കുകളും സല്‍ഫാ മരുന്നുകളും ഫലപ്രദമായ പ്രതിവിധികളാണ്.
 
പാലിലെ പാടത്തരികള്‍ ആദ്യമേ കണ്ടെത്തുവാന്‍ സഹായിക്കുന്ന സ്ട്രിപ്പ്കപ്പ് (Strip cup), പ്രത്യേക ഡൈ(Dye)കളില്‍ പാല്‍ ഉണ്ടാക്കുന്ന വര്‍ണവ്യത്യാസങ്ങളില്‍ നിന്നും രോഗബാധ നിര്‍ണയിക്കുവാന്‍ സഹായിക്കുന്ന 'മാസ്റ്റൈറ്റിസ് കാര്‍ഡുകള്‍' എന്നിവ പൊതുവായ രോഗനിര്‍ണയത്തിനുള്ള ഉപാധികളാണ്. സൂക്ഷ്മദര്‍ശനികൊണ്ടുള്ള പരിശോധനയില്‍ മാത്രമേ രോഗകാരികളായ അണുപ്രാണികളെ മനസിലാക്കാന്‍ സാധിക്കുകയുള്ളു.

കാലിഫോര്‍ണിയന്‍ മാസ്റ്റൈറ്റിസ് ടെസ്റ്റ് എന്ന ടെസ്റ്റ് വഴി ഒരു പ്രത്യേക ലായിനി ഉപയോഗിച്ച് പാല്‍ പരിശോധിക്കുന്നത് അകിടുവീക്കം തുടക്കത്തിലേ തന്നെ മനസ്സിലാക്കാന്‍ സഹായിക്കും. ഇത് കര്‍ഷകര്‍ക്ക് സ്വന്തമായി വീടുകളില്‍ ചെയ്യാന്‍ കഴിയുന്ന ഒരു പരിശോധനാരീതിയാണ്. തുടക്കത്തിലേ രോഗബാധ ഉണ്ടെന്ന് കണ്ടെത്താന്‍ കഴിഞ്ഞാല്‍ ചികിത്സാപ്രയോഗങ്ങള്‍ വിജയകരമായിത്തീരുകയും അസുഖം മൂലമുണ്ടാകുന്ന നഷ്ടം കുറയ്ക്കുവാന്‍ കഴിയുകയും ചെയ്യും. യഥാര്‍ഥ അണുപ്രാണികളെ മനസ്സിലാക്കാന്‍ കഴിഞ്ഞാല്‍ അതിനെതിരെയുള്ള കൃത്യമായ മരുന്നുപയോഗിച്ച് ചികിത്സ വളരെ ഫലപ്രദമാക്കിത്തീര്‍ക്കാന്‍ സാധിക്കും.

(ഡോ. ആര്‍. ഗോപാലകൃഷ്ണന്‍ നായര്‍, 
ഡോ. കെ. രാധാകൃഷ്ണന്‍)
[[Category:ജന്തുശാസ്ത്രം-രോഗം]]