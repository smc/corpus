= അഗ്നിപര്‍വതം =

തിളച്ചുരുകിയ മാഗ്മ (Magma) ദ്രവരൂപത്തിലോ, ബാഷ്പമായോ, രണ്ടും ചേര്‍ന്നോ വന്‍തോതില്‍ ബഹിര്‍ഗമിക്കുന്ന ഭൂവല്കച്ഛിദ്രം (Crustal Vent). മിക്കപ്പോഴും ഇവ ഉയര്‍ന്ന കുന്നുകളുടെയോ പര്‍വതങ്ങളുടെയോ രൂപത്തിലായിരിക്കും.

==ആമുഖം== 

ഭൂവല്കത്തിനടിയില്‍ ഉഗ്രമായ ചൂടുമൂലം (3,000<sup>o</sup>C) തിളച്ചുമറിയുന്ന ശിലാദ്രവം (മാഗ്മ) ആണുളളത്. ഭൂവല്കത്തിലെ വിടവുകളിലേക്ക് ഇവ തള്ളിക്കയറുന്നു. പെട്ടെന്നുണ്ടാകുന്ന മര്‍ദക്കുറവില്‍ മാഗ്മയില്‍ സഞ്ചിതമായിട്ടുള്ള ബാഷ്പങ്ങള്‍ - വിശിഷ്യ നീരാവി-സ്വതന്ത്രമായി മാഗ്മയുടെ ഉപരിതലത്തില്‍ തിങ്ങിക്കൂടുന്നു. ഇവയുടെ സമ്മര്‍ദഫലമായി ഭൂപാളിപിളര്‍ന്നു മാഗ്മ വമിക്കുന്നു. ദ്രവരൂപത്തില്‍ ബഹിര്‍ഗമിക്കുന്ന മാഗ്മയാണ് ലാവ. ഉദ്ഗാരഫലമായി മാഗ്മ വളരെ ഉയരത്തില്‍ ചുഴറ്റി എറിയപ്പെടുന്നു. ഇവ വലിയ ശിലാഖണ്ഡങ്ങള്‍ മുതല്‍ ചെറുകണങ്ങളും തരികളും വരെയായി വിവിധ വലുപ്പത്തില്‍ ചിതറിവീഴുന്നു; ധൂമപടലങ്ങളും ഇതിന്റെകൂടെയുണ്ടാകാം. സാന്ദ്രമായ നീരാവി-ശിലാധൂളിയുമായി കലര്‍ന്നുണ്ടാകുന്ന ഇരുണ്ട വിഷമയപദാര്‍ഥങ്ങള്‍ ധൂമപടലമായി പ്രത്യക്ഷപ്പെടുന്നു. ചിലപ്പോള്‍ ഇവ മേഘപാളിപോലെ കാണപ്പെടും. സ്ഫോടനഫലമായോ അതിനുമുമ്പോ നേരിയതോതിലുള്ള ഭൂകമ്പങ്ങളും അനുഭവപ്പെടാം.

വിലമുഖത്തിലൂടെ നാലുപാടും പരന്നൊഴുകുന്ന ലാവയും ചുറ്റുപാടും ചിതറിവീഴുന്ന ശിലാഖണ്ഡങ്ങളും അടിഞ്ഞുകൂടി സ്തൂപികാകാരമായ കുന്നുകള്‍ക്കു രൂപം നല്കുന്നു; ചിലപ്പോള്‍ പ്രോത്ഥാനശക്തികളുടെ പിന്തുണയോടെ ഇവ വന്‍മലകളായി വളരാനും ഇടയുണ്ട്. ഇവയുടെ മധ്യത്തുള്ള വിലമുഖം നാളീരൂപത്തില്‍ അത്യഗാധമായിരിക്കും. ഇതിനെയാണ് 'അഗ്നിപര്‍വത വക്ത്രം ' എന്നു പറയുന്നത്. ഒരേ അഗ്നിപര്‍വതത്തിനു തന്നെ ഒന്നിലധികം വിലമുഖങ്ങളുണ്ടായി എന്നു വരാം.

ഭൌമപ്രക്രിയകളില്‍ മറ്റുള്ള ഒന്നുംതന്നെ അഗ്നിപര്‍വതങ്ങളോളം മനുഷ്യന്റെ ശ്രദ്ധ ആകര്‍ഷിച്ചിട്ടില്ല. അത്രയ്ക്കു രൂക്ഷവും വിനാശകരവുമായ പ്രതിഭാസങ്ങളാണിവ. എ.ഡി. 79-ശ.-ത്തില്‍ പൊട്ടിത്തെറിച്ച്, ഹെര്‍ക്കുലേനിയം, പൊംപേയ് തുടങ്ങിയ നഗരങ്ങളെ ഒന്നാകെ നശിപ്പിച്ചുകളഞ്ഞ വെസൂവിയന്‍ സ്ഫോടനത്തെസംബന്ധിച്ച വിവരണം റോമന്‍ചരിത്രരേഖകളിലുണ്ട്. പുകയും തീയും വമിപ്പിച്ച് എരിഞ്ഞുകൊണ്ടിരിക്കുന്ന ഒരു പര്‍വതമായിട്ടാണ് അഗ്നിപര്‍വതത്തെ പൊതുവേ ധരിച്ചിരുന്നത്. ഈ ധാരണയെ ഭൂവിജ്ഞാനികള്‍ തിരുത്തിയിരിക്കുന്നു. തടികളും മറ്റും കത്തുന്നതുപോലെ സാധാരണ ജ്വലനത്തിന് ഇവിടെ പ്രസക്തിയില്ല. ചിലപ്പോള്‍ വിലമുഖങ്ങളില്‍ തീജ്വാലയുണ്ടാകാം. ആന്തരികമായ ചൂടുകൊണ്ടല്ല ഇതുസംഭവിക്കുന്നത്; ബാഷ്പങ്ങള്‍ വായുവുമായി ഇടകലരുമ്പോഴുണ്ടാകുന്ന ഉരസല്‍ മൂലം തീ കത്തുന്നതാണ്. അഗ്നിപര്‍വതപ്രക്രിയ പര്‍വതാഗ്രങ്ങളില്‍നിന്നാകണമെന്നില്ല. മിക്ക അഗ്നിപര്‍വതങ്ങളും സ്ഫോടനത്തിനുശേഷം ഉയര്‍ന്നുവന്നിട്ടുള്ളവയാണ്. നേപ്പിള്‍സ് ഉള്‍ക്കടലിന് അഭിമുഖമായി നില്ക്കുന്ന വെസൂവിയസ് ഇതിനുദാഹരണമാണ്. പണ്ട് ഒരു അഗ്നിപര്‍വത ദ്വീപായിരുന്ന ഇത് ഉദ്ഗാരഫലമായി കടല്‍ നികന്നു കരയുമായി ബന്ധിക്കപ്പെട്ടു.

അഗ്നിപര്‍വതപ്രക്രിയയെ അധികരിച്ചു പല മൂഢവിശ്വാസങ്ങളും പുരാതനകാലത്ത് നിലനിന്നിരുന്നു. റോമന്‍ വിശ്വാസമനുസരിച്ച് അഗ്നിപര്‍വതം അഗ്നിദേവനായ വള്‍ക്കന്റെ ഉലയായിരുന്നു. അഗ്നിപര്‍വതത്തിന്റെ ആംഗലരൂപമായ 'വള്‍ക്കാനോ' (Volcano) ഈ വിശ്വാസത്തില്‍ നിന്നും നിഷ്പന്നമാണ്. 

ആഗ്നേയ പ്രക്രിയ(Igneous Activity)യില്‍ ഒരിനമാണ് അഗ്നിപര്‍വതോദ്ഗാരം; മറ്റേ ഇനം അന്തര്‍വേധനവും (Intrusion). ഭൂവല്കത്തിലെ ശിലകളുടെ അടിയില്‍ വിദരങ്ങളും വിടവുകളും സൃഷ്ടിച്ചു തിളച്ചുരുകിയ ശിലാദ്രവം മുകളിലേക്കിരച്ചുകയറുന്ന പ്രക്രിയയാണ് അന്തര്‍വേധനം. ഇതില്‍ അഗ്നിപര്‍വതത്തിലെപ്പോലെ മാഗ്മ ബഹിര്‍ഗമിക്കുന്നില്ല.

==ഉദ്ഗാരങ്ങള്‍== 

അഗ്നിപര്‍വതോദ്ഗാരങ്ങള്‍ സാധാരണയായി രണ്ടുതരത്തിലാണ് കണ്ടുവരുന്നത്. ഉരുകി ഉയരുന്ന മാഗ്മ, അഗ്നിപര്‍വതവക്ത്രത്തിന്റെ വശങ്ങള്‍ കവിഞ്ഞുപടര്‍ന്നും ശാന്തമായും ഒഴുകുന്ന ലാവാപ്രവാഹങ്ങള്‍ സൃഷ്ടിക്കുന്നതാണ് ആദ്യത്തെ ഇനം. ചിലപ്പോള്‍ അടിയിലുള്ള വാതകങ്ങളുടെ തള്ളല്‍നിമിത്തം മാഗ്മ അനേകം മീറ്റര്‍ ഉയരത്തില്‍ പ്രസ്രവണങ്ങളായി (fountains) കുതിക്കുന്നു. ലാവാപ്രവാഹങ്ങളില്‍തന്നെ വിലീനമായ വാതകങ്ങള്‍ കുമിളകളായി ഉയര്‍ന്നു വായുവില്‍ ലയിക്കുന്നു; ഇത്തരം അഗ്നിപര്‍വതങ്ങള്‍ ലാവാപ്രവാഹത്തില്‍പ്പെടുന്ന വസ്തുക്കള്‍ക്കു മാത്രമേ നാശനഷ്ടങ്ങള്‍ വരുത്തുന്നുള്ളു.

അഗ്നിപര്‍വതനാളങ്ങളില്‍ ഭയങ്കരമായ പൊട്ടിത്തെറി ഉണ്ടാകുകയും വാതകസമ്മിശ്രമായ ലാവ പതഞ്ഞുയര്‍ന്നു ബഹിര്‍ഗമിക്കുകയും ചെയ്യുന്നതാണ് രണ്ടാമത്തെ ഇനം. ഈ പത ഖരീഭവിച്ചു ചാരമായും 'പ്യൂമിസ്' (pumice) ആയും തെറിച്ചുവീഴുന്നു. മാഗ്മ ദ്രവരൂപത്തില്‍ ഒഴുകുന്നതിനുപകരം ഖരാവസ്ഥയിലുള്ള പൈറോക്ളാസ്റ്റികങ്ങളായി (Pyroclastic materials) ചുഴറ്റി എറിയപ്പെടുന്നു. ഇവ ചിതറിവീഴുന്നത് ധാരാളം നാശനഷ്ടങ്ങള്‍ക്ക് ഇടയാക്കും.

ഏറിയകൂറും അഗ്നിപര്‍വതസ്ഫോടനങ്ങള്‍ മേല്‍പ്പറഞ്ഞ രണ്ടിനങ്ങള്‍ക്കും ഇടയ്ക്കായിരിക്കും. ഇടത്തരം സ്ഫോടനത്തോടെ ലാവയും പൈറോക്ളാസ്റ്റികങ്ങളും വാതകങ്ങളും ബഹിര്‍ഗമിക്കുന്നു.

==സ്ഫോടനോത്പന്നങ്ങള്‍== 

അഗ്നിപര്‍വതോത്പന്നങ്ങളെ വാതകങ്ങള്‍, ദ്രവമാഗ്മ, പൈറോക്ളാസ്റ്റികങ്ങള്‍ എന്നിങ്ങനെ വിഭജിക്കാം.
===വാതകങ്ങള്‍=== 

ഉദ്ഗാരങ്ങളുടെ ഭാഗമായി വാതകങ്ങളും  ബാഷ്പങ്ങളും വന്‍തോതില്‍ ബഹിര്‍ഗമിക്കുന്നു. ഉഗ്രമായ സ്ഫോടനത്തോടനുബന്ധിച്ചുണ്ടാകുന്ന വാതകങ്ങള്‍ ധൂളീമാത്രങ്ങളായ പൈറോക്ളാസ്റ്റികങ്ങളുമായി കലര്‍ന്ന് ധൂമപടലംപോലെയോ, മേഘംപോലെയോ വ്യാപിക്കുന്നു; ഇവ മിക്കവാറും വിഷലിപ്തവുമായിരിക്കും. ഉയര്‍ന്ന ഊഷ്മാവില്‍ വമിക്കുന്ന ഈ വാതകങ്ങളെ സംബന്ധിച്ച പഠനം പല പ്രശ്നങ്ങളും സൃഷ്ടിച്ചു; ഇന്നും ഇവയെക്കുറിച്ചുള്ള അറിവ് അപൂര്‍ണമാണ്. അന്തരീക്ഷവായുവുമായുള്ള സമ്പര്‍ക്കത്തില്‍ ചുവപ്പും നീലയും ജ്വാലകളില്‍ കത്തുന്നതാണ് ദഹനസ്വഭാവമുള്ള വാതകങ്ങളുടെ സാന്നിധ്യം തെളിയിച്ചത്. വാതകങ്ങളുടെ കൂട്ടത്തില്‍ ഏതാണ്ട് 70ശ.മാ.വും നീരാവിയാണ്. ഇതില്‍ മാഗ്മീയജലത്തിന്റെയും ഭൂഗര്‍ഭജലത്തിന്റെയും (Ground water) അളവുകള്‍ തിട്ടപ്പെടുത്താനാവില്ല. ഹൈഡ്രജന്‍ക്ളോറൈഡ്, ഹൈഡ്രജന്‍സള്‍ഫൈഡ്, ഹൈഡ്രജന്‍, കാര്‍ബണ്‍ഡൈഓക്സൈഡ്, കാര്‍ബണ്‍മോണോക്സൈഡ്, സള്‍ഫര്‍ട്രൈഓക്സൈഡ്, സള്‍ഫര്‍ഡൈഓക്സൈഡ്, ഹൈഡ്രജന്‍ ഫ്ളൂറൈഡ് തുടങ്ങിയവയാണ് മറ്റു മുഖ്യവാതകങ്ങള്‍. മീഥെയിന്‍, അമോണിയ, ഹൈഡ്രജന്‍തയോസയനൈറ്റ്, നൈട്രജന്‍, ആര്‍ഗണ്‍ തുടങ്ങിയവയും സള്‍ഫര്‍ ബാഷ്പവും നേരിയ തോതില്‍ കണ്ടുവരുന്നു. ആല്‍ക്കലിലോഹങ്ങള്‍, അയണ്‍ എന്നിവയുടെ ക്ളോറൈഡുകളും ചിലപ്പോള്‍ കാണാറുണ്ട്. അഗ്നിപര്‍വതത്തില്‍നിന്നും ഉദ്ഗമിക്കുന്ന മൂലവാതകങ്ങള്‍ അന്തരീക്ഷ ഓക്സിജനുമായി പ്രതിപ്രവര്‍ത്തിച്ചാണ് കാര്‍ബണ്‍, സള്‍ഫര്‍ തുടങ്ങിയവയുടെ ഓക്സൈഡുകളും നീരാവിയുടെ ഒരംശവും ഉത്പാദിതമാകുന്നതെന്നും അഭിപ്രായമുണ്ട്. എല്ലാ അഗ്നിപര്‍വതങ്ങളില്‍നിന്നും ഒരേ വാതകങ്ങള്‍തന്നെ ബഹിര്‍ഗമിക്കണമെന്നില്ല.

===ലാവാപ്രവാഹങ്ങള്‍=== 

ഖരാങ്കത്തില്‍നിന്നും അധികം ഉയര്‍ന്നതല്ലാത്ത ഊഷ്മാവില്‍ ബഹിര്‍ഗമിക്കുന്ന ദ്രവമാഗ്മയാണ് ലാവ. രാസസംയോഗം, വിലീനവാതകങ്ങളുടെ അളവ് എന്നിവയെ ആശ്രയിച്ച് ഇതിന്റെ താപനില 900<sup>o</sup>C-നും 1,200<sup>o</sup>C-നും ഇടയ്ക്കായിരിക്കും. 600<sup>o</sup>C-നും 900<sup>o</sup>C-നും ഇടയ്ക്കാണ് ലാവ ഖരീഭവിക്കുന്നത്. ഭൂവല്ക്കത്തെ ഭേദിച്ചുകൊണ്ടു ബഹിര്‍ഗമിക്കുമ്പോള്‍തന്നെ, മാഗ്മയിലെ ചില ഘടകങ്ങള്‍ താപ-മര്‍ദഭേദങ്ങളുടെ ഫലമായി ബാഷ്പീകരിക്കുന്നു. വാതകങ്ങള്‍ സ്വതന്ത്രമാകുന്നത് മാഗ്മയുടെ ശ്യാനതയെ (viscosity) ബാധിക്കുന്നു. ലാവാപ്രവാഹത്തിന്റെ രൂപഭാവങ്ങള്‍ നിര്‍ണയിക്കുന്നത് പ്രധാനമായും അതിന്റെ ശ്യാനതയാണ്. തണുക്കുന്നതിന്റെ തോതും പ്രതലത്തിന്റെ ചരിവുമാണ് ഇതിനെ സ്വാധീനിക്കുന്ന മറ്റു ഘടകങ്ങള്‍. ലാവാപാളികളിലെ ഊഷ്മാവ് അടിയില്‍നിന്നും മുകളിലേക്കു കുറഞ്ഞുവരുന്നതുനിമിത്തം ഉപരിതലത്തിലെ ലാവ താരതമ്യേന ശ്യാനവും താഴത്തേത് കൂടുതല്‍ ഗതിശീലവുമാകാം. ലാവയുടെ ഉപരിതലം വികൃതപ്പെടുവാന്‍ ഇതു കാരണമാകുന്നു; പെട്ടെന്നു തണുത്തു മിനുസമേറിയ പ്രതലം ഉണ്ടാകാനും മതി. ഇത് അഗ്നിപര്‍വതസ്ഫടികത്തിന്റെ (volcanic glass) ഉത്പാദനത്തിനു ഹേതുവായിത്തീരുന്നു. ലാവയുടെ ചേരുവ ഒന്നുപോലാണെങ്കിലും ഘടകങ്ങളുടെ സംയോഗാനുപാതം വ്യത്യസ്തമാകാം. സിലിക്കയുടെ അംശം 25 ശ.മാ. മുതല്‍ 75 ശ.മാ. വരെ വ്യതിചലിച്ചുകാണുന്നു. സിലിക്ക അധികമാകുന്നത് ലാവയ്ക്കു കൂടുതല്‍ മുറുക്കം നല്കുന്നു. ഉയര്‍ന്ന ഊഷ്മാവില്‍പോലും ഇവയുടെ ശ്യാനത കൂടിയിരിക്കും. ഇവ എളുപ്പം ഒലിച്ചുപോകുന്നില്ല. ചിലപ്പോള്‍ ഇത്തരം ലാവ അഗ്നി പര്‍വതനാളിയില്‍തന്നെ കട്ടിപിടിച്ചു പ്രവാഹത്തിനു പ്രതിബന്ധമായിത്തീരുന്നു. ഇതിന്നടിയിലായി തിങ്ങിക്കൂടുന്ന വാതകങ്ങള്‍ ഊക്കോടെ പുറത്തേക്കുവരുമ്പോഴാണ് അത്യുഗ്രമായ സ്ഫോടനമുണ്ടാകുന്നത്. അഗ്നിപര്‍വതവക്ത്രത്തിന്റെ ശിലാഭിത്തികള്‍പോലും ഈ പൊട്ടിത്തെറിയില്‍ പങ്കുചേരുന്നു. ശ്യാനതകൂടിയ ലാവ അധികദൂരം ഒഴുകുന്നില്ല. ഇങ്ങനെയുണ്ടാകുന്ന ലാവാതലത്തിന്റെ അറ്റത്തു ധാരാളം മുനമ്പുകള്‍ കാണുന്നു. ഇത്തരം ലാവ മങ്ങിയ നിറത്തോടു കൂടിയതായിരിക്കും.

സിലിക്കാംശം കുറവുള്ള ലാവയുടെ ഉദ്ഗാരം പ്രായേണ ശാന്തമാണ്. ഗതിശീലങ്ങളായ ഇവ രണ്ടുരീതിയിലുള്ള ലാവാപ്രവാഹങ്ങള്‍ക്ക് ഇടയാക്കുന്നു. ലാവയുടെ മുകള്‍ഭാഗം കട്ടിയാകുമ്പോഴും അടിഭാഗം ഒഴുകിക്കൊണ്ടിരിക്കും. തന്‍മൂലം കട്ടിപിടിച്ചഭാഗം വിണ്ടുകീറി കട്ടകളായിത്തീരുന്നു. പെട്ടെന്നുറയുന്നതുമൂലം പരുപരുത്ത തലങ്ങളായിരിക്കും ഇവയ്ക്കുണ്ടായിരിക്കുക. എന്നാല്‍ പരന്നൊഴുകുമ്പോള്‍ വിലീനവാതകങ്ങള്‍ കുമിളകളായി രക്ഷപ്പെട്ടശേഷം തണുത്തു കട്ടിയാകുന്ന ലാവയുടെ മിനുസമായ പുറന്തോട് കയര്‍പോലെ പിരിവുകളോടെ കാണപ്പെടുന്നു. ഇവയെ യഥാക്രമം ഖണ്ഡ ലാവ (Block Lava) എന്നും കയര്‍ ലാവ (Rope Lava) എന്നും പറയുന്നു; ആആ ലാവ (AA Lava), പാഹോഹോ ലാവ (Pahoehoe Lava) എന്നീ ഹവായിയന്‍ പേരുകളും പ്രചാരത്തിലുണ്ട്. കയര്‍ ലാവ ഖണ്ഡ ലാവയെക്കാള്‍ സാവധാനത്തിലാണ് തണുക്കുന്നത്.

സമുദ്രത്തിന്റെയോ ജലാശയങ്ങളുടെയോ അടിത്തട്ടില്‍ ലാവാ ഉദ്ഗാരമുണ്ടാകുമ്പോള്‍ തലയണകള്‍ നിരത്തിയിട്ടതുപോലുള്ള ആകൃതിയില്‍ പെട്ടെന്നു തണുത്ത് ഉറയുന്നു. ഇവയെ തലയണലാവ (Pillow Lava) എന്നു പറഞ്ഞുവരുന്നു. പസിഫിക് സമുദ്രത്തിലെ അഗ്നിപര്‍വതദ്വീപുകള്‍ക്കടുത്ത് ഇത്തരം ഘടന ധാരാളമായി കാണാം. ഭൂഅഭിനതികളുമായി (Geosynclines) ബന്ധപ്പെട്ടും ഇവയെ കണ്ടെത്തിയിട്ടുണ്ട്.

അഗാധതലങ്ങളിലുണ്ടാകുന്ന ഉദ്ഗാരങ്ങളെപ്പറ്റി നേരിട്ടുള്ള അറിവുകള്‍ പരിമിതമാണ്. രാസസംയോഗത്തില്‍ ഇവയ്ക്കു ബസാള്‍ട്ടിനോടു സാദൃശ്യമുണ്ട്. എന്നാല്‍ ധാതു സംയോഗത്തില്‍ വ്യത്യസ്തവുമാണ്. ഇവയെ സ്പിലൈറ്റ് (Spilite) എന്നു പറയുന്നു.

നിശ്ചലമായി തളംകെട്ടിനിന്നു ഘനീഭവിക്കുന്ന ലാവാ ഘടനകളുടെ അടിഭാഗം സ്തംഭാകാരമായി കാണപ്പെടുന്നു (columnar structure). ഇവ തരിമയവും വിലീനവാതകങ്ങളുടെ അഭാവംകൊണ്ടു സവിശേഷവുമായ പ്ളേറ്റോ ബസാള്‍ട്ടിന്റെ ഏകരൂപഘടനകളാണ്. ഡെക്കാണ്‍ പീഠഭൂമിയിലെ 5,20,000 ച.കി.മീ. സ്ഥലത്ത് ഇത്തരം സ്തംഭാകാരലാവയാണുളളത്. ലോകത്തിന്റെ നാനാഭാഗങ്ങളിലും ഇത്തരം ലാവാപീഠഭൂമികള്‍ കാണാം. കഴിഞ്ഞ 18 കോടി വര്‍ഷങ്ങള്‍ക്കിടയില്‍ 42 ലക്ഷം ഘ.കി.മീ. ലാവാശിലകള്‍ രൂപം കൊണ്ടിട്ടുണ്ടെന്ന് കണക്കാക്കപ്പെടുന്നു.

അഗ്നിപര്‍വതനാളത്തില്‍വച്ചുതന്നെ വാതകങ്ങളുമായി കലര്‍ന്നു പതഞ്ഞുയരുന്നതാണ് ലാവാനുര (Lava forth). ഏറ്റവും ശ്യാനവും ഊഷ്മളവുമായ ലാവ കുത്തനെയുള്ള ചരിവുകളില്‍ മണിക്കൂറില്‍ 50 മുതല്‍ 65 കി.മീ. വരെ വേഗത്തിലൊഴുകുന്നു. എന്നാല്‍ സിലിക്ക അധികമുള്ള ലാവ വളരെ സാവധാനത്തില്‍ മാത്രമേ ഇറങ്ങുന്നുള്ളു. ഇവയുടെ ഉപരിതലം ഉറയുകയും ഒപ്പംതന്നെ അടിവശം ഒഴുകുകയും ചെയ്യും. ഇതു വൈവിധ്യമുളള പ്രതലസംരചനയ്ക്കു കാരണമാകുന്നു. ഉറഞ്ഞുകൂടിയ ലാവാതലങ്ങള്‍ക്കിടയില്‍ വലിയ തുരങ്കങ്ങള്‍ ഉണ്ടാകുന്നതും സാധാരണയാണ്. ചിലപ്പോള്‍ ഇവയുടെ മുകള്‍ത്തട്ട് ഇടിഞ്ഞമര്‍ന്ന് അഗാധങ്ങളായ കിടങ്ങുകളായിത്തീരുന്നു.

===പൈറോക്ളാസ്റ്റികങ്ങള്‍=== 

ഖരരൂപത്തിലുള്ള അഗ്നിപര്‍വതോത്പന്നങ്ങളാണിവ. പൈറോക്ളാസ്റ്റികങ്ങള്‍ നന്നെ ചെറിയതരികള്‍ മുതല്‍ ടണ്‍കണക്കിനു ഭാരമുളള വലിയ ശിലാഖണ്ഡങ്ങള്‍ വരെ വിവിധവലുപ്പങ്ങളില്‍ കാണപ്പെടുന്നു. സ്ഫോടനത്തിന്റെ ശക്തി, കാറ്റിന്റെ ഗതിവേഗം, വലുപ്പം തുടങ്ങിയവയെ ആശ്രയിച്ച് ഇവ അഗ്നിപര്‍വതവക്ത്രത്തിനുചുറ്റും അടുത്തോ ദൂരയോ ആയി വീഴുന്നു. ചിലപ്പോള്‍ വലിയ ഖണ്ഡങ്ങള്‍ അഗ്നിപര്‍വത വക്ത്ര(Crater)ങ്ങളിലേക്കുതന്നെ വീണെന്നും വരാം. ക്രേറ്ററിന്റെ വശങ്ങളിലായിവീണ് ഉരുണ്ടുനീങ്ങുന്നവയുമുണ്ട്. ഇവ പിന്നീട് ജ്വാലാശ്മസഞ്ചയമോ (agglomerate) അഗ്നിപര്‍വത ബ്രക്ഷ്യയോ (Volcanic Breccia) ആയി രൂപം കൊള്ളുന്നു.

3 സെ.മീ മുതല്‍ അനേകം മീ. വരെ വ്യാസമുള്ള പൈറോക്ളാസ്റ്റിക് പിണ്ഡങ്ങളാണ് അഗ്നിപര്‍വത ബോംബുകള്‍. ഇവ ഖരരൂപത്തില്‍തന്നെ വിക്ഷേപിക്കപ്പെടുന്ന മാഗ്മാകഷണങ്ങളാകാം. ചിലപ്പോള്‍ വായുമണ്ഡലത്തിലേക്ക് ചുഴറ്റി എറിയപ്പെടുമ്പോള്‍ ഉരുണ്ടുകൂടുന്നതുമാകാം. മിക്കവാറും ഇവയ്ക്കു സരന്ധ്രഘടനയായിരിക്കും ഉണ്ടാവുക. ഭൌമോപരിതലത്തിലെത്തുന്നതിനു മുന്‍പുതന്നെ ഇവ ബാഹ്യമായിട്ടെങ്കിലും ഖരീഭവിച്ചിരിക്കും; ചിലപ്പോള്‍ നീണ്ടുരുണ്ടവയായും കൂമ്പിയും കാണപ്പെടുന്നു; അപൂര്‍വമായി ക്രമരൂപമില്ലാത്തവയും കാണാം. ആകാശത്തുവച്ചുതന്നെ ഘനീഭവിച്ചു സരന്ധ്രങ്ങളായി കാണപ്പെടുന്ന ലാവാക്കഷണങ്ങളെ 'സ്കോറിയ' (Scoria) എന്നു പറയുന്നു. 0.3 മി.മീ. മുതല്‍ 5 മി.മീ. വരെ വലുപ്പമുളളവ 'ചാര'മായും അതിലും സൂക്ഷ്മങ്ങളായവ 'ധൂളി' ആയും വിശേഷിപ്പിക്കപ്പെടുന്നു. (നോ: അഗ്നിപര്‍വതച്ചാരം). സുഷിരങ്ങളില്ലാതെ പയര്‍മണികളെപ്പോലെയുള്ള ചെറിയ കഷണങ്ങളാണ് 'ലാപ്പിലി' (Lappili). പ്യൂമിസ് (Pumice) ആണ് മറ്റൊരിനം. ശ്യാനതകൂടി രന്ധ്രമയവും വാതകസാന്നിധ്യമുള്ളതുമായ മാഗ്മാക്കഷണങ്ങളാണ് പ്യൂമിസ്. അന്തരീക്ഷത്തിലൂടെയുള്ള യാത്രയ്ക്കിടയില്‍ മാഗ്മ പെട്ടെന്നു തണുക്കുമ്പോഴാണ് പ്യൂമിസ് ഉണ്ടാകുന്നത്. പ്യൂമിസ് പൊടിയുമ്പോള്‍ സ്ഫടികതുല്യമായ ചാരമാകുന്നു.

അധിസിലിക മാഗ്മയുടെ വിദരോദ്ഗാരത്തോടനുബന്ധിച്ച് അഗ്നിസ്ഫുലിംഗങ്ങളും വിഷവാതകങ്ങളും ഉള്‍ക്കൊള്ളുന്ന ഭീമാകാരങ്ങളായ ധൂളീമേഘങ്ങളുണ്ടാകുന്നു. ഇവയെ 'നൂയെസ് ആര്‍ദെന്റെസ്' (Nuees Ardentes) എന്നു വിളിച്ചുവരുന്നു. മാഗ്മാ ഉദ്ഗാരത്തിനു തൊട്ടുമുന്‍പായി പരല്‍രൂപത്തിലുളള ആഗൈറ്റ്, ഫെല്‍സ്പാര്‍ തുടങ്ങിയ ധാതുക്കള്‍ വര്‍ഷിക്കപ്പെടാറുണ്ട്.

==വര്‍ഗീകരണം== 

ഏതെങ്കിലും രണ്ട് അഗ്നിപര്‍വതങ്ങളിലെ സ്ഫോടനം ഒരിക്കലും തന്നെ എല്ലാ രീതിയിലും തുല്യമായി എന്നു വരികയില്ല. മര്‍ദം, വാതകത്തിന്റെ അളവ്, ലാവയുടെ ശ്യാനത എന്നിവയുടെ അടിസ്ഥാനത്തില്‍ പ്രത്യേക ഭാവങ്ങള്‍ പ്രദര്‍ശിപ്പിച്ചുകാണുന്നവയെ താഴെപ്പറയുന്നപ്രകാരം വിഭജിച്ചിരിക്കുന്നു.

===ഹവായിയന്‍=== 

(Hawaiian) 

ഇവയുടെ ഉദ്ഗാരം ഏറിയ കൂറും ഗതിശീലലാവയായിട്ടായിരിക്കും. വാതകാംശം നന്നെ കുറവാണെന്നു മാത്രമല്ല വന്‍തോതിലുള്ള സ്ഫോടനം ഉണ്ടാകുന്നുമില്ല. വിലമുഖം തുളുമ്പിയൊഴുകുന്ന ലാവ നേരിയ സ്തരങ്ങളായി അനേകം കി.മീ. കളോളം വ്യാപിക്കുന്നു. ഉയര്‍ത്തി എറിയപ്പെടുന്ന മാഗ്മാപിണ്ഡങ്ങള്‍ നിലത്തു വീഴുമ്പോള്‍ തല്ലിപ്പരത്തിയതുപോലെയാകുന്നു. ലാവാ തടാകങ്ങളും 'പിലേയുടെ മുടി' എന്നറിയപ്പെടുന്ന അഗ്നി പര്‍വതസ്ഫടികത്തിന്റെ നാരുകളുമാണ് ഹവായിയന്‍ തരത്തിന്റെ സവിശേഷതകള്‍. ശ്യാനമായ ലാവ കൂടുതല്‍ മുറുകിയ ലാവയുടെ മുകളില്‍ തളംകെട്ടി സംവഹനരീതിയില്‍ പരിസഞ്ചരിക്കുന്നതാണ് ലാവാതടാകം. വാതകങ്ങളുടെ പെട്ടെന്നുള്ള നിഷ്‍ക്രമണംമൂലം ഉപരിതലത്തില്‍നിന്നും പുറത്തുചാടുന്ന ലാവ ശക്തിയായി അടിക്കുന്ന കാറ്റില്‍പ്പെട്ടു ഘനീഭവിക്കുമ്പോഴാണ് സ്ഫടികനാരുകളുണ്ടാകുന്നത്.

===സ്ട്രോംബോലിയന്‍===

(Strombolian). 

ലാവ അധികം സുചലമല്ലാതിരിക്കുമ്പോഴാണ് ഈ രീതിയിലുള്ള സ്ഫോടനം ഉണ്ടാകുന്നത്. വാതകങ്ങള്‍ക്കു ശക്തിയായ സ്ഫോടനത്തോടുകൂടി മാത്രമേ ബഹിര്‍ഗമിക്കാന്‍ കഴിയൂ. ഉദ്ഗാരവസ്തുക്കള്‍ അധികവും പൈറോക്ളാസ്റ്റികങ്ങളായിരിക്കും; ബോംബുകളും സ്കോറിയകളും ലാപ്പിലിയും ധാരാളമായി പതിക്കുന്നു; ജ്വലിക്കുന്ന ധൂളിമേഘങ്ങളും ഉണ്ടാകാം. സിസിലിക്കു വടക്കുള്ള ലിപ്പാരിദ്വീപിലെ സ്ട്രോംബോലി അഗ്നിപര്‍വതത്തെ ആധാരമാക്കിയാണ് ഈ വിഭജനം.

===വള്‍ക്കാനിയന്‍===

(Vulcanian) 

മുറുകി കുഴമ്പുപരുവത്തിലുള്ള മാഗ്മ ഉദ്ഗമിക്കുന്നവയാണ് ഈ ഇനം. അതിശക്തമായ സ്ഫോടനത്തോടുകൂടി വലുതും ചെറുതുമായ ലാവാപിണ്ഡങ്ങള്‍ ധാരാളമായി ചുഴറ്റി എറിയപ്പെടുന്നു. ഉദ്ഗാരത്തിന്റെ ആദ്യഘട്ടത്തില്‍ ഉയര്‍ന്നുപൊങ്ങുന്ന മാഗ്മ, മര്‍ദക്കുറവുമൂലം കട്ടിപിടിച്ചു വിലമുഖം അടയ്ക്കുന്നു. ഇതിനടിയില്‍ സഞ്ചിതമാകുന്ന വാതകങ്ങള്‍ ശക്തിയോടെ പുറത്തേക്കു വമിക്കുന്നതാണ് സ്ഫോടനത്തിനു ഹേതു. കോളിഫ്ളവറിന്റെ രൂപത്തിലുളള പടര്‍ന്ന ധൂമപടലങ്ങളും ജ്വലിക്കുന്നതും വിഷമയവുമായ ധൂളീമേഘങ്ങളും ഈയിനം ഉദ്ഗാരങ്ങളില്‍ സാധാരണയാണ്. ഇവ പൊട്ടുമ്പോള്‍ ദ്രവലാവ ഒട്ടുംതന്നെ പ്രവഹിക്കുന്നില്ല.

===പിലിയന്‍=== 

(Pelean) 

ഇതിലെ മാഗ്മ ഏറ്റവും ശ്യാനവും തന്‍മൂലം സ്ഫോടനം ഏറ്റവും ശക്തിയുള്ളതുമായിരിക്കും. വിലമുഖത്തിന്റെ വക്കുകള്‍ ക്രമേണ ഉയര്‍ന്നു വൃത്തസ്തൂപികാകൃതിയിലുള്ള ഒരു കുന്നിനു രൂപംകൊടുക്കുന്നു. ചിലപ്പോള്‍ ഇതിന്റെ വശങ്ങള്‍ ഭ്രംശിച്ചുനീങ്ങുന്നതു വന്‍പിച്ച നാശനഷ്ടങ്ങള്‍ക്കിടയാക്കും. വശങ്ങളിലുളള ലാവാ അട്ടികളിലെ വിലീന വാതകങ്ങള്‍ പെട്ടെന്നു രക്ഷപെടുന്നത് ഇടയ്ക്കിടെയുള്ള സ്ഫോടനങ്ങള്‍ക്കു കാരണമാകാം. ചിലപ്പോള്‍ ഗോളാകൃതിയില്‍ ഉരുണ്ടുകൂടുന്ന മാഗ്മാപിണ്ഡം വിലമുഖം അടച്ചുകളയുന്നതിനാല്‍, അഗ്നിപര്‍വതത്തിന്റെ ചരിവുകളില്‍ രണ്ടാമതൊരു വിലമുഖമുണ്ടാകാന്‍ സാധ്യതയുണ്ട്.

===വെസൂവിയന്‍=== 

(Vesuvian) 

ഇതില്‍നിന്നുള്ള വാതകപൂര്‍ണമായ മാഗ്മാ ഉദ്ഗാരം സാമാന്യം ശക്തമായ സ്ഫോടനത്തോടെയാകും. വിലമുഖത്തിന്റെ ഭിത്തികള്‍ ഈ സ്ഫോടനത്തിന്റെ ഫലമായി പൊട്ടിത്തെറിക്കുന്നു. ആദ്യം പതഞ്ഞു പൊങ്ങുന്ന മാഗ്മയും ധൂളീമേഘങ്ങളും ഉണ്ടാകുന്നു. പിന്നെ സുചലമായ മാഗ്മ വശങ്ങള്‍ കവിഞ്ഞൊഴുകുന്നു. അഗ്നിപര്‍വതച്ചാരം ഇതില്‍നിന്നു ധാരാളമായി വര്‍ഷിക്കപ്പെടും. വെസൂവിയസ് അഗ്നിപര്‍വതത്തില്‍നിന്നും നിഷ്പന്നമായതാണ് ഈ പേര്.

===മറ്റിനങ്ങള്‍=== 

ചില ഉദ്ഗാരങ്ങളില്‍ വിസര്‍ജിത പദാര്‍ഥങ്ങള്‍ക്ക് അഗ്നിപര്‍വതപ്രക്രിയയുമായി ബന്ധമുണ്ടാകില്ല; അല്ലാത്തപക്ഷം ആഗ്നേയപദാര്‍ഥങ്ങള്‍ നന്നെ കുറഞ്ഞ അളവിലായിരിക്കും. ഉത്പന്നങ്ങളുടെ ഊഷ്മാവ് താരതമ്യേന താണിരിക്കുന്നു. ഇത്തരം ഉദ്ഗാരങ്ങളാണ് അള്‍ട്രാ വള്‍ക്കാനിയന്‍ (Ultra Vulcanian). ഭൂഗര്‍ഭജലം വിലമുഖത്തിലെ മാഗ്മയുമായുളള സമ്പര്‍ക്കത്തില്‍ പെട്ടെന്നു ബാഷ്പീഭവിച്ചു ശക്തിയോടെ പുറത്തേക്കു വമിക്കുന്നു. ഉദ്ഗാരം ഈ രീതിയിലുളളതാകുമ്പോള്‍ അവയെ ഫ്രിയാറ്റിക്ക് (Friatic) തരമെന്നു പറയുന്നു.

==ബാഷ്പമുഖങ്ങള്‍== 

(Fumaroles) 

ശിലകളിലുള്ള വിദരങ്ങളിലൂടെ നീരാവിയും മറ്റു വാതകങ്ങളും ശക്തിയായി പുറത്തേക്കു വരുന്നതിനെ ബാഷ്പമുഖം അഥവാ ഫ്യുമറോള്‍ എന്നു പറയുന്നു. ഈ വാതകങ്ങളില്‍ 99 ശ.മാ.വും നീരാവിയായിരിക്കും. കാര്‍ബണ്‍ഡൈഓക്സൈഡ്, ഹൈഡ്രോക്ളോറിക് അമ്ളം, ഹൈഡ്രജന്‍സള്‍ഫൈഡ്, മീഥെയിന്‍ തുടങ്ങിയവയാണ് മറ്റു വാതകങ്ങള്‍.

ഗന്ധക വാതകങ്ങള്‍ പുറത്തേക്കു വിടുന്ന ഫ്യൂമറോളുകളാണ് സോള്‍ഫറ്റാറകള്‍ (Solfataras). ഹൈഡ്രജന്‍സള്‍ഫൈഡ് വാതകത്തിനു വായുസമ്പര്‍ക്കത്താല്‍ ജാരണം സംഭവിച്ചു ഗന്ധകം ഉണ്ടാകുന്നു. ഇവ സോള്‍ഫറ്റാറയുടെ അരികില്‍തന്നെ നിക്ഷേപിക്കപ്പെടുന്നു. പല സ്ഥലങ്ങളിലും ഇങ്ങനെയുള്ള നിക്ഷേപങ്ങളില്‍നിന്നു ഗന്ധകം ഖനനം ചെയ്തെടുക്കുന്നുണ്ട്. ഇതില്‍ പ്രധാനപ്പെട്ടത് നേപ്പിള്‍സിലെ ലാ സോള്‍ഫറ്റാറ (La Solfatara) ആണ്.

==ഉഷ്ണനീരുറവകള്‍== 

(Hot Springs) 

പല അഗ്നിപര്‍വത പ്രദേശങ്ങളിലും ചൂടുറവകളും ഫ്യൂമറോളുകളും ധാരാളമായി കണ്ടുവരാറുണ്ട്. ഇവ തമ്മില്‍ വളരെ അടുത്ത ബന്ധമാണുള്ളത്. ചില ചൂടുറവകള്‍ വരണ്ട കാലാവസ്ഥയില്‍ ഫ്യൂമറോളുകളായും ആര്‍ദ്രകാലാവസ്ഥയാകുമ്പോള്‍ വീണ്ടും ചൂടുറവകളായും മാറുന്നു. ചൂടുറവകള്‍ക്കു നിദാനം പ്രധാനമായും ഭൂഗതജലം ആണെന്നും അവ മാഗ്മയില്‍നിന്നുണ്ടാകുന്ന നീരാവികൊണ്ടു ചൂടുപിടിക്കുകയാണെന്നുമുള്ള വാദത്തെ ഇതു ന്യായീകരിക്കുന്നു.

പലതരത്തിലുള്ള ചൂടുറവകള്‍ ഉണ്ടെങ്കിലും ഏറ്റവും പ്രധാനപ്പെട്ടവ തിളയ്ക്കുന്ന ഉറവകളും ഉഷ്ണോല്‍സ(Geyser)ങ്ങളുമാണ്. പല അഗ്നിപര്‍വതപ്രദേശങ്ങളിലും കണ്ടുവരുന്ന ഒരു പ്രത്യേകതയാണ് തിളയ്ക്കുന്ന നീരുറവകള്‍; യു.എസ്സിലെ 'യെല്ലോസ്റ്റോണ്‍ നാഷണല്‍ പാര്‍ക്കി'ല്‍ ഇവ ധാരാളമുണ്ട്. ഇവ ചൂടുവെള്ളമുള്ള കുളങ്ങള്‍ മുതല്‍ ശക്തിയായി തിളയ്ക്കുന്ന ഉറവകള്‍വരെ ആകാം.

ഇടവിട്ടിടവിട്ടു കുറെയധികം ചൂടുവെള്ളവും നീരാവിയും കൂടി പൊട്ടിത്തെറിയോടെ പുറത്തേക്കുതള്ളുന്ന ചൂടുറവകളെയാണ് 'ഉഷ്ണോല്‍സം' എന്നു പറയുന്നത്. ചില അവസരങ്ങളില്‍ ഇവ നൂറുകണക്കിന് അടി ഉയരത്തില്‍ ചീറ്റാറുണ്ട്. ചിലതിന്റെ ഉദ്ഗാരം ഏതാനും നിമിഷത്തേക്കായിരിക്കും; എന്നാല്‍ മിനിട്ടുകളോളമോ മണിക്കൂറുകളോളമോ നീണ്ടുനില്ക്കുന്നവയുമുണ്ട്. ബഹിര്‍ഗമിക്കുന്ന ജലത്തിന്റെ അളവ് വളരെ കുറച്ചുമുതല്‍ അനേകായിരം ഘ.മീ. വരെ ആകാം. ചില ഉഷ്ണോല്‍സങ്ങള്‍ ഒരു നിശ്ചിതസമയക്രമം പാലിച്ച് ആവര്‍ത്തിക്കുന്നു; എന്നാല്‍ ഭൂരിഭാഗവും യാതൊരു ക്രമവും പാലിക്കാത്തവയാണ്.

==ചെളി അഗ്നിപര്‍വതങ്ങള്‍== 

(Mud Volcanoes) 

ഭൂമിയുടെ ഉപരിതലം ഭേദിച്ചു പുറത്തേക്കുവരുന്ന വാതകങ്ങള്‍ വഹിച്ചുകൊണ്ടുവരുന്ന മണലും കളിമണ്ണും നാളിയുടെ ചുറ്റുമായി നിക്ഷേപിക്കപ്പെടുന്നു. തുടര്‍ച്ചയായുള്ള ഇത്തരം പ്രവൃത്തിമൂലം ഒരു കോണ്‍ (cone) രൂപംകൊള്ളുന്നു. ജലത്തിന്റെ സാന്നിധ്യത്തില്‍ മണലും കളിമണ്ണും കൂടിച്ചേര്‍ന്ന് ചെളിയായിത്തീരും. ഇതു കോണിന്റെ മുകളില്‍ ഉണങ്ങി കട്ടപിടിക്കുന്നു. കോണിന്റെ മുകള്‍ഭാഗം പൊട്ടിത്തെറിക്കുന്ന ഘട്ടംവരെ കട്ടപിടിച്ച ഉപരിതലത്തിനുതാഴെ വാതകങ്ങള്‍ സഞ്ചയിക്കുകയും തന്‍മൂലം മര്‍ദം കൂടിവരികയും ചെയ്യുന്നു. മര്‍ദത്തിന്റെ ആധിക്യംമൂലം കോണിന്റെ മുകള്‍ഭാഗം പൊട്ടിത്തെറിക്കും. ഇതു ചെറിയതോതിലെങ്കിലും ഒരു അഗ്നിപര്‍വതത്തിന്റെ പ്രതീതി ജനിപ്പിക്കുന്നു. പല ചെളി അഗ്നിപര്‍വതങ്ങളും എണ്ണപ്പാടങ്ങളിലാണ് കണ്ടുവരുന്നത്. ഇവയില്‍ മിക്കതും അഗ്നിപര്‍വതപ്രദേശങ്ങളില്‍ നിന്നകന്നു സ്ഥിതിചെയ്യുന്നവയുമാണ്. ചെളിയില്‍ക്കൂടി നീരാവി - മിക്കവാറും അഗ്നിപര്‍വത വിസര്‍ജിതമായത് - രക്ഷപെടുമ്പോള്‍ ഉണ്ടാകുന്നതാണ് മറ്റു ചെളിഅഗ്നി പര്‍വതങ്ങള്‍. മഴക്കാലത്താണ് ചെളിഅഗ്നിപര്‍വതങ്ങള്‍ സജീവമായി കണ്ടുവരുന്നത്. പുറത്തേക്കുവരുന്ന ചെളി അന്തരീക്ഷ-ഊഷ്മാവില്‍നിന്ന് അല്പം ഉയര്‍ന്ന താപനില കാണിക്കാറുണ്ട്.

==വിതരണം== 

ഇപ്പോള്‍ ഭൂമുഖത്ത് 500-ല്‍പരം അഗ്നിപര്‍വതങ്ങള്‍ സജീവങ്ങളായി ഉണ്ടെന്നാണ് കണക്കാക്കപ്പെട്ടിരിക്കുന്നത്. ഇവയില്‍ വ. അമേരിക്കയുടെ പടിഞ്ഞാറന്‍ തീരത്തുള്ള കട്മൈ, മൌണ്ട് ഹൂഡ്, ലാസ്സെന്‍പീക്, ഹവായിയിലെ മാണാലോവാ, മെക്സിക്കോയിലെ പാരീകൂട്ടിന്‍, പോപോകാറ്റെപെറ്റി, ഇക്വഡോറിലെ കോട്ടപക്സി, മാര്‍ട്ടനിക് ദ്വീപിലെ മൌണ്ട് പിലേ, സിസിലിയിലെ എറ്റ്ന, ഇറ്റലിയിലെ വെസൂവിയസ്, ജപ്പാനിലെ ഫ്യൂജിയാമ, ഫിലിപ്പീന്‍സിലെ മായാണ്‍ എന്നിവ ഏറ്റവും അപകടകാരികളായി ഗണിക്കപ്പെടുന്നു.

ഭൂമുഖത്തൊട്ടാകെയുള്ള അഗ്നിപര്‍വതങ്ങളെ വ്യത്യസ്ത മേഖലകളാക്കിത്തിരിക്കാം. പസിഫിക് സമുദ്രത്തെ ചുറ്റി അഗ്നിപര്‍വതങ്ങളുടെ ഒരു ശൃംഖല കാണാം. തെ. അമേരിക്കയിലെ ആന്‍ഡീസ് പര്‍വതനിരകളില്‍ത്തുടങ്ങി മധ്യ അമേരിക്ക, മെക്സിക്കോ, പശ്ചിമ യു.എസ്സിലെ കാസ്കേഡ് നിരകള്‍, അലൂഷ്യന്‍ ദ്വീപുകള്‍, കംചാത്ക, കുറൈല്‍ദ്വീപുകള്‍, ജപ്പാന്‍, ഫിലിപ്പീന്‍സ്, സെലിബിസ്, ന്യൂഗിനി, സോളമന്‍ ദ്വീപുകള്‍, ന്യൂകാലിഡോണിയ, ന്യൂസിലന്‍ഡ് എന്നീ പ്രദേശങ്ങളിലൂടെ നീളുന്ന ഈ മേഖലയില്‍ ഉടനീളം സജീവമോ (active) നിര്‍വാണമോ (dormant) ലുപ്തമോ (extinct) ആയ ധാരാളം അഗ്നിപര്‍വതങ്ങളുണ്ട്. മറ്റു മേഖലകള്‍ താഴെ പറയുന്നവയാണ്. (1) ഹവായ് ഗലാപഗോസ്, ജോണ്‍ ഫര്‍ണാണ്ടസ് എന്നീ ദ്വീപസമൂഹങ്ങള്‍; (2) ടിമോര്‍, ജാവ, സുമാത്ര, ബാലി എന്നീ ദ്വീപുകള്‍; (3) അറേബ്യന്‍ ഉപദ്വീപില്‍ തുടങ്ങി ആഫ്രിക്കയുടെ കിഴക്കെ അരികിലുള്ള ഭ്രംശ-താഴ്വര(rift valley)യിലൂടെ മലഗസി റിപ്പബ്ളിക്കോളം നീളുന്ന മേഖല; (4) ഏഷ്യയിലെ അരാറത്ത് പര്‍വതത്തില്‍ തുടങ്ങി മെഡിറ്ററേനിയന്‍ തീരത്തു കൂടെ അസോര്‍സ്, കാനറിദ്വീപസമൂഹം എന്നിവിടങ്ങളോളമെത്തുന്ന ശൃംഖല; (5) വെസ്റ്റ്ഇന്‍ഡീസ്. ഇവ കൂടാതെ ഐസ്‍ലന്‍ഡ് തുടങ്ങി ഒറ്റപ്പെട്ട പ്രദേശങ്ങളുമുണ്ട്; നിഷ്ക്രിയ അഗ്നിപര്‍വതങ്ങളുടേതായ നിരവധി മേഖലകള്‍ മേല്പറഞ്ഞതില്‍ ഉള്‍പ്പെടുന്നില്ല. ഇന്ത്യയിലെ ഏകസജീവ അഗ്നിപര്‍വതം ആന്തമാന്‍-നിക്കോബാര്‍ ദ്വീപസമൂഹത്തിലെ ബാരണ്‍ ഐലന്റി (Barren island)ലേതാണ്.

അഗ്നിപര്‍വതസ്ഫോടനം അളവറ്റ നാശനഷ്ടങ്ങള്‍ക്കു കാരണമാകുന്നുവെന്നു മുന്‍പു സൂചിപ്പിച്ചു. 1883-ലെ ക്രാകതോവാ വിസ്ഫോടനഫലമായി 36,000 പേര്‍ക്കു ജീവാപായം നേരിട്ടു. ഇന്തോനേഷ്യയിലെ ജാവയ്ക്കും സുമാത്രയ്ക്കും ഇടയ്ക്കുള്ള ക്രാകതോവാദ്വീപ് അതിലുള്ള അഗ്നിപര്‍വതത്തിന്റെ സ്ഫോടനത്തെതുടര്‍ന്ന് ഒന്നാകെ കടലില്‍ ആണ്ടുപോകുകയുണ്ടായി. മൌണ്ട്പിലേയില്‍ നിന്നുള്ള (1902) വിഷലിപ്തമായ ധൂളീപ്രസരം 12,000 ആളുകളെ വകവരുത്തുകയും സമീപത്തുള്ള സെയിന്റ്പിയറെ പട്ടണം അഗ്നിക്കിരയാക്കുകയും ചെയ്തു. 1951-ല്‍ ന്യൂഗിനിയിലെ മൌണ്ട്‍ലാമിങ്ടണ്‍ പെട്ടെന്നു പൊട്ടിത്തെറിച്ചപ്പോള്‍ 3,000 ആളുകള്‍ കൊല്ലപ്പെട്ടു.

<gallery>
Image:p133g.png|ലാവാപ്രവാഹം
Image:p133h.png|വെസുവിയന്‍
Image:p133i.png|ഹവായിയന്‍
Image:p133j.png|സ്ട്രോംബോലിയന്‍
Image:p133k.png|പീലിയന്‍
<\gallery>

<gallery >
Image:p133a.png|ദ്രവലാവ
Image:p133b.png|'ആആ' ലാവ
Image:p133c.png|ചെളി അഗ്നിപര്‍വ്വതം
Image:p133d.png|അഗ്നിപര്‍വ്വത വക്ത്രം
Image:p133e.png|ഹ്യൂമറോള്‍
Image:p133f.png|ഉഷ്ണനീരുറവ
</gallery>
ഒരു അഗ്നിപര്‍വതം സജീവമോ നിര്‍ജീവമോ എന്നു സ്ഥിരപ്പെടുത്തി പറയുക എളുപ്പമല്ല; അവയുടെ നിഷ്ക്രിയത്വം താത്കാലികമായിരിക്കും. ഫിലിപ്പീന്‍സിലെ താല്‍ അഗ്നിപര്‍വതം 1572-നുശേഷം 26 തവണ പൊട്ടിത്തെറിച്ചു. ഏറ്റവുമൊടുവിലത്തെ സ്ഫോടനം (1865) മൂലം പര്‍വതശിഖരത്തില്‍ 1,500 മീ. നീളവും 400 മീ. വീതിയുമുള്ള അഗാധമായ ഒരു കിടങ്ങുണ്ടായി. കോസ്റ്ററിക്കയിലെ ഇറാസു 45 വര്‍ഷം തണുത്തുകിടന്നശേഷം പെട്ടെന്നു സജീവമായി (1963); ഈ ഉദ്ഗാരം 20 മാസം നീണ്ടുനിന്നു. ഇറ്റലിയിലെ വെസൂവിയസ് എ.ഡി. 79-നുശേഷം വളരെക്കാലം നിഷ്ക്രിയമായി കിടന്നെങ്കിലും 1944-ല്‍ വീണ്ടും സജീവമായി. പ്ളീസ്റ്റോസീന്‍ യുഗത്തിനിപ്പുറം എരിഞ്ഞടങ്ങിയ അഗ്നിപര്‍വതങ്ങള്‍ എല്ലാം തന്നെ വീണ്ടും സജീവമാകാനിടയുള്ളതായി കരുതപ്പെടുന്നു.

പുതിയ ഭൂരൂപങ്ങള്‍ സൃഷ്ടിക്കുന്നതില്‍ അഗ്നിപര്‍വതങ്ങള്‍ക്കു വലിയ പങ്കുണ്ട്. മെക്സിക്കോയിലെ പാരീകൂട്ടിന്‍ 1943-ല്‍ ഉദ്ഗമിച്ച്, 1952 വരെ ലാവ ഒഴുകിയതിന്റെ ഫലമായി 410 മീ. കനത്തിലുള്ള ലാവാനിക്ഷേപങ്ങളുണ്ടായി. ഐസ്‍ലന്‍ഡിന്റെ തീരത്തുള്ള സര്‍ട്ട്സിദ്വീപിന്റെ പ്രോത്ഥാനം (1963) മറ്റൊരു ഉദാഹരണമാണ്. ഏറെ താമസിയാതെ ലിറ്റില്‍ സര്‍ട്ട്സി എന്ന ദ്വീപും സമീപത്തായി പൊന്തിവന്നു (1965). ഭൌമായുസ്സിലെ ആദ്യഘട്ടങ്ങള്‍ മുതല്‍ അഗ്നിപര്‍വതങ്ങള്‍ പ്രതലസംരചനയില്‍ പ്രമുഖമായ പങ്കു വഹിച്ചിരുന്നു. ഭൂമുഖത്ത് ഇന്നു കാണുന്ന ജലസഞ്ചയത്തിനും പ്രാണവായു നിറഞ്ഞ അന്തരീക്ഷത്തിനും രൂപം നല്കിയതു യുഗങ്ങളോളം നീണ്ടുനിന്ന അഗ്നിപര്‍വതപ്രക്രിയയായിരുന്നു. അഗ്നിപര്‍വതജന്യമായ ലാവയും ചാരവും കലര്‍ന്ന മണ്ണ് ഏറ്റവുമധികം ഫലഭൂയിഷ്ഠമാണ്. നോ: അഗ്നിപര്‍വതവക്ത്രം, അഗ്നിപര്‍വതച്ചാരം, അഗ്നിപര്‍വതവിജ്ഞാനീയം, ആഗ്നേയശില, ഭൂചലനം

(വി. നാരായണന്‍ നായര്‍)
[[Category:ഭൂവിജ്ഞാനീയം]]