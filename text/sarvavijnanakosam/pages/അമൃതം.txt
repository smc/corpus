= അമൃതം =

മൃതം (മരണം) യാതൊന്നുകൊണ്ട് ഒഴിവാക്കുവാന്‍ സാധിക്കുമോ അതാണ് അമൃതം. മരിക്കാതിരിക്കുക എന്നു മാത്രമല്ല മരിച്ചവര്‍ ജീവിക്കുക എന്നതുകൂടി അമൃതംമൂലം സാധ്യമായിത്തീരുന്നു എന്നു വിശ്വസിക്കപ്പെടുന്നു. അമൃതത്തെ ബ്രഹ്മാനന്ദമായി ഉപനിഷത്തുകളില്‍ നിര്‍ദേശിച്ചിട്ടുണ്ട്. 'യജ്ഞാത്വാമൃതമശ്‍നുതേ' എന്നിങ്ങനെ ആത്മജ്ഞാനത്തിന്റെ ഫലം അമൃതാനുഭൂതിയാണെന്നു ഗീതയിലും പ്രസ്താവിച്ചിരിക്കുന്നു.

യജ്ഞശിഷ്ടത്തെ (യാഗത്തില്‍ ദേവതയ്ക്കു സമര്‍പ്പിച്ചതിനുശേഷം ദ്രവ്യത്തെ) അമൃതമെന്നു വ്യവഹരിക്കുന്നുണ്ട്. അതു ഭുജിക്കുന്നവര്‍ എല്ലാ പാപത്തില്‍ നിന്നും മുക്തരാകുന്നു എന്നു ഭഗവദ്ഗീതയില്‍ പറഞ്ഞിട്ടുണ്ട്.

'യജ്ഞശിഷ്ടാമൃതഭൂജോ

മുച്യന്തേ സര്‍വകില്ബിഷാല്‍'..... (ഭ.ഗീ. 13:12) യാചിച്ചു ലഭിക്കുന്ന ഭിക്ഷ മൃതവും യാചിക്കാതെ ലഭിക്കുന്ന ഭിക്ഷ അമൃതവും ആണെന്നു മനുസ്മൃതിയില്‍ വിവേചനം ചെയ്യപ്പെട്ടിരിക്കുന്നു.

'മൃതം സ്യാദ്യാചിതം ഭൈക്ഷ-

മമൃതം സ്യാദയാചിതം'

ഗോരൂപം ധരിച്ച ഭൂമിദേവിയെ തന്റെ ആജ്ഞയ്ക്കുവശംവദയാക്കി പൃഥുചക്രവര്‍ത്തി അവരവര്‍ക്കിഷ്ടമുള്ളതു കറന്നെടുക്കാന്‍ നിര്‍ദേശിച്ചപ്പോള്‍ ദേവന്‍മാര്‍ ദുഗ്ധരൂപേണ കറന്നെടുത്തതു അമൃതമാണെന്നു പുരാണത്തില്‍ പറഞ്ഞിട്ടുണ്ട്. ദേവന്‍മാര്‍ ദുര്‍വാസാവിന്റെ ശാപംകൊണ്ട് ജരാബാധിതരായപ്പോള്‍ അസുരന്‍മാരുമായി സഖ്യം ചെയ്ത് ഇരുവരുംകൂടി പാലാഴി മഥിച്ചതില്‍നിന്നും ലഭിച്ച വിഭവങ്ങളില്‍ സര്‍വപ്രധാനം അമൃതമായിരുന്നു എന്നു പ്രസിദ്ധമാണ്. കര്‍ണാമൃതം, നേത്രാമൃതം തുടങ്ങിയ ഭാഷാപ്രയോഗങ്ങള്‍ അമൃതത്തിന്റെ മഹനീയതയെ ദ്യോതിപ്പിക്കുന്നു. ചന്ദ്രന്‍ അമൃതകിരണനാണെന്നും പ്രസിദ്ധിയുണ്ട്. ദേവന്‍മാര്‍ ഭക്ഷിക്കുന്നതുകൊണ്ട് കൃഷ്ണപക്ഷത്തില്‍ ചന്ദ്രന്‍ ഓരോ കലയായി കാണാതാകുന്നു എന്ന് പൌരാണികര്‍ വിശ്വസിച്ചിരുന്നു. ചന്ദ്രനിലെ ഒരിക്കലും ക്ഷയിക്കാത്ത ഒരൊറ്റകലയ്ക്ക് അമൃതകല എന്നാണ് പേര്‍. ജീമൂതവാഹനന്റെ പ്രാണത്യാഗത്താല്‍ പശ്ചാത്താപഭരിതനായ ഗരുഡന്‍ അമൃതം സമ്പാദിച്ചു വര്‍ഷിക്കുകയാല്‍ അസ്ഥിശേഷരായിരുന്ന നാഗങ്ങളെല്ലാം ജീവിച്ചു എന്നു ജാതകകഥകളെ ആസ്പദമാക്കിയെഴുതിയ നാഗാനന്ദം നാടകത്തില്‍ വര്‍ണിതമായിട്ടുണ്ട്. രാവണവധത്താല്‍ സന്തുഷ്ടരായ ദേവന്‍മാര്‍ ശ്രീരാമനെ അഭിനന്ദിച്ച ഘട്ടത്തില്‍ ദേവേന്ദ്രന്‍ അദ്ദേഹത്തോടു വരം ചോദിക്കുവാന്‍ നിര്‍ദേശിച്ചു. യുദ്ധത്തില്‍ തനിക്ക് ഉപകാരം ചെയ്തു മരിച്ച വാനരന്‍മാര്‍ ജീവിക്കണമെന്നും അംഗവൈകല്യം സംഭവിച്ചവര്‍ മുമ്പത്തെപ്പോലെ സ്വസ്ഥരാകണമെന്നും ആഗ്രഹം പ്രദര്‍ശിപ്പിക്കുകയുണ്ടായി. ദേവരാജന്‍ അമൃതവര്‍ഷംകൊണ്ട് ആ അപേക്ഷ നിറവേറ്റിക്കൊടുത്തു.
  
'അസതോ മാ സദ് ഗമയ

തമസോ മാ ജ്യോതിര്‍ഗമയ

മൃത്യോര്‍മാ അമൃതം ഗമയ' എന്ന ഔപനിഷദ പ്രാര്‍ഥനയില്‍ അസത്തിന് എതിരായി സത്തിനേയും ഇരുട്ടിന് വെളിച്ചത്തേയ്ക്ക് മൃത്യുവിന് എതിരായി അമൃതത്തെയും നിര്‍ദേശിച്ചിട്ടുള്ളത് മനുഷ്യന്റെ സര്‍വോത്തമമായ ലക്ഷ്യം അമൃതം (മോക്ഷം) തന്നെയാണ് എന്ന നിര്‍ണയത്തിനു തികഞ്ഞ നിദര്‍ശനമാകുന്നുണ്ട്.

(എം.എച്ച്. ശാസ്ത്രികള്‍)

[[Category:തത്ത്വശാസ്ത്രം]]