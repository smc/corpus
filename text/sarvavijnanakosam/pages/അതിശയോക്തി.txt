= അതിശയോക്തി =


ഒരു അര്‍ഥാലങ്കാരം. വസ്തുസ്ഥിതികളെ അതിക്രമിച്ചുള്ള ഏതു ചൊല്ലും അതിശയോക്തിയാണ്. 'ചൊല്ലുള്ളതില്‍ കവിഞ്ഞുള്ളതെല്ലാമതിശയോക്തിയാം' എന്ന് ഭാഷാഭൂഷണം. സാമ്യ-വാസ്തവ-ശ്ളേഷമൂലകങ്ങളായ അലങ്കാരങ്ങളിലും ഒരളവുവരെ അതിശയോക്തി ഉണ്ടായിരിക്കും.

'തെല്ലതിന്‍ സ്പര്‍ശമില്ലാതെ-

യില്ലലങ്കാരമൊന്നുമേ'            (ഭാഷാഭൂഷണം)

അതിശയം അതിപ്രകടമായിരിക്കുന്ന ഉക്തികളെയാണ് അതിശയോക്ത്യലങ്കാരങ്ങളായി പരിഗണിക്കുന്നത്. ഇവ അനേകവിധമുണ്ട്. പ്രധാനമായി ഇവയെ മൂന്നിനമായി തിരിക്കാം:-
 
'''വര്‍ണ്യത്തിന്റെ പരിമാണം'''. (അളവ്, എണ്ണം മുതലായവ) ഉള്ളതില്‍ കൂട്ടിയോ കുറച്ചോ പറയുന്നത്. ഇതില്‍ (1) സംബന്ധാതിശയോക്തി; (2) അസംബന്ധാതിശയോക്തി; (3) ഭേദകാതിശയോക്തി എന്നിവയാണ് മുഖ്യം. 'അയോഗത്തിങ്കലെ യോഗം സംബന്ധാതിശയോക്തിയാം' (ഉദാ. മലയോളം പോന്ന കാള);  'അയോഗം ചൊല്കയോഗത്തില്‍' അസംബന്ധാതിശയോക്തിയും. (ഉദാ. വിറ്റുവിറ്റ് ഇനി ഉടുതുണിയേ ബാക്കിയുള്ളു); 'ഭേദം ചൊന്നാലഭേദത്തില്‍ ഭേദകാതിശയോക്തി'യും (ഉദാ. അന്യാദൃശം തന്നെ ഇവളുടെ അഴക്) ആണ്. ആധാരാധിക്യവും ആധേയാധിക്യവും പറയുന്ന 'അധികം' എന്ന അലങ്കാരവും ഈ ഇനത്തില്‍ പെടും.
  
'''കാര്യകാരണവ്യതിക്രമം.''' ഏതുകാര്യത്തിനും കാരണമുണ്ടായിരിക്കും; കാരണത്തെ തുടര്‍ന്നേ കാര്യം സംഭവിക്കൂ. ഈ കാര്യകാരണ നിയമത്തെ അതിക്രമിച്ചുള്ള ചൊല്ലുകളാണ് ഈ ഇനത്തില്‍ വരുന്നത്.
  
(1) കാര്യകാരണങ്ങള്‍ അഭിന്നം എന്നു കല്പിക്കുന്ന 'ഹേത്വതിശയോക്തി'; (2) കാര്യവും കാരണവും ഒരുമിച്ചു സംഭവിക്കുന്നതായി പറയുന്ന 'അക്രമാതിശയോക്തി'; (3) കാരണത്തിനും മുന്‍പേ കാര്യം സംഭവിച്ചതായി പറയുന്ന 'അത്യന്താതിശയോക്തി'; (4) കാരണമില്ലാതെയും കാര്യമുണ്ടായതായി പറയുന്ന 'വിഭാവന'; (5) കാരണമുണ്ടായിട്ടും കാര്യമുണ്ടായില്ല എന്നു പറയുന്ന 'വിശേഷോക്തി'; (6) കാരണം ഒരിടത്തും കാര്യം മറ്റൊരിടത്തും സംഭവിച്ചതായി പറയുന്ന 'അസംഗതി'; ഇത്രയുമാണ് ഈ ഇനത്തില്‍ പ്രധാനം.
 
'''സാമ്യസംബന്ധമുള്ളവ'''. ഈ ഇനത്തില്‍ മുഖ്യം രൂപകാതിശയോക്തിയാണ്. 'നിഗീര്യാധ്യവസാനം താന്‍ രൂപകാതിശയോക്തിയാം' (ഭാഷാഭൂഷണം).

ഉദാ. 'സരോജയുഗളം കാണ്‍ക

ശരങ്ങള്‍ ചൊരിയുന്നിതാ.'

ഇവിടെ നേത്രകടാക്ഷങ്ങളെ തദുപമാനങ്ങളായ സരോജശരങ്ങളായി പറഞ്ഞിരിക്കുന്നു. 'തദ്ഗുണം', 'അതദ്ഗുണം' മുതലായ അലങ്കാരങ്ങളെയും ഈ ഇനത്തില്‍ പെടുത്താവുന്നതാണ്.
(കെ.കെ. വാധ്യാര്‍)
[[Category:ഭാഷാശാസ്ത്രം-അലങ്കാരം]]