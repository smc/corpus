==ചലവീണ==

ശ്രുതിഭേദം പരീക്ഷിക്കുന്നതിനായി കമ്പി അയച്ചു കെട്ടുന്ന വീണ. ഈ പരീക്ഷണത്തിനായി ആദ്യം രണ്ടു വീണകള്‍ ഒരുപോലെ നാദം പുറപ്പെടുവിക്കത്തക്കവണ്ണം ശ്രുതിപ്പെടുത്തുന്നു. തുടര്‍ന്ന് ഒന്നിന്റെ മുറുക്കം അതേപടി നിലനിര്‍ത്തിക്കൊണ്ട്, പരീക്ഷണ വിധേയമാക്കുന്ന മറ്റേ വീണയുടെ കമ്പി ക്രമമായി അയയ്ക്കുകയാണ് ചെയ്യുന്നത്. അപ്പോള്‍ അതിനുണ്ടാകുന്ന ശ്രുതിവ്യത്യാസം കമ്പി അയയ്ക്കാത്ത വീണയുടെ ശ്രുതിയുമായി താരതമ്യപ്പെടുത്തുന്നു. ഇവയില്‍ കമ്പികളുടെ മുറുക്കം അയച്ച് ശ്രുതിഭേദം നിര്‍ണയിക്കുന്നതിന് ഉപയോഗിക്കുന്ന വീണ ചലവീണയെന്നും മറ്റേത് അചലവീണ അഥവാ ധ്രുവവീണ എന്നും അറിയപ്പെടുന്നു.
   
സംഗീതരത്നാകര കര്‍ത്താവായ ശാര്‍ങ്ഗദേവന്‍ 22 ശ്രുതികളെ വ്യക്തമാക്കുന്നതിനായി ഈ പരീക്ഷണം നടത്തുന്നതിനെക്കുറിച്ച് പരാമര്‍ശിച്ചിട്ടുണ്ട്.