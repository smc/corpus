= അന്‍ഡോറാ =

Andorra


സ്പെയിനിനും ഫ്രാന്‍സിനുമിടയ്ക്ക് പിരണീസ് പര്‍വതമേഖലയില്‍ സ്ഥിതി ചെയ്യുന്ന ഒരു രാജ്യം. വിസ്തീര്‍ണം 450 ച.കി.മീ.; തലസ്ഥാനം: അന്‍ഡോറാ-ലാ-വെല്ല്യം. സംസ്ഥാനത്തിന്റെ തെക്കുപടിഞ്ഞാറന്‍ ഭാഗം ഒഴിച്ചാല്‍ ചുറ്റിലും ഉയരം കൂടിയ പര്‍വതങ്ങളാണ്. വീതികുറഞ്ഞ മലയിടുക്കുകളും അഗാധമായ ചുരങ്ങളും ക്രമരഹിതമായ താഴ്വരകളുംകൊണ്ട് സങ്കീര്‍ണമാണ് ഭൂസ്ഥിതി. 


1991-ലെ സെന്‍സസ് പ്രകാരം 65971 ആയിരുന്നു അന്‍ഡോറായിലെ ജനസംഖ്യ. 95 ശ.മാ.-ത്തിലധികം ജനങ്ങളും നഗരവാസികളാണ്. ജനസംഖ്യയുടെ 60 ശ.മാ.-ത്തിലധികവും സ്പാനിഷ് വംശജരാകുന്നു. ഔദ്യോഗിക ഭാഷയായ കാറ്റലാനിനു പുറമേ സ്പാനിഷും വന്‍തോതില്‍ പ്രചാരത്തിലുണ്ട്.


ഗോത്രാധിപഭരണ സമ്പ്രദായമാണ് മുമ്പ് അന്‍ഡോറയില്‍ നിലനിന്നിരുന്നത്. 1993 മേയ് 4-ന് ഒരു ജനാധിപത്യഭരണ ക്രമം ഇവിടെ നിലവില്‍വന്നു. ഇതിന്‍പ്രകാരം ഫ്രഞ്ച് റിപ്പബ്ളിക്കിന്റെ പ്രസിഡന്റും അര്‍ജെല്‍ (urgel) ബിഷപ്പുമാണ് രാഷ്ട്രത്തലവന്‍മാര്‍. സഹരാജപദവിയാണ് ഇവര്‍ക്ക് ലഭിച്ചിട്ടുള്ളത്. ജനറല്‍ കൌണ്‍സില്‍ ഒഫ് ദ അന്‍ഡോറന്‍ വാലീസ് എന്ന പേരില്‍ അറിയപ്പെടുന്ന പാര്‍ലമെന്റില്‍ 28 അംഗങ്ങളാണുള്ളത്. അംഗങ്ങളുടെ കാലാവധി നാലുവര്‍ഷമാണ്. ഭരണത്തലവന്‍ കൂടിയായ എക്സിക്യൂട്ടീവ് കൌണ്‍സില്‍ പ്രസിഡന്റിനെ തെരഞ്ഞെടുക്കുന്ന ചുമതലയും ജനറല്‍ കൌണ്‍സിലില്‍ നിക്ഷിപ്തമായിരിക്കുന്നു.


സ്പെയിന്‍, ഫ്രാന്‍സ്, ജര്‍മനി, സ്വിറ്റ്സര്‍ലന്‍ഡ്, യു.എസ്.എ. തുടങ്ങിയ രാജ്യങ്ങളുമായി അന്‍ഡോറ വാണിജ്യ-വ്യാപാര ബന്ധം പുലര്‍ത്തുന്നു. ഫ്രഞ്ചു നാണയവും, സ്പാനിഷ് നാണയവും ഇവിടെ പ്രചാരത്തിലുണ്ട്.


രാജ്യത്തിന്റെ മൊത്തം ഭൂവിസ്തൃതിയുടെ രണ്ട് ശ.മാ. പ്രദേശങ്ങളും കൃഷിക്കുപയുക്തമാവുന്നു. പുകയിലയും ഉരുളന്‍കിഴങ്ങുമാണ് മുഖ്യ വിളകള്‍. ആടു വളര്‍ത്തലും ഒരു പ്രധാന ഉപജീവന മാര്‍ഗമാണ്. ഉത്പാദന മേഖലയില്‍ സിഗരറ്റ്, സിഗാര്‍, ഫര്‍ണിച്ചര്‍ തുടങ്ങിയവയുടെ ഉത്പാദനത്തിന് പ്രാമുഖ്യം ലഭിച്ചിരിക്കുന്നു. വിനോദസഞ്ചാരത്തിന് സമ്പദ്ഘടനയില്‍ സുപ്രധാനമായൊരു പങ്കുണ്ട്.

[[Category:രാജ്യം]]