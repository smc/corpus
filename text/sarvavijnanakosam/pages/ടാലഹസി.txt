=ടാലഹസി =
Tallahassee

യു. എസിലെ ഫ്ളോറിഡ സംസ്ഥാനത്തിന്റെ തലസ്ഥാന നഗരം. 1823-ലാണ് ഈ നഗരം സ്ഥാപിതമായത്. 1825-ല്‍ ടാലഹസി പ്രദേശം അമേരിക്കന്‍ യൂണിയനില്‍ ലയിച്ചു. ജനസംഖ്യ: 124,773 (90). 137057 (94 ല.)

ഫ്ളോറിഡയുടെ ഭരണസിരാകേന്ദ്രമായ ടാലഹസി, വിദ്യാഭ്യാസ വാണിജ്യ മേഖലകളിലും ഏറെ പുരോഗതി നേടിയിട്ടുണ്ട്. നഗരത്തിനു ചുറ്റുമായി വ്യാപിച്ചിരിക്കുന്ന കൃഷിയിടങ്ങളില്‍നിന്ന് ഉത്പാദിപ്പിക്കുന്ന കാര്‍ഷികോത്പന്നങ്ങളുടെ പ്രധാന വിപണനകേന്ദ്രവും ഈ നഗരംതന്നെ. കന്നുകാലിവളര്‍ത്തലിലും പരുത്തി, പുകയില എന്നിവയുടെ ഉത്പാദനത്തിലുമാണ് ഇവിടത്തെ കാര്‍ഷിക മേഖല പ്രധാനമായും കേന്ദ്രീകരിച്ചിരിക്കുന്നത്. വ്യവസായങ്ങളില്‍ തടിയുത്പന്നങ്ങള്‍, സംസ്കരിച്ച ഭക്ഷ്യസാധനങ്ങള്‍, കെട്ടിടനിര്‍മാണ സാമഗ്രികള്‍, അച്ചടി സാമാനങ്ങള്‍, വെടിമരുന്ന് എന്നിവയുടെ നിര്‍മാണത്തിന് മുന്‍തൂക്കം ലഭിച്ചിരിക്കുന്നു. 1845-ല്‍ നിര്‍മാണം പൂര്‍ത്തിയായ പഴയ സ്റ്റേറ്റ് കാപ്പിറ്റോള്‍, 1957-ല്‍ പൂര്‍ത്തിയായ ഗവര്‍ണറുടെ വസതി, സെന്റ് ജോണിന്റെ ശ്മശാനം, ദ് ലെമോയ് ന്‍ ആര്‍ട് ഗാലറി (The Lemoyne art gallery), ദ് മ്യൂസിയം ഒഫ് ഫ്ളോറിഡ ഹിസ്റ്ററി മുതലായവ ഇവിടത്തെ മനോഹര സൗധങ്ങളാണ്. ജാക്സണ്‍, ടാല്‍ക്വിന്‍ (Talquin) തടാകങ്ങളും, സെന്റ് മാര്‍ക്ക് വനസങ്കേതവും, ലൈറ്റ് ഹൗസും ഇതിനടുത്തായി സ്ഥിതിചെയ്യുന്നു.
[[Image:goodwood.png|200x|left|thumb|ടാലഹസിയിലെ ഗുഡ്വുഡ് പ്ലാന്റേഷന്‍ മന്ദിരം ]]
ഫ്ളോറിഡ എ&എം സര്‍വകലാശാല (1887) (Florida Agricultural & Mechanical University), ഫ്ളോറിഡ സ്റ്റേറ്റ് സര്‍വകലാശാല (1857), ജൂനിയര്‍ കോളെജ് എന്നിവയുടെ ആസ്ഥാനം കൂടിയാണ് ടാലഹസി നഗരം. 

1539-40-ലെ മഞ്ഞുകാലത്ത് സ്പാനിഷ് പര്യവേക്ഷകനായിരുന്ന ഹെര്‍ണാന്‍ ഡോ ദ സോട്ടോ ഇന്നത്തെ ആധുനിക ടാലഹസി നഗരം സ്ഥിതി ചെയ്യുന്ന പ്രദേശം സന്ദര്‍ശിക്കുകയുണ്ടായി. വംശനാശം സംഭവിച്ച അപലാച്ചീ ഇന്‍ഡ്യരുടെ ഒരു ഗ്രാമം അന്ന് അദ്ദേഹം ഇവിടെ കണ്ടെത്തിയിരുന്നു. ഏതാണ്ട് നൂറു വര്‍ഷത്തിനു ശേഷമാണ് റോമന്‍ കത്തോലിക്ക വിഭാഗത്തിലെ ഒരു സന്ന്യാസി സംഘമായ ഫ്രാന്‍സിസ്കന്‍സ് ഇവിടെ മിഷനറി പ്രവര്‍ത്തനം ആരംഭിച്ചത്. 1675 ആയപ്പോഴേക്കും ഏഴോളം മിഷനറി സംഘങ്ങള്‍ ഇവിടെ പ്രവര്‍ത്തിക്കുന്നുണ്ടായിരുന്നു. സാന്‍ ലൂയിസ് ദ ടാലിമാലി (San Luis de Talimali) ആയിരുന്നു ഇവയില്‍ ഏറ്റവും മുഖ്യം. 

1702-13-ലെ ക്യൂന്‍ ആനിയുടെ യുദ്ധകാലത്ത് (Queen Anne's War) ഇംഗ്ലീഷുകാരും ക്രീക് ഇന്‍ഡ്യരും ഇവിടത്തെ മിഷനറികളെ ആക്രമിക്കുകയുണ്ടായി. 1821-ല്‍ യു. എസ്. ഔദ്യോഗികമായി ഫ്ളോറിഡ ഏറ്റെടുത്തു. ഇതിനുശേഷമാണ് ഫ്ളോറിഡയുടെ തലസ്ഥാനമായി ടാലഹസിയെ തിരഞ്ഞെടുത്തത്. 1824-ല്‍ ആദ്യസംഘം കുടിയേറ്റക്കാര്‍ ടാലഹസിയില്‍ എത്തി. 

അമേരിക്കന്‍ ആഭ്യന്തര യുദ്ധകാലത്ത് മിസിസിപ്പി നദിക്ക് കിഴക്കായി യൂണിയന്‍ സേന കീഴടക്കാത്ത ഏക കോണ്‍ഫെഡറേറ്റഡ് തലസ്ഥാനം ടാലഹസി ആയിരുന്നു.