
== കുമ്പസാരം ==


== Confession ==

പാപങ്ങളെപ്പറ്റി പശ്ചാത്തപിച്ച്‌ വൈദികനോട്‌ ഏറ്റു പറഞ്ഞ്‌ പാപപ്പൊറുതി സ്വീകരിക്കുന്നതിനുള്ള ക്രസ്‌തവ കൂദാശ. "ഏറ്റു പറയുക' എന്നര്‍ഥമുള്ള ഒരു പോര്‍ച്ചുഗീസ്‌ പദത്തില്‍ ("കൊമ്പസാരെ') നിന്നുദ്‌ഭൂതമാണ്‌ ഈ പദം. ഈ കൂദാശയുടെ ബാഹ്യാനുഷ്‌ഠാനത്തിനും അര്‍ഥത്തിനും വ്യത്യാസമുണ്ടായിട്ടുണ്ട്‌. പാപി, ദൈവവും സഭയുമായി ഈ കൂദാശവഴി അനുരഞ്‌ജനപ്പെടുന്നു എന്നു ദ്യോതിപ്പിക്കുന്നതിന്‌ ആദിമ ക്രസ്‌തവ നൂറ്റാണ്ടുകളില്‍ (മൂന്നാം നൂറ്റാണ്ടുവരെ) ഇതിനു അനുരഞ്‌ജന കൂദാശ എന്നായിരുന്നു പേര്‌. പാപത്തിന്റെ ഗൗരവത്തിനനുസൃതമായി പ്രായശ്ചിത്തം ചെയ്‌താണ്‌ പാപമോചനം സ്വീകരിക്കേണ്ടത്‌ എന്ന ആശയത്തിന്‌ പ്രാധാന്യം നല്‌കിക്കൊണ്ട്‌ ഇടക്കാലത്ത്‌ (4, 5, 6 നൂറ്റാണ്ടുകളില്‍) ഈ കൂദാശയ്‌ക്ക്‌ പ്രായശ്ചിത്തത്തിന്റെ കൂദാശ എന്നുപേരുണ്ടായി. പില്‌ക്കാലത്തു പാപം ഏറ്റുപറയുന്നതിന്‌ പ്രാധാന്യം നല്‌കിക്കൊണ്ട്‌ ഈ കൂദാശ കുമ്പസാരം എന്നും വിളിക്കപ്പെട്ടു.
[[ചിത്രം:Vol7p684_The Confession - Pietro Longhi. Artist_ Pietro Longhi.jpg|thumb|കുമ്പസാരം-പീട്രാ ലോങ്‌ഗിയുടെ പെയിന്റിങ്‌]]
പരിശുദ്ധതമനായ ദൈവത്തെ സമീപിക്കുന്ന മനുഷ്യന്‍ പാപരഹിതനായിരിക്കണമെന്ന ആശയം സാര്‍വത്രികമാണ്‌. അക്കാരണത്താല്‍ പാപമോചനമെന്നത്‌ ദൈവാരാധനയുടെ ഭാഗമായി കരുതപ്പെടുന്നു. മാമോദീസാ പാപവിമോചകമാണെന്നതും ക്രിസ്‌തുവിന്റെ പരിത്രാണകര്‍മം മനുഷ്യരാശിയുടെ പാപമോചനത്തിനുള്ളതാണെന്നതും പാപങ്ങളുടെ മോചനത്തിനായി മനുഷ്യര്‍ക്കുവേണ്ടി "മുറിക്കപ്പെട്ട ശരീര'വും "ചിന്തപ്പെട്ട രക്ത'വുമാണ്‌  (ലൂക്കൊ. 22:19, മത്താ. 26:28, 1 കൊരി. 11:24) കുര്‍ബാന എന്നതുമൊക്കെ ക്രസ്‌തവ വിശ്വാസസംഹിതയുടെ ഭാഗമാണ്‌. കത്തോലിക്കാസഭയിലും അകത്തോലിക്കാസഭകളില്‍ പലതിലും ഉപയോഗത്തിലുള്ളതാണ്‌ പാപമോചനത്തിനായി പ്രത്യേകം നിശ്ചയിച്ചിട്ടുള്ള കുമ്പസാരം എന്ന കൂദാശ. സഭ അനുരഞ്‌ജനത്തിന്റെ അടയാളമാണ്‌. എല്ലാവരെയും അനുരഞ്‌ജനവും സമാധാനവും അറിയിക്കുകയും കൂദാശപരമായി അത്‌ ഓരോരുത്തര്‍ക്കും എത്തിച്ചുകൊടുക്കുകയുമാണ്‌ സഭയുടെ കര്‍ത്തവ്യം. പാപത്തെപ്പറ്റിയുള്ള മനസ്‌താപം, മേലില്‍ പാപം ചെയ്യുകയില്ലെന്നുള്ള പ്രതിജ്ഞ, പാപത്തിനുള്ള പ്രായശ്ചിത്തം, വൈദികനോടുള്ള പാപമേറ്റുപറച്ചില്‍, സഭയുടെ നാമത്തില്‍ വൈദികന്‍ ചെയ്യുന്നതായ പാപമോചനപ്രഖ്യാപനം ഇവയൊക്കെ കുമ്പസാരത്തിന്റെ ഘടകമാണ്‌.

ക്രസ്‌തവസഭകളില്‍ ഇന്ന്‌ പരസ്യമായും രഹസ്യമായും ഉള്ള പാപമോചന ശുശ്രൂഷകളും അവയോടൊപ്പംതന്നെ ഓരോ വ്യക്തിയും സമൂഹമൊന്നാകെയും കുമ്പസാരിക്കുന്ന ശുശ്രൂഷകളുമുണ്ട്‌. സമൂഹമൊന്നാകെയോ വ്യക്തികളായോ പാപം പരസ്യമായി സഭാസമൂഹത്തില്‍ ഏറ്റുപറയുന്ന രീതിയും സാമൂഹികമായ ശുശ്രൂഷകളോടുകൂടിയോ അല്ലാതെയോ ഓരോ വ്യക്തിയും പാപം രഹസ്യമായി ഏറ്റുപറയുന്ന രീതിയും വിവിധ സഭകളില്‍ ഇന്നും നിലനില്‍ക്കുന്നു.

ഒരിക്കല്‍ പരസ്യപാപം ചെയ്‌തിരുന്നവര്‍ ആരാധനാസമൂഹത്തില്‍നിന്ന്‌ ബഹിഷ്‌കരിക്കപ്പെടുകയും അവര്‍ പരസ്യമായി ദീര്‍ഘകാലം കഠിനപ്രായശ്ചിത്തം ചെയ്യുകയും തുടര്‍ന്ന്‌ പരസ്യമായി അനുരഞ്‌ജനപ്പെടുകയും ചെയ്‌തിരുന്നു. പില്‌ക്കാലത്ത്‌ കുമ്പസാരം രഹസ്യമായി മാത്രം ചെയ്‌താല്‍ മതി എന്നും വൈദികനോടു വെളിപ്പെടുത്തുന്നതായ പാപങ്ങളെ സംബന്ധിച്ച്‌ രഹസ്യം പാലിക്കുവാന്‍ അദ്ദേഹത്തിന്‌ കടപ്പാടുണ്ടെന്നുമുള്ള ധാരണയുണ്ടായി. 

രഹസ്യമായി കുമ്പസാരിക്കുന്നതിന്‌ "കുമ്പസാരക്കൂട്‌' ഉപയോഗിക്കുന്ന രീതി 16-ാം നൂറ്റാണ്ടില്‍ മിലാനിലെ കാര്‍ഡിനല്‍ ചാള്‍സ്‌ ബൊറോമിയോ നടപ്പിലാക്കി, ലത്തീന്‍ സഭകളില്‍ ഒന്നാകെ പ്രചരിപ്പിച്ചു. വൈദികനും ഇരുവശത്തും അനുതാപികള്‍ക്കും ഉപയോഗിക്കത്തക്കവിധത്തില്‍ മൂന്ന്‌ "അറകള്‍' ഉള്‍ക്കൊള്ളുന്നതാണ്‌ കുമ്പസാരക്കൂട്‌. കുമ്പസാരക്കൂട്‌ സ്‌ത്രീകളെ കുമ്പസാരിപ്പിക്കുന്നതിനായി ആദ്യം ഉപയോഗിച്ചുതുടങ്ങി. പിന്നീടത്‌ കുമ്പസാരത്തിന്റെ രംഗസംവിധാനവുമായി താദാത്മ്യം പ്രാപിച്ചു. തത്‌ഫലമായി ഈ കൂട്‌ അജ്ഞാതമായി കുമ്പസാരിക്കുന്നതിനുള്ള ഒരുപാധിയായിത്തീര്‍ന്നു.

ആദിമസഭയിലെ പരസ്യപ്രായശ്ചിത്തത്തിനാകട്ടെ കുമ്പസരിക്കുന്ന ആള്‍ ആരെന്ന്‌ കുമ്പസാരിപ്പിക്കുന്ന ആള്‍ അറിയരുതെന്ന ആശയമേ ഉണ്ടായിരുന്നില്ല. വൈദികന്‍ കുമ്പസാര രഹസ്യം പാലിച്ചിരുന്നു എന്നതല്ലാതെ അനുതാപി ആരെന്ന്‌ അറിയാതിരുന്നില്ല. ലത്തീന്‍ സ്വാധീനത ഏറ്റിട്ടില്ലാത്ത പൗരസ്‌ത്യ കത്തോലിക്കാസഭകളിലും മറ്റ്‌ അകത്തോലിക്കാ സഭകളിലും കുമ്പസാരം നടത്തുന്നത്‌ ദേവാലയ മധ്യത്തിലോ മദ്‌ബഹായുടെ വാതുക്കലോ മറ്റേതെങ്കിലും തുറന്ന സ്ഥലത്തോ വച്ചാണ്‌. റോമന്‍ റീത്തിലെ പുതിയ കൂദാശക്രമത്തിന്‌ (1973) കുമ്പസാരക്കൂടിനെപ്പറ്റി യാതൊരു നിര്‍ദേശവും നല്‌കാതെ "കുമ്പസാരത്തിനുള്ള സ്ഥല'മെന്ന പരാമര്‍ശമേയുള്ളുവെന്നത്‌ പ്രസ്‌താവ്യമാണ്‌.

റോമന്‍ റീത്തിലെ അനുരഞ്‌ജന കൂദാശയില്‍ മൂന്നു ക്രമങ്ങളുണ്ട്‌: 

(1) വൈദികനോട്‌ രഹസ്യമായി കുമ്പസാരിക്കാനുള്ള ക്രമം; 

(2) രഹസ്യ കുമ്പസാരത്തിനു മുമ്പ്‌ സമൂഹമൊന്നാകെ അതിനൊരുങ്ങുന്ന അനുതാപന ശുശ്രൂഷകള്‍ ഉള്‍ക്കൊള്ളുന്ന ക്രമം; 

(3) പൊതുവിലുള്ള അനുതാപ ശുശ്രൂഷയും പൊതുവായ ഏറ്റുപറച്ചിലും പൊതുവില്‍ സമൂഹത്തിന്‌ നല്‌കുന്ന പാപമോചനവും ഉള്‍ക്കൊള്ളുന്ന ക്രമം. ഈ മൂന്നു ക്രമങ്ങളിലും വേദപുസ്‌തക വായനയ്‌ക്കു പ്രാധാന്യമുണ്ട്‌. അക്കാരണത്താല്‍ ഈ കൂദാശ ആഘോഷമായ ഒരു അനുഷ്‌ഠാനമായി അനുഭവപ്പെടുന്നു.

(ഡോ. ജേക്കബ്‌ വെള്ളിയാന്‍)