
== ഓക്‌സൈഡുകള്‍ ==


== Oxides ==

മൂലകങ്ങള്‍ ഓക്‌സിജനോടു ചേര്‍ന്നുണ്ടാകുന്ന യൗഗികങ്ങള്‍. അനവധി ലോഹ-അലോഹ ഓക്‌സൈഡുകള്‍ ഭൂപ്രതലത്തില്‍ കാണപ്പെടുന്നു. സര്‍വസാധാരണമായ ജലം ( H<sub>2</sub>O), കാര്‍ബണ്‍ ഡൈഓക്‌സൈഡ്‌(CO<sub>2</sub>), അലുമിനിയം അയിരായ ബോക്‌സൈറ്റ്‌ (Al<sub>2</sub>O<sub>3</sub>), ടൈറ്റാനിയം അയിരായ റൂടൈല്‍(TiO<sub>2</sub>), സിലിക്കണ്‍ യൗഗികമായ സിലിക്ക, ക്വാര്‍ട്‌സ്‌ മുതലായവ (SiO<sub>2</sub>), ഇരുമ്പയിരുകളായ മാഗ്നറ്റൈറ്റ്‌ (Fe<sub>3</sub>O<sub>4</sub>), ഹെമറ്റൈറ്റ്‌ (Fe<sub>2</sub>O<sub>3</sub>) തുടങ്ങിയവ മാങ്‌ഗനീസ്‌ അയിരായ പൈറോളുസൈറ്റ്‌ (MnO<sub>2</sub>) സിര്‍ക്കോണിയം യൗഗികമായ സിര്‍ക്കണ്‍ (ZrO<sub>2</sub>), ടിന്‍, അയിരായ കാസിറ്ററൈറ്റ്‌ (SnO<sub>2</sub>) ഇത്യാദി ഖനിജപദാര്‍ഥങ്ങളെല്ലാം ഓക്‌സൈഡുകളാകുന്നു.

നിഷ്‌ക്രിയ വാതകങ്ങളൊഴിച്ചുള്ള മിക്ക മൂലകങ്ങളും നേരിട്ടോ അല്ലാതെയോ ഓക്‌സിജനുമായി സംയോജിക്കുന്നുണ്ട്‌. ക്രിയാശീലമുള്ളവ ഓക്‌സിജനിലും വായുവിലും കത്തുന്നു; ചിലത്‌ ചൂടാക്കിയാല്‍ സംയോജിക്കുന്നു. നേരിട്ടു സംയോജിക്കാത്ത ഓക്‌സൈഡുകള്‍ അനുയോജ്യമായ യൗഗികങ്ങളുടെ താപവിഘടനത്താല്‍ ഉണ്ടാകുന്നു. ലോഹ ഓക്‌സൈഡുകള്‍ മിക്കതും ഹൈഡ്രാക്‌സൈഡ്‌, കാര്‍ബണേറ്റ്‌, ഓക്‌സലേറ്റ്‌, നൈട്രറ്റ്‌ എന്നീ ലവണങ്ങളുടെ വിഘടനത്താല്‍ നിര്‍മിക്കാം. (ഉദാഹരണമായി കോപ്പര്‍ ഓക്‌സൈഡ്‌).

ഓക്‌സൈഡുകളില്‍ അയോണിക ഓക്‌സൈഡുകളും (വിദ്യുത്‌-ഋണത വളരെ കുറവുള്ള മൂലകങ്ങളുടേത്‌) സഹസംയോജിത ഓക്‌സൈഡുകളുമുണ്ട്‌. അയോണിക ഓക്‌സൈഡുകള്‍ക്ക്‌ താരതമ്യേന ഉയര്‍ന്ന ഉരുകല്‍നില, ഉയര്‍ന്ന തിളനില എന്നിവയുണ്ട്‌.  MgO, CaO, Al<sub>2</sub>O<sub>3</sub> തുടങ്ങിയവ ഇക്കൂട്ടത്തില്‍പ്പെടുന്നു. അലോഹ ഓക്‌സൈഡുകള്‍ മിക്കവാറും താണ ദ്രവണ ക്വഥനാങ്കങ്ങള്‍ ഉള്ളവയാകുന്നു. പലതും വാതകങ്ങളുമാണ്‌ (സാധാരണ പരിതഃസ്ഥിതികളില്‍). നൈട്രജന്‍ ഓക്‌സൈഡുകള്‍, കാര്‍ബണ്‍ ഡൈഓക്‌സൈഡ്‌ മുതലായവ ഉദാഹരണങ്ങള്‍.
രാസസ്വഭാവത്തിന്റെ അടിസ്ഥാനത്തില്‍ ഓക്‌സൈഡുകളെ വര്‍ഗീകരിക്കാറുണ്ട്‌.

1. അമ്ലീയ-ഓക്‌സൈഡുകള്‍. പല അലോഹ ഓക്‌സൈഡുകള്‍ക്കും അമ്ലഗുണമുണ്ട്‌ (ഉദാ.P<sub>2</sub>O<sub>5</sub>, P<sub>2</sub>O<sub>3</sub>, CO<sub>2</sub>, SiO<sub>2</sub>, N<sub>2</sub>O<sub>5</sub>, N<sub>2</sub>O<sub>3</sub>, B<sub>2</sub>O<sub>3</sub>)ഇവ ബേസുകളോടു ചേര്‍ന്നു ലവണങ്ങള്‍ നല്‍കുന്നു. 

2. ക്ഷാരീയ-ഓക്‌സൈഡുകള്‍. ഇവ ബേസികഗുണമുള്ള ലോഹ-ഓക്‌സൈഡുകളാണ്‌. CaO, Na<sub>2</sub>O, Li<sub>2</sub>O, MgO തുടങ്ങിയവ അയോണിക-ബേസിക-ഓക്‌സൈഡുകളാകുന്നു. വളരെ ബേസിക സ്വഭാവമുള്ളവ ജലത്തില്‍ ലയിച്ച്‌ ആല്‍ക്കലികള്‍ നല്‍കുന്നു. 

3. ഉഭയധര്‍മി-ഓക്‌സൈഡുകള്‍. ഇവ പ്രബല ബേസുകളോട്‌ അമ്ലക്ഷാരീയമായും പ്രബല-അമ്ലങ്ങളോട്‌ ക്ഷാരീയമായും പെരുമാറുന്നു. Al<sub>2</sub>O<sub>3</sub>, SnO<sub>2</sub>, ZnO മുതലായ ഓക്‌സൈഡുകള്‍ ഉദാഹരണങ്ങളാണ്‌

4. ഉദാസീന-ഓക്‌സൈഡുകള്‍. ഇവയ്‌ക്ക്‌ അമ്ല സ്വഭാവമോ ക്ഷാരീയ സ്വഭാവമോ ഇല്ല.  H<sub>2</sub>O, CO, NO, എന്നിവ ഇക്കൂട്ടത്തില്‍പ്പെടുന്നു.

5. സലൈന്‍-ഓക്‌സൈഡുകള്‍. ഇവ ഒരേ ലോഹത്തിന്റെതന്നെ രണ്ടു സംയോജകതാവസ്ഥകളിലുള്ള ഓക്‌സൈഡുകളുടെ യൗഗികമെന്നപോലെ കരുതാവുന്നവയാണ്‌. സംയോജകത കുറഞ്ഞ ലോഹഓക്‌സൈഡ്‌ ക്ഷാരീയമായും കൂടിയത്‌ അമ്ലീയമായും കരുതാം. ഉദാ. Fe<sub>3</sub>O<sub>4</sub> = FeO. Fe<sub>2</sub>O<sub>3</sub>

6. സബ്‌-ഓക്‌സൈഡുകള്‍. സാധാരണ ഓക്‌സൈഡില്‍ കുറവായി ഓക്‌സിജന്‍ അടങ്ങുന്ന ഓക്‌സൈഡുകളാണിവ. Pb<sub>2</sub>O,  Ag<sub>4</sub>O എന്നിവ ഇക്കൂട്ടത്തില്‍പ്പെടുന്നു.

7. ഉച്ചതര-ഓക്‌സൈഡുകള്‍. സാധാരണ ഓക്‌സൈഡിലും കൂടുതല്‍ ഓക്‌സിജനുള്ളവയാണിവ. ഉദാ. PbO<sub>2</sub>, MNO<sub>2</sub>, NO<sub>2</sub> ഇവയെ ഡൈഓക്‌സൈഡ്‌ എന്നും പറയാം.

8. പെറോക്‌സൈഡുകള്‍. ഹൈഡ്രജന്‍ പെറോക്‌സൈഡിന്റെ ലവണങ്ങളായി കരുതാവുന്ന ഇവയില്‍ പെറോക്‌സൈഡ്‌ അയോണ്‍ ഉണ്ട്‌. തന്മൂലം അമ്ലമായി ഇവ ഹൈഡ്രജന്‍ പെറോക്‌സൈഡ്‌ തരുന്നു. Na<sub>2</sub>O<sub>2</sub>,BaO<sub>2</sub>  ഇവ ഇക്കൂട്ടത്തിലുള്ളവയാണ്‌.

ഏതാനും കാര്‍ബണിക യൗഗികങ്ങള്‍ അമീനുകള്‍, ഫോസ്‌ഫീനുകള്‍, സള്‍ഫൈഡുകള്‍-ഓക്‌സിജനോടോ ഓക്‌സീകാരകങ്ങളോടോ സംയോജിപ്പിച്ചുണ്ടാക്കുന്ന യൗഗികങ്ങളെയും ഓക്‌സൈഡുകള്‍ എന്നുപറയാറുണ്ട്‌; അമീന്‍ ഓക്‌സൈഡ്‌, ഫോസ്‌ഫീന്‍ ഓക്‌സൈഡ്‌, സള്‍ഫോക്‌സൈഡ്‌ എന്നിവയില്‍ ഓക്‌സിജന്‍ യഥാക്രമം N, P, S  എന്നീ അണുക്കളോട്‌ സഹസംയോജന-ബദ്ധമായിരിക്കും. ഇവ കൂടാതെ, ഒലിഫീന്‍ ഓക്‌സൈഡുകള്‍ ഉണ്ട്‌. അവ യഥാര്‍ഥത്തില്‍ ചാക്രിക-ഈതറുകളാകുന്നു.

(ഡോ. കെ.പി. ധര്‍മരാജയ്യര്‍)