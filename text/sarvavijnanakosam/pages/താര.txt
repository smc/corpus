=താര= 

''രാമായണ''ത്തില്‍ പരാമര്‍ശിക്കപ്പെട്ടിട്ടുള്ള വാനര രാജ്ഞി. വാനര രാജാവായ ബാലിയുടെ പത്നി. അംഗദന്‍ ഇവരുടെ പുത്രനാണ്. സുഷേണന്‍ എന്ന വാനരന്റെ പുത്രിയാണ് താരയെന്നും, പാലാഴി മഥനത്തില്‍ നിന്നാണ് താര ജനിച്ചതെന്നും രണ്ട് ഐതിഹ്യങ്ങള്‍ നിലനില്ക്കുന്നു. ബാലിയുടെ മരണശേഷം താര സുഗ്രീവന്റെ പത്നിയായി. മഹാപാതകനാശനത്തിനായി കേരളീയര്‍ ഭജിക്കുന്ന പഞ്ചകന്യകമാരില്‍ ഒരാളാണ് താര.

താരയെ വിവേകമതിയും അനുനയപാടവമുള്ളവളുമായാണ് രാമായണത്തില്‍ ചിത്രീകരിച്ചിരിക്കുന്നത്. സുഗ്രീവനോട് ഏറ്റുമുട്ടാന്‍ പോയ ബാലിയെ യുക്തിവാദംകൊണ്ട് താര പിന്തിരിപ്പിക്കുവാന്‍ ശ്രമിച്ചതായി ''രാമായണ''ത്തില്‍ കാണുന്നു. സീതാന്വേഷണത്തിനു താമസം നേരിടുന്നതില്‍ കുപിതനായി സുഗ്രീവനെ ശകാരിക്കാനെത്തിയ ലക്ഷ്മണനെ ശാന്തനാക്കിയതും താരയാണ്. 

ദേവഗുരുവായ ബൃഹസ്പതിയുടെ പത്നിയുടെ പേരും താര എന്നാണെന്ന് ''ഭാഗവതം'' നവമസ്കന്ധത്തില്‍ കാണുന്നു. അതിസുന്ദരിയായ താര ചന്ദ്രനില്‍ അനുരക്തയായി അദ്ദേഹത്തോടൊപ്പം താമസം ആരംഭിച്ചു. ഇത് ദേവന്മാരെ പ്രകോപിപ്പിക്കുകയും അവര്‍ ചന്ദ്രനെതിരെ യുദ്ധത്തിനൊരുങ്ങുകയും ചെയ്തു. ഒത്തുതീര്‍പ്പിനു തയ്യാറായ ചന്ദ്രന്‍ താരയെ ഭര്‍ത്തൃഗൃഹത്തിലേക്ക് മടക്കി അയച്ചു. ഇതിനു ശേഷം താര പ്രസവിച്ച ശിശു(ബുധന്‍)വിനെച്ചൊല്ലി ബൃഹസ്പതിയും ചന്ദ്രനും തമ്മില്‍ തര്‍ക്കമുണ്ടായി. ശിശു ചന്ദ്രന്റേതാണെന്ന് താര പറയുകയും ആ ശിശു ചന്ദ്രഭവനത്തില്‍ വളരുകയും ചെയ്തു. 

തിബത്തന്‍ പുരാണത്തിലെ ഒരു ദേവിക്കും താര എന്നു പേരുള്ളതായി കാണുന്നു. ബുദ്ധമതത്തില്‍ അവലോകിതേശ്വരന്റെ  തുല്യ പദവിയുള്ള സ്ത്രീ രൂപമാണ് താരാദേവി.