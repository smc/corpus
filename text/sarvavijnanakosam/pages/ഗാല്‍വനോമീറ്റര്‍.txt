==ഗാല്‍വനോമീറ്റര്‍==

==Galvanometer==

വൈദ്യുതിയുടെ സാന്നിധ്യം, ദിശ എന്നിവ മനസ്സിലാക്കുന്നതിനും അതിന്റെ തീവ്രത അളക്കുന്നതിനും ഉപയോഗിക്കുന്ന ഉപകരണം. 18-ാം ശ.-ത്തില്‍ ജീവിച്ചിരുന്ന ല്യൂജി ഗാല്‍വനി (Luigi Galvani) എന്ന ഇറ്റാലിയന്‍ ഭൗതികശാസ്ത്രജ്ഞന്റെ പേരില്‍ നിന്നാണ് ഗാല്‍വനോമീറ്റര്‍ എന്ന പദം നിഷ്പന്നമായത്. ഒരു ചാലകത്തിലൂടെ വൈദ്യുതി പ്രവഹിക്കുമ്പോള്‍ ചാലകത്തിനു ചുറ്റും കാന്തികമണ്ഡലം സൃഷ്ടിക്കപ്പെടുന്നു. വിദ്യുത്കാന്തികത (electro magnetism) എന്നറിയപ്പെടുന്ന ഈ പ്രഭാവമാണ് ഗാല്‍വനോമീറ്ററിന്റെ നിര്‍മാണത്തിന് അടിസ്ഥാനം. വിദ്യുത്കാന്തികത കണ്ടുപിടിച്ചത് (1819) ഈഴ്സ്റ്റെഡ് (Han Christian Oersted) എന്ന ഡാനിഷ് ഭൗതികശാസ്ത്രജ്ഞനാണ്. 1820-ല്‍ യൊഹന്‍ ഷ്വൈഗര്‍ (Johann Schweiger) ആദ്യത്തെ ഗാല്‍വനോമീറ്റര്‍ നിര്‍മിച്ചു. 1880-കളില്‍ ഷാക് ആര്‍സെന്‍ ഓര്‍സോങ്വാലും (Jacques Arsene d' Arsonval) എഡ്വേഡ് വെസ്റ്റണും (Edward Weston) ചേര്‍ന്നു പരിഷ്കരിച്ച ഗാല്‍വനോമീറ്റര്‍ നിര്‍മിച്ചു. ഇപ്പോള്‍ രണ്ടുതരം ഗാല്‍വനോമീറ്ററുകളാണ് ഉപയോഗത്തിലുള്ളത്.
   
1.ചലന ചുരുള്‍ (moving coil) ഗാല്‍വനോമീറ്റര്‍

2.ചലനകാന്ത (moving magnet) ഗാല്‍വനോമീറ്റര്‍
   
1. '''ചലനചുരുള്‍ ഗാല്‍വനോമീറ്റര്‍.''' കവചിത ചെമ്പുകമ്പികൊണ്ടു നിര്‍മിച്ചതും വളരെ ഭാരം കുറഞ്ഞതും ദീര്‍ഘചതുരാകൃതിയുള്ളതും ചലനസ്വാതന്ത്യ്രമുള്ളതുമായ ഒരു കമ്പിവലയം ഒരു സ്ഥിര ലാടകാന്തത്തിന്റെ ധ്രുവങ്ങള്‍ക്കിടയില്‍ ഫോസ്ഫര്‍ ബ്രോണ്‍സ് നാടകൊണ്ടു കെട്ടിത്തൂക്കിയിരിക്കുന്നു. ഈ നാടവഴിയാണ് കമ്പിച്ചുരുളിലേക്കു വൈദ്യുതധാര പ്രവഹിക്കുന്നത്. അയഞ്ഞുകിടക്കുന്ന ഒരു സ്പ്രിങ്ങിലാണ് വലയത്തിലെ കമ്പിയുടെ മറ്റേയറ്റം ഘടിപ്പിച്ചിരിക്കുന്നത്. ഈ സ്പ്രിങ് വഴി ധാര വലയത്തില്‍ നിന്ന് ബഹിര്‍ഗമിക്കുന്നു. വൈദ്യുതി പ്രവഹിക്കുമ്പോള്‍ AD, BC എന്നീ ഭുജങ്ങളില്‍ വിപരീത ദിശകളില്‍ തുല്യബലങ്ങള്‍ പ്രവര്‍ത്തിക്കുന്നു (AB, DC എന്നീ ഭുജങ്ങളില്‍ പ്രവാഹം കാന്തികമണ്ഡലത്തിനു സമാന്തരമാകയാല്‍ ബലം അനുഭവപ്പെടുന്നില്ല). തന്മൂലമുണ്ടാകുന്ന ബലയുഗ്മം കമ്പിവലയത്തെ തിരിയാന്‍ പ്രേരിപ്പിക്കുന്നു. വലയത്തിനുണ്ടാകുന്ന വ്യതിയാനം (വിഭ്രംശം) θ ആയാല്‍ വൈദ്യുതിയുടെ തീവ്രത i വലയത്തിന്റെ വിഭ്രംശത്തിന് ആനുപാതികമായിരിക്കും  (i α  θ) വലയത്തിന്റെ വിഭ്രംശം അളക്കാന്‍ സാധാരണ രണ്ടു മാര്‍ഗങ്ങള്‍ അവലംബിക്കുന്നു.

[[ചിത്രം:Galvanometer scheme.png|200px|right|thumb|ഗാല്‍വനോമീറ്ററിന്റെ ഘടന (ഉള്‍ച്ചിത്രം: ഗാല്‍വനോമീറ്റര്‍]]
    
i. വലയത്തില്‍, അതിന്റെ തലത്തിനു ലംബമായി ഭാരം വളരെ കുറഞ്ഞ സൂചിക ഉറപ്പിക്കുക. ഈ സൂചികയുടെ സ്വതന്ത്രാഗ്രം ഒരു സ്കെയിലിനു മുകളിലൂടെ നീങ്ങുന്നു. വലയത്തിന്റെ വിഭ്രംശം സ്കെയിലില്‍നിന്നു നേരിട്ടളക്കാം.
    
ii. നിലംബന തന്ത്രിയില്‍ വളരെ ചെറിയ ഒരു കണ്ണാടിക്കഷണം ഒട്ടിക്കുന്നു. വലയം തിരിയുമ്പോള്‍ നിലംബന തന്ത്രിയോടൊപ്പം ഈ കണ്ണാടിയും തിരിയുന്നു. കണ്ണാടി നിശ്ചിത കോണില്‍ തിരിയുമ്പോള്‍ പ്രതിഫലനരശ്മി രണ്ടിരട്ടി കോണില്‍ തിരിയുന്നു എന്ന തത്ത്വം ഉപയോഗിച്ച് വലയത്തിന്റെ വിഭ്രംശം അളക്കാം. വളരെ കൃത്യമായി ധാര അളക്കുന്നതിന് ഈ രീതി ഉപയോഗിക്കുന്നു.
   
2. '''ചലനകാന്ത ഗാല്‍വനോമീറ്റര്‍.''' വൈദ്യുതവാഹിയായ ഒരു കമ്പിവലയത്തിന്റെ കേന്ദ്രത്തില്‍ക്കൂടി വലയം ഉള്‍ക്കൊള്ളുന്ന തലത്തിനു ലംബമായി ഒരു കാന്തികമണ്ഡലം പ്രവര്‍ത്തിക്കുന്നുണ്ട്. തന്മൂലം വലയത്തിന്റെ കേന്ദ്രത്തില്‍ ചലനസ്വാതന്ത്യ്രമുള്ള ഒരു കാന്തസൂചി താങ്ങിനിര്‍ത്തിയാല്‍ ആ സൂചിക്ക് വ്യതിയാനം സംഭവിക്കുന്നു. സൂചിയുടെ വ്യതിചലനം പ്രവാഹത്തിന്റെ സാന്നിധ്യത്തെയും വ്യതിചലനത്തിന്റെ അളവ് ധാരയെയും കുറിക്കുന്നു. ഈ തത്ത്വം അനുസരിച്ചാണ് ചലനകാന്ത ഗാല്‍വനോമീറ്റര്‍ പ്രവര്‍ത്തിക്കുന്നത്.
   
ടാന്‍ജന്റ് ഗാല്‍വനോമീറ്റര്‍ (T.G.) ഒരുതരം ചലനകാന്ത ഗാല്‍വനോമീറ്ററാണ്.
   
സാധാരണമായി ഉപയോഗിക്കാവുന്ന അമ്മീറ്റര്‍ (ammeter). വോള്‍ട്ട്മീറ്റര്‍ (voltmeter) എന്നിവ ഗാല്‍വനോമീറ്ററിന്റെ വിവിധരൂപങ്ങളാണ്. അമ്മീറ്ററില്‍ ഗാല്‍വനോമീറ്ററിനു സമാന്തരമായി താഴ്ന്ന പ്രതിരോധമുള്ള ഒരു പാത (shunt) ഘടിപ്പിച്ചിരിക്കുന്നു. വിദ്യുത്പരിപഥത്തില്‍ (electric circuit) എല്ലായ്പ്പോഴും അമ്മീറ്റര്‍, ശ്രേണിയില്‍ (series) ഘടിപ്പിച്ച് വിദ്യുത്ധാര അളക്കുന്നു. പരിപഥത്തിലെ ഏതു സ്ഥാനങ്ങള്‍ക്കിടയ്ക്കുള്ള പൊട്ടെന്‍ഷ്യല്‍ അന്തരമാണോ കാണേണ്ടത് ആ സ്ഥാനങ്ങള്‍ക്കു സമാന്തരമായി വോള്‍ട്ട്മീറ്റര്‍ ഘടിപ്പിച്ച് പൊട്ടെന്‍ഷ്യല്‍ അന്തരവും അളക്കാം.
   
ആട്ടോമൊബൈലുകളില്‍ ബാറ്ററി ചാര്‍ജ് ചെയ്യുന്നതിന്റെയും ഡിസ്ചാര്‍ജ് ചെയ്യുന്നതിന്റെയും അളവുകുറിക്കുന്ന ആംപിയര്‍ മീറ്റര്‍ അമ്മീറ്റര്‍ തന്നെയാണ്.
   
സാധാരണയായി ഉയര്‍ന്ന വൈദ്യുതധാരാമാപനത്തിന് അമ്മീറ്ററും സൂക്ഷ്മധാരാമാപത്തിന് (10<sup>-8</sup> to 10<sup>-3</sup> amp) ഗാല്‍വനോ മീറ്ററും ഉപയോഗിക്കുന്നു.
   
10<sup>-11</sup> ആംപിയര്‍വരെ അളക്കുന്ന ഗാല്‍വനോമീറ്ററുകള്‍ നിര്‍മിക്കപ്പെട്ടിട്ടുണ്ട്. ആധുനിക ഡിജിറ്റല്‍ ഉപകരണങ്ങള്‍ ഗാല്‍വനോമീറ്ററുകള്‍ക്കു പകരമായും ഉപയോഗിച്ചുവരുന്നു. നോ: അമ്മീറ്റര്‍