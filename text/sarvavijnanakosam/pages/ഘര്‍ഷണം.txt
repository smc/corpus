==ഘര്‍ഷണം==

==Friction==

അന്യോന്യം സ്പര്‍ശിച്ചിരിക്കുന്ന രണ്ടു വസ്തുക്കളുടെ സ്പര്‍ശപ്രതലഭാഗത്ത് പരസ്പരം നിരങ്ങിനീങ്ങുന്നതിനെതിരായി പ്രതിരോധം അനുഭവപ്പെടുന്ന പ്രതിഭാസം.
   
ഒരു വസ്തു മറ്റൊന്നിനുമുകളില്‍ ഉരസ്സി നീങ്ങുമ്പോള്‍ വസ്തു നീങ്ങുന്നതിന്റെ എതിര്‍ദിശയില്‍ പ്രതിരോധബലം അനുഭവപ്പെടുന്നു. ഈ പ്രതിരോധബലത്തെ ഘര്‍ഷണ പ്രതിരോധം അഥവാ ഘര്‍ഷണ ബലം എന്നു വിളിക്കുന്നു. ഈ പ്രതിരോധം തരണം ചെയ്തുകൊണ്ടു മാത്രമേ വസ്തുവിനു നീങ്ങാന്‍ കഴിയുകയുള്ളൂ. അതുകൊണ്ട് വസ്തുവിനെ തള്ളിനീക്കണമെങ്കില്‍ പ്രയോഗിക്കേണ്ട ബലം ഘര്‍ഷണബലത്തെക്കാള്‍ കൂടിയിരിക്കണം. ഘര്‍ഷണപ്രതിരോധം കൂടുതലാണെങ്കില്‍ പ്രയോഗിക്കേണ്ട ബലവും വലുതായിരിക്കും. യന്ത്രങ്ങളുടെയും യന്ത്രാവലികളുടെയും പ്രവര്‍ത്തനത്തെ ഘര്‍ഷണം സാരമായി ബാധിക്കുന്നു. ഒരു വസ്തു ചലിപ്പിക്കുക, വേണ്ടപ്പോള്‍ നിര്‍ത്തുക, അതിന്റെ വേഗത്തില്‍ വ്യതിയാനം വരുത്തുക ഇവയെല്ലാം ഘര്‍ഷണം വഴിയാണ് നാം സാധിക്കാറുള്ളത്. ഘര്‍ഷണമില്ലാത്ത ഒരവസ്ഥയില്‍ നമുക്ക് നടക്കാനോ വണ്ടി ഓടിക്കാനോ ഒരു സാധനം കൈകൊണ്ട് പിടിക്കാനോ സാധിക്കുകയില്ല.
   
തുടര്‍ച്ചയായി പ്രവര്‍ത്തിക്കുന്ന ഒരു യന്ത്രത്തില്‍ ഘര്‍ഷണം പലപ്പോഴും ഒരു ശല്യമാണ്. കാരണം, ഘര്‍ഷണപ്രതിരോധം നേരിടുന്നതിനു വേണ്ടിമാത്രം വളരെയധികം ഊര്‍ജം ഉപയോഗിക്കേണ്ടിവരാറുണ്ട്. ഇങ്ങനെ ഉപയോഗിക്കപ്പെടുന്ന ഊര്‍ജമാകട്ടെ ഉരസല്‍ പ്രതലങ്ങളെ ക്രമാധികം ചൂടുപിടിപ്പിക്കാതിരിക്കാന്‍ തണുപ്പിക്കേണ്ടി വരുക സാധാരണമാണ്. മാത്രവുമല്ല, ഘര്‍ഷണംമൂലം പ്രതലങ്ങള്‍ക്ക് തേയ്മാനം (wear) സംഭവിക്കുകയും ചെയ്യും. ലോകത്തിലെ മൊത്തം ഉത്പാദനശേഷിയുടെ തന്നെ ഒരു നല്ല ശതമാനം ഇപ്രകാരം തേയ്മാനം വന്ന ഭാഗങ്ങള്‍ വീണ്ടും ഉണ്ടാക്കുന്നതിനുവേണ്ടിയാണ് വിനിയോഗിക്കപ്പെടുന്നത്.
  
ഒരു മനുഷ്യന്‍ P എന്ന ബലം പ്രയോഗിച്ച് ഒരു തെന്നുവണ്ടി വലിച്ചു നീക്കുന്നുവെന്നു വിചാരിക്കുക (ചിത്രം 1). ഭൂമിക്കും വണ്ടിക്കുമിടയില്‍ ഘര്‍ഷണപ്രതിരോധം അനുഭവപ്പെടുന്നു. ഇത്  F ആണെന്നിരിക്കട്ടെ.

[[ചിത്രം:Vol10 pg 595 scr00.png]]
   
വണ്ടി വലിച്ചുനീക്കണമെങ്കില്‍ P എന്ന ബലത്തിന്റെ തിരശ്ചീന ഘടകം, അതായത്,  P cos θ , F നെക്കാള്‍ കൂടിയിരിക്കണം. ബലം കുറവായിരിക്കുമ്പോള്‍ വണ്ടി ചലിക്കുന്നില്ല. ക്രമേണ P cos θ വര്‍ധിപ്പിക്കുമ്പോള്‍ ഒരു ഘട്ടത്തില്‍ അത് F ന് തുല്യമായി വരുന്നു. ഈ അവസ്ഥയെ പരമ ഘര്‍ഷണം (limiting friction) എന്നുപറയുന്നു. P cos θ അല്പംകൂടി വര്‍ധിക്കുന്നതോടെ വസ്തു ചലിച്ചു തുടങ്ങുന്നു. വസ്തു നിശ്ചാലവസ്ഥയില്‍ ആയിരിക്കുമ്പോള്‍ അനുഭവപ്പെടുന്ന ഘര്‍ഷണത്തെ സ്ഥൈതിക ഘര്‍ഷണം (static friction) എന്നുപറയുന്നു. നീങ്ങിക്കൊണ്ടിരിക്കുന്ന അവസ്ഥയില്‍ അനുഭവപ്പെടുന്ന ഘര്‍ഷണബലം പരമഘര്‍ഷണത്തേക്കാള്‍ അല്പം കുറഞ്ഞിരിക്കും. ഗതികഘര്‍ഷണം (dynamicfriction) എന്നാണ് ഇതിനെ വിളിക്കുക.
   
'''ഘര്‍ഷണ നിയമങ്ങള്‍'''. രണ്ടു വരണ്ട പ്രതലങ്ങള്‍ ഒന്ന് മറ്റൊന്നിനുമുകളില്‍ ഉരസ്സിനീങ്ങുമ്പോള്‍ അനുഭവപ്പെടുന്ന ഘര്‍ഷണം ചില നിയമങ്ങള്‍ അനുസരിക്കുന്നുണ്ട്. കൂളുംമ്പ് (Coulomb), വെസ്റ്റിങ് ഹൗസ്, മോറിന്‍ തുടങ്ങിയ ശാസ്ത്രജ്ഞന്മാരുടെ പരീക്ഷണങ്ങളാണ് ഈ നിയമങ്ങള്‍ക്ക് രൂപംകൊടുത്തത്. പ്രധാനപ്പെട്ട ഘര്‍ഷണ നിയമങ്ങള്‍ ഇനി പറയുന്നവയാണ്.

1.പരസ്പരം ഉരസ്സുന്ന രണ്ടു വസ്തുക്കള്‍ക്കിടയിലെ ഘര്‍ഷണം അവയുടെ സ്പര്‍ശതല വിസ്തീര്‍ണത്തെ ആശ്രയിക്കുന്നില്ല. ചിത്രം 2-ല്‍ ഇത് വിശദമാക്കിയിരിക്കുന്നു.

2.ഘര്‍ഷണബലം പ്രതലങ്ങളിന്മേല്‍ വരുന്ന ലംബഭാരത്തിന് ആനുപാതികമായി വ്യത്യാസപ്പെടുന്നു. ചിത്രം 3-ല്‍ ഈ നിയമം വിശദീകരിച്ചിരിക്കുന്നു.

3.താപനിലയില്‍ വരുന്ന വ്യത്യാസങ്ങള്‍ അല്പമായെങ്കിലും ഘര്‍ഷണബലത്തെ ബാധിക്കുന്നതാണ്.

4.നിരങ്ങല്‍വേഗം വര്‍ധിക്കുന്നതിനനുസരിച്ച് ഘര്‍ഷണബലം കുറയുന്നു.

5.വളരെ ചെറിയ നിരങ്ങല്‍ വേഗത്തില്‍, ഘര്‍ഷണബലം വേഗത്തെ ആശ്രയിക്കുന്നില്ല.

6.ഒരു ദിശയില്‍ സഞ്ചരിക്കുന്ന വസ്തു നേരേ തിരിച്ച് സഞ്ചരിക്കുമ്പോള്‍ ഒരു പക്ഷേ ഘര്‍ഷണബലം വര്‍ധിച്ചേക്കാം.

[[ചിത്രം:Vol10 pag595 sc04.png]]

[[ചിത്രം:Vol 10 pg 595-scr07.png]]
   
'''ഘര്‍ഷണ സിദ്ധാന്തങ്ങള്‍.''' ഘര്‍ഷണത്തിനുള്ള കാരണത്തെ സംബന്ധിച്ച മുഖ്യസിദ്ധാന്തങ്ങള്‍ താഴെ പറയുന്ന നാലു വിഭാഗങ്ങളില്‍പ്പെടുന്നവയായി കണക്കാക്കാം.

[[ചിത്രം:Vol 10 pg 565 scr08.png|300px]]
   
പ്രതലത്തിലെ സൂക്ഷ്മങ്ങളായ തരിപ്പുകള്‍ micro asperites) പരസ്പരം തട്ടി നിമ്നോഭാഗങ്ങള്‍ പരസ്പരം കൊളുത്തി നില്‍ക്കുന്നതായി സങ്കല്പിച്ചാണ് ആദ്യവിഭാഗം വിശദീകരണങ്ങള്‍. 17-ാം ശ.-ത്തിന്റെ അവസാനത്തിലും 18-ാം ശതകത്തിന്റെ ആരംഭത്തിലും പ്രചരിച്ച ഈ ഘര്‍ഷണസിദ്ധാന്തങ്ങള്‍ വെറും ജ്യാമിതീയ പരിഗണനകളെ ആസ്പദമാക്കിയുള്ളവയാണ്. കാരണം, അന്ന് ഖരപദാര്‍ഥങ്ങളെല്ലാം ദൃഢ (rigid)മാണെന്നാണ് വിശ്വസിച്ചിരുന്നത്. ഈ സിദ്ധാന്ത പ്രകാരം ഘര്‍ഷണഗുണാങ്കം  (Friction Coefficient) എന്നതു ഓരോ തരിപ്പുകളുടെയും ചരിയല്‍ കോണത്തിന്റെ സ്പര്‍ശകം (tangent) ആയിരിക്കുമെന്നാണ് കരുതിപ്പോന്നത്. 
   
പ്രതലങ്ങളിലെ കണികകള്‍ തമ്മിലുള്ള ആകര്‍ഷണത്തിന്റെ ഫലമായിട്ടാണ് ഘര്‍ഷണം അനുഭവപ്പെടുന്നതെന്ന് തെളിയിക്കാനാണ് രണ്ടാമത്തെ വിഭാഗത്തിലെ സിദ്ധാന്തങ്ങള്‍ ശ്രമിച്ചുകാണുന്നത്. പരുക്കനായ (rough) രണ്ടു പ്രതലങ്ങള്‍ പരസ്പരം ചേര്‍ന്നിരിക്കുന്ന രീതി ചിത്രം 4-ല്‍ കാണിച്ചിരിക്കുന്നു. പ്രതലങ്ങളിലെ തരിപ്പുകള്‍ അന്യോന്യം മുട്ടി നില്‍ക്കുന്നു. ഇപ്രകാരമുള്ള വിവിധ സ്പര്‍ശ ബിന്ദുക്കളിലെ ചെറിയ വിസ്തീര്‍ണങ്ങളുടെ ആകെത്തുകയാണ് യഥാര്‍ഥ സ്പര്‍ശതല വിസ്തീര്‍ണം. ഇതിന് ആനുപാതികമായി മൊത്തം ബലം പ്രതല(ചിത്രം 4)ങ്ങളിന്മേല്‍ അനുഭവപ്പെടുന്ന ബലത്തിനനുസരിച്ചും വ്യത്യാസപ്പെടും. തൊട്ടുനില്‍ക്കുന്ന തരിപ്പുകള്‍ പരസ്പരം വെല്‍ഡ് ചെയ്യപ്പെട്ട് സന്ധികളായി ഭവിക്കുന്നു. ഇവ പൊട്ടിച്ച് മുന്നോട്ട് നീങ്ങുന്നതിനാവശ്യമായ ബലമാണ് ഘര്‍ഷണബലം.
   
ഒരു പ്രതലത്തിന്മേലുള്ള തരിപ്പുകള്‍ മറ്റേ പ്രതലത്തില്‍ തുളച്ചുകയറി അതിലെ പദാര്‍ഥത്തിന് വിരൂപണം (deformation) സംഭവിക്കുന്നതില്‍ നിന്നാണ് ഘര്‍ഷണത്തിന്റെ ആവിര്‍ഭാവമെന്നാണ് മൂന്നാമത്തെ വിഭാഗത്തില്‍പ്പെട്ട സിദ്ധാന്തങ്ങളുടെ അടിസ്ഥാന സങ്കല്പം.
   
നാലാം വിഭാഗത്തില്‍പ്പെട്ട സിദ്ധാന്തങ്ങള്‍ സംയുക്ത വിഭാഗത്തില്‍പ്പെടുന്നു. പ്രതലപാരുഷ്യങ്ങളുടെ പരസ്പര സമ്പര്‍ക്കം കൊണ്ടും അവയുടെ അന്യോന്യമുള്ള ഉയര്‍ത്തല്‍കൊണ്ടുമാണ് ഘര്‍ഷണമുണ്ടാകുന്നതെന്ന കൂളംബിന്റെ സിദ്ധാന്തം ഈ വിഭാഗത്തില്‍പ്പെടുന്നു.
   
'''ഘര്‍ഷണ ഗുണാങ്കം''' (Coefficient of friction). ഒരു വസ്തുവിനു മുകളില്‍ മറ്റൊന്ന് ഉരസ്സിനീങ്ങുമ്പോള്‍ അനുഭവപ്പെടുന്ന ഘര്‍ഷണ പ്രതിരോധവും ലംബഭാരവും തമ്മിലുള്ള അനുപാത സംഖ്യയാണ് ഘര്‍ഷണ ഗുണാങ്കം.
   
സാധാരണ രീതിയില്‍ നടക്കുന്നതിന് ചെരുപ്പും തറയും തമ്മിലുള്ള ഘര്‍ഷണ ഗുണാങ്കം 0.2-ല്‍ കൂടുതലായിരിക്കണം. മെഴുകുപുരട്ടിയ തറയില്‍ നടക്കുക സാധ്യമല്ല; ഘര്‍ഷണ ഗുണാങ്കം 0.15-നോടടുത്ത് ആയിത്തീരുന്നതുകൊണ്ടാണിത്. മഞ്ഞുകട്ടകള്‍ക്കുമുകളില്‍ ഘര്‍ഷണ ഗുണാങ്കം 0.15-ല്‍ കുറവായിരിക്കുക പതിവാണ്. നല്ലയിനം ടയറുകളും റോഡ് പ്രതലവും തമ്മില്‍ 0.8-ന് അടുത്ത ഘര്‍ഷണ ഗുണാങ്കം ഉണ്ടായിരിക്കും. തന്മൂലം മോട്ടോര്‍ വാഹനങ്ങള്‍ ബ്രേക്കിട്ട് നിര്‍ത്തുക എളുപ്പമാണ്. നേരെ മറിച്ച് ട്രെയിനും പാളങ്ങളും തമ്മില്‍ താരതമ്യേന കുറഞ്ഞഘര്‍ഷണ ഗുണാങ്കമേ ഉള്ളൂ (ഏകദേശം 0.2). അതുകൊണ്ട് ഒരേ വേഗമുള്ള ഒരു കാറും ട്രെയിനും ഒരുമിച്ച് ബ്രേക്കിട്ടാല്‍ കാറിനെക്കാള്‍ നാലിരട്ടിയോളം ദൂരം കൂടുതല്‍ സഞ്ചരിച്ചശേഷമേ ട്രെയിന്‍ നില്‍ക്കുകയുള്ളൂ.
   
മിക്ക ലോഹങ്ങളും ബഹിരാകാശത്തെ ശൂന്യാവസ്ഥയില്‍ ഉയര്‍ന്ന ഘര്‍ഷണ ഗുണാങ്കം (ഏതാണ്ട് 5.0) ഉള്ളവയാണ്. അതിനാല്‍ ബഹിരാകാശവാഹനങ്ങളുടെ രൂപകല്പന വിഷമം പിടിച്ചതാണ്.
   
പ്രതലങ്ങളുടെ സ്നേഹനസ്ഥിതിക്കനുസരിച്ച് (lubrication) ഘര്‍ഷണ ഗുണാങ്കത്തിന്റെ മൂല്യത്തില്‍ ഏറ്റക്കുറച്ചിലുകള്‍ വരുന്നു. ചിത്രം 5-ല്‍ ഈ മാറ്റങ്ങള്‍ കാണിച്ചിരിക്കുന്നു.
   
സ്നേഹനം ഘര്‍ഷണ ഗുണാങ്കം കുറയ്ക്കുന്നു. ഉരസ്സല്‍ പ്രവേഗത്തില്‍ വരുന്ന വര്‍ധനവും ഘര്‍ഷണ ഗുണാങ്കത്തില്‍ വ്യതിയാനം വരുത്തുന്നു. ചിത്രം 6 നോക്കുക.
   
നിരപ്പായ ഒരു പ്രതലത്തില്‍ ഇരിക്കുന്ന ഒരു വസ്തുവിനെ സംബന്ധിച്ചു പറയുമ്പോള്‍ ഘര്‍ഷണകോണം (friction angle) എന്നത് വസ്തുവില്‍ നിരങ്ങല്‍ ഉണ്ടാക്കാതെ ആ പ്രതലത്തില്‍ വരുത്താവുന്ന പരമാവധി ചരിവുകോണമാണ്. ചിത്രം 7-ല്‍ കൊടുത്തിരിക്കുന്ന  എന്ന ഈ കോണത്തിന്റെ സ്പര്‍ശകം ആയിരിക്കും സ്ഥിതികഘര്‍ഷണഗുണാങ്കം. ഉരുളല്‍ ഘര്‍ഷണം ഇതില്‍ നിന്ന് വ്യത്യാസപ്പെട്ടിരിക്കും. നിരങ്ങല്‍ ഘര്‍ഷണം ഉരുളല്‍ ഘര്‍ഷണത്തെക്കാള്‍ വളരെ കൂടുതലാണ്.

(ഡോ. ആര്‍. രവീന്ദ്രന്‍നായര്‍)