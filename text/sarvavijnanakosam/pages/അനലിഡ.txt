= അനലിഡ =   
Annelida


സമമായ ദേഹഖണ്ഡങ്ങളുള്ളവയും, ദ്വിപാര്‍ശ്വസമമിതി (bilateral symmetry) ഉള്ളവയുമായ വിരകള്‍ ഉള്‍പ്പെടുന്ന ജന്തുവിഭാഗം  (phylum). മണ്ണിര, കുളയട്ട, കടല്‍വിര എന്നിവ ഇതില്‍ ഉള്‍പ്പെടുന്നു. അനലിഡ എന്ന പേരിന്റെ ഉദ്ഭവം ചെറിയ വളയം എന്നര്‍ഥമുള്ള അനുലസ് എന്ന ലാറ്റിന്‍ പദത്തില്‍നിന്നാണ്. നീണ്ട ദേഹമുള്ള ഇവയുടെ ശരീരം അനവധി ഖണ്ഡങ്ങളായി (segments) ഭാഗിക്കപ്പെട്ടിരിക്കുന്നു. ഓരോ ദേഹഖണ്ഡത്തെയും 'മെറ്റാമിയര്‍' (metamere) എന്നു വിളിക്കുന്നു. മെറ്റാമിയറുകള്‍ക്കോരോന്നിനും അന്തര്‍ഭാഗത്ത് രക്തക്കുഴലുകള്‍, നാഡീഗുഛിക(Nerve ganglion)കള്‍, ജനനഗ്രന്ഥികള്‍, പേശീവ്യൂഹഭാഗങ്ങള്‍ എന്നിവ കണ്ടുവരുന്നു. യഥാര്‍ഥത്തില്‍ അവശ്യഭാഗങ്ങളില്‍ മിക്കതും ഇപ്രകാരം ആവര്‍ത്തിക്കപ്പെട്ടുകാണുന്നു. ഇങ്ങനെയുള്ള ദേഹഖണ്ഡങ്ങള്‍ക്കുള്ളിലെ ആന്തരാവയവങ്ങളുടെ ആവര്‍ത്തനത്തെയാണ് മെറ്റാമെറിസം (metamerism) അതായത് അംശീകരണം എന്നു പറയുന്നത്. നോ: മെറ്റാമെറിസം


അടിസ്ഥാനപരമായി അനലിഡകളുടെ ഘടന പുറമേ കാണുന്ന നാളിപോലെയുള്ള ദേഹഭിത്തിയും ഉള്ളില്‍ ഒരു പചനനാളിയും ഇവയെ തമ്മില്‍ വേര്‍തിരിക്കുന്ന സീലോമും (coelom) ഉള്‍പ്പെട്ടതാണ്. ദഹനേന്ദ്രിയനാളത്തിന്റെ അഗ്രഭാഗത്തായി വായും പശ്ചഭാഗത്തായി ഗുദവും കാണപ്പെടുന്നു. വായുടെ മുന്നോട്ടു തള്ളിനില്ക്കുന്ന പ്രോസ്റ്റോമിയം (prostomium) എന്നൊരു ശരീരഭാഗമുണ്ട്. ഇതേത്തുടര്‍ന്നു പുറകോട്ടാണ് മറ്റു ശരീരഖണ്ഡങ്ങള്‍ കാണപ്പെടുന്നത്. ഓളിഗോക്കീറ്റയില്‍ (Oligochaeta) ദേഹഖണ്ഡങ്ങള്‍ ഏകരൂപമുള്ളവയാണ്. ഹിറുഡീനിയയില്‍ (Hirudinea) ഇവയ്ക്ക് ദ്വിതീയ ഖണ്ഡനം സംഭവിച്ചിരിക്കുന്നു. എന്നാല്‍ പോളിക്കീറ്റുകളില്‍ (Polychaeta) ഇവ പല അളവില്‍ വിഭേദനം (differentiation) സംഭവിച്ചവയാണ്.


അനലിഡകളുടെ ബാഹ്യഭാഗം കട്ടിയുളള ഒരു കോശപാളി(cuticle)യാല്‍ നിര്‍മിതമാണ്. ഇതില്‍ ശൂകങ്ങളോ (setae) പരിവര്‍ത്തിത സീലിയകളോ പ്രത്യേകതരം പാപ്പിലകളോ കാണാം. ഇവയെല്ലാംതന്നെ ആഹാരസമ്പാദനത്തിനോ ശ്വസനത്തിനോ പരിരക്ഷയ്ക്കോ ആയിട്ടാണ് ഉപയോഗിക്കപ്പെടുന്നത്. ബാഹ്യചര്‍മത്തില്‍ കാണുന്ന ശൂകങ്ങള്‍ എണ്ണത്തിലും ആകൃതിയിലും, വിവിധ ഇനങ്ങളില്‍ വ്യത്യസ്തങ്ങളായിരിക്കും.


'''പേശീഘടന.''' പേശീവ്യൂഹം പ്രധാനമായും മൂന്നുതരം പേശികളുള്‍ക്കൊള്ളുന്നു; വര്‍ത്തുളപേശികള്‍ (circular muscles), അനുദൈര്‍ഘ്യപേശികള്‍ (longitudinal muscles), തിര്യക് പേശികള്‍ (oblique muscles). ഇവയുടെ വലുപ്പവും കനവും വ്യത്യസ്തമായിരിക്കും. ഇവ രേഖിതപേശീകോശങ്ങളാല്‍ (striated muscle cells) നിര്‍മിതമാണ്. സംവേദകാംഗങ്ങള്‍ (sense organs) പ്രധാനമായും കണ്ണുകള്‍, സംവേദക പാപ്പിലകള്‍, ഗ്രസനി-സ്വാദേന്ദ്രിയങ്ങള്‍, സംതുലനപുടി (statocyst) എന്നിവയാണ്. മൃദുശരീരത്തിന്റെ സ്വരക്ഷയെക്കരുതി മിക്ക അനലിഡുകളിലും വഴുക്കല്‍-സ്രവണം (slime-secretion), നാളികള്‍, പുനങ്ങള്‍, തുരങ്കങ്ങള്‍ എന്നിവയുടെ നിര്‍മാണം എന്നീ പ്രക്രിയകളെ പ്രേരിപ്പിക്കത്തക്കവിധം ഗ്രന്ഥികോശ (glandular cell) വളര്‍ച്ച കൂടുതലായി കാണാറുണ്ട്.


'''നാഡീവ്യൂഹം.''' നാഡീവ്യൂഹത്തിന്റെ പ്രധാന ഭാഗം പൃഷ്ഠീയ-മസ്തിഷ്ക-ഗുച്ഛിക (dorsal cerebral ganglion) അഥവാ മസ്തിഷ്കമാണ്. ഇത് പ്രോസ്റ്റോമിയത്തിനുള്ളിലായോ അല്പം പുറകിലായോ കാണപ്പെടുന്നു. ഇതേത്തുടര്‍ന്നു ഒന്നോ രണ്ടോ പരിഗ്രസനി സംധായികള്‍ (circum oesophagial commissures) കാണാം. ഇവ ഒരു അധര-തന്ത്രികാരജ്ജു(ventral nerve cord)വുമായി ബന്ധിച്ചിരിക്കുന്നു. അധര-തന്ത്രികാരജ്ജുവിന് ഓരോ ഖണ്ഡത്തിലും ഗുച്ഛിക(ganglion)കളുണ്ട്.
[[Image:p.no.451a.jpg|thumb|150x200px|left|നീറിസിന്റെ നാഡീവ്യൂഹം-
മുന്‍ഭാഗം 1.മസ്തിഷ്കം 2.പരിഗ്രസനിസംധായി 3.അധര തന്ത്രികാരജ്ജു]]
'''ചംക്രമണ വ്യൂഹം.''' ചംക്രമണ വ്യൂഹത്തില്‍ രണ്ടു അധരപൃഷ്ഠ-അനുദൈര്‍ഘ്യ വാഹികള്‍ (dorsal and ventral longitudinal vessels) ഉണ്ട്. ഇവയ്ക്ക് പാര്‍ശ്വശാഖകളുമുണ്ട്. അധരവാഹികയില്‍ രക്തം മുന്‍പോട്ടും പൃഷ്ഠവാഹികയില്‍ പിന്‍പോട്ടും ഒഴുകുന്നു. ചില അനലിഡുകളില്‍ സീലോമിക ദ്രാവകവും രക്തവും തമ്മില്‍ വേര്‍പെട്ടു കാണപ്പെടുന്നു. മറ്റു ചിലവയില്‍ രക്തവാഹികള്‍ കാണുന്നില്ല. ഇവയില്‍ സീലോമില്‍ ആണ് രക്തം കാണപ്പെടുന്നത്. സ്പന്ദിക്കുന്ന വാഹികകളോ ഹൃദയമോ ഒന്നോ അതിലധികമോ ഖണ്ഡങ്ങളില്‍ ഉണ്ടാകാറുണ്ട്.
[[Image:p.no.451b.png|thumb|150x200px|left|നീറിസിന്റെ അനുപ്രസ്ഥ
ഛേദം :1.ക്യൂട്ടിക്കിള്‍ 2.ശുകം 3.അധിചര്‍മ്മം 4.വര്‍ത്തുളപേശി 5.അനുദൈര്‍ഘ്യ പേശി
6.തിര്യക്പേശി 7.സീലോമിക കോടാരം 8.നെഫ്റീഡിയം 9.നെഫ്റീഡിയത്തിന്റെ രന്ധ്രം
10.പൃഷ്ഠ രക്തവാഹിനി 11.അധര രക്തവാഹിനി 12.അധര തന്ത്രികാരജ്ജു
13.പൃഷ്‍ഠ-സിറസ് 14.അധര തന്ത്രികാരജ്ജു 13.പൃഷ്ഠ-സിറസ് 14.അധര - സിറസ്
15.ആന്ത്രം 16.ആന്ത്രയോജനി]]
1'''വിസര്‍ജന വ്യവസ്ഥ'''. വിവിധ പരിണാമഘട്ടങ്ങളിലുള്ള വിസര്‍ജനേന്ദ്രിയങ്ങളാണ് അനലിഡുകള്‍ക്കുള്ളത്. സാധാരണയായി ജോഡിയായി കാണപ്പെടുന്ന നെഫ്രീഡിയം (Nephridium) എന്ന അവയവമാണുള്ളത്. ഇവ ഖണ്ഡങ്ങള്‍തോറും കാണപ്പെടുന്നു. സീലിയാമയ കോശങ്ങളോടുകൂടിയ ഇവയുടെ നാളികള്‍ സീലോമില്‍നിന്നും വെളിയിലേക്ക് തുറക്കുന്നു. ചില അനലിഡുകളില്‍ ലിംഗകോശങ്ങളുടെ ബഹിര്‍ഗമനവും പ്രത്യേകതരം നെഫ്രീഡിയകള്‍വഴിയാണ്.

'''പ്രത്യുത്പാദന വ്യൂഹം.''' മിക്ക അനലിഡുകളിലും ലിംഗഭേദം കണ്ടുവരുന്നു. ഉള്‍ഭിത്തികളില്‍നിന്നാണ് ലിംഗകോശങ്ങള്‍ വളര്‍ന്നുവരുന്നത്. അവ സീലോമിനുള്ളില്‍വച്ച് പൂര്‍ണ വളര്‍ച്ച പ്രാപിക്കുന്നു. ഇവയ്ക്ക് ബാഹ്യബീജസങ്കലനമാണുള്ളത്. ഏറിയകൂറും പോളിക്കീറ്റുകളും എല്ലാ ക്ളൈറ്റലേറ്റുകളും ഉഭയലിംഗികള്‍ (bisexual) ആണ്. മുകുളനം (budding), വിഖണ്ഡനം (fragmentation) തുടങ്ങിയ അലൈംഗിക പ്രത്യുത്പാദനരീതികളും കണ്ടുവരുന്നുണ്ട്.
[[Image:p.no.452a.jpg|thumb|200x200px|left|A-നീറിസ് B-തല; ഗ്രസനി വെളിയിലേക്ക് തള്ളിയ രൂപത്തില്‍ C-തല;ഗ്രസനി പിന്‍വലിക്കപ്പെട്ട രൂപത്തില്‍ 
1.ഗ്രാഹികള്‍ 2.നേത്രങ്ങള്‍ 3.ദന്ദങ്ങള്‍ 4.ഹനു 5.വായ് 6.പാല്‍പുകള്‍ 7.പ്രോസ്റ്റോമിയം 
8.പെരിസ്റ്റോമിയം 9.പാല്‍പുകള്‍ 10.പാരപ്പോഡിയ]]
'''പുനരുദ്ഭവം''' (Regeneration). നഷ്ടപ്പെട്ട ശരീരഭാഗങ്ങളെ വീണ്ടും വളര്‍ത്തിയെടുക്കാനുള്ള കഴിവ് അനലിഡുകള്‍ പ്രദര്‍ശിപ്പിക്കുന്നു. നോ: പുനരുദ്ഭവം

ഞാഞ്ഞൂലുകള്‍, കണ്ണട്ടകള്‍ എന്നിവയില്‍ നേരിട്ടുള്ള പരിവര്‍ധനം (development) ആണ് നടക്കുന്നത്. നീറിസ്സുകള്‍, ആര്‍ക്കി അനലിഡുകള്‍ (Archianneldia) എന്നിവയില്‍ സ്വതന്ത്രമായി നീന്തിനടക്കുന്ന ട്രോക്കോഫോര്‍ (Trochophore) ലാര്‍വയെ കണ്ടുവരുന്നു.


അനലിഡുകള്‍ പല വലുപ്പത്തില്‍ കാണപ്പെടുന്നു. മണ്ണിരയുടെ ഇനത്തിലെ കീറ്റോഗാസ്റ്ററിന് ഒരു മി.മീ. നീളംവരും. റൈനോഡ്രൈലസ്, മെഗാസ്കോലക്സ് ഓസ്ട്രാലിസ് എന്നിവ 175 സെ.മീ. നീളവും രണ്ട് സെ.മീ. വണ്ണവും വരെ വളരുന്നു. നീറിസ് വര്‍ഗത്തിലെ യൂണിസ് ജൈജാന്‍ഷായ്ക്ക് 300 സെ.മീ. വരെ നീളം വരും.


'''വര്‍ഗീകരണം.''' ഫൈലം അനലിഡയെ നാലു വര്‍ഗങ്ങളായി തിരിച്ചിരിക്കുന്നു. പാരപ്പോഡിയം (parapodium), ശൂകങ്ങള്‍ (setae), മെറ്റാമിയറുകള്‍ എന്നിവയേയും മറ്റു ബാഹ്യസവിശേഷതകളെയും ആധാരമാക്കിയാണ് ഈ വര്‍ഗീകരണം നടത്തിയിരിക്കുന്നത്.
[[Image:p.no.452b.jpg|thumb|200x200px|left|കീറ്റോപ്‍ടെറസ്]]
'''വര്‍ഗം 1. ഓളിഗോക്കീറ്റ''' (Oligochaeta). ശരീരത്തിന്റെ ബാഹ്യാന്തര്‍ഭാഗങ്ങള്‍ ഖണ്ഡരൂപത്തിലുള്ളതാണ്. പാരപ്പോഡിയ കാണാറില്ല. ശൂകങ്ങളുടെ എണ്ണം കുറവായിരിക്കും. തല ചെറുതാണ്. ഇവ ഉഭയലിംഗികളാണ്. ലാര്‍വാ ദശ കാണാറില്ല. ഉദാ. ലംബ്രീക്കസ്, റ്റ്യൂബിഫെക്സ്.
'''വര്‍ഗം 2. പോളിക്കീറ്റ''' (Polychaeta). പാരപ്പോഡിയകളും ശൂകങ്ങളുമുള്ള അനലിഡുകള്‍. തലയും, തലയില്‍ കണ്ണുകള്‍, പാല്പുകള്‍ (palps), ഗ്രാഹികള്‍ (antennae) എന്നിവയും കാണപ്പെടുന്നു. ശരീരത്തിന്റെ ബാഹ്യാന്തര്‍ഭാഗങ്ങള്‍ ഖണ്ഡനം സംഭവിച്ചവയാണ്. ക്ളൈറ്റല്ലം (clitellum) കാണാറില്ല. ഏകലിംഗാശ്രയികളാണിവ. പരിവര്‍ധനഘട്ടത്തില്‍ ട്രോക്കോഫോര്‍ ലാര്‍വാ ദശ കാണപ്പെടുന്നു. ഉദാ. കീറ്റോപ്ടെറസ് (Chaetopterus), അരണിക്കോള (Arenicola), അഫ്രോഡൈറ്റ് (Aphrodite).


''' വര്‍ഗം 3. ഹിറുഡീനിയ''' (Hirudinea). അട്ടകള്‍ ഉള്‍പ്പെടുന്ന വര്‍ഗം. നോ: അട്ടകള്‍


'''വര്‍ഗം 4. ആര്‍ക്കി-അനലിഡ.''' ഇവയെ ആദി അനലിഡകളായി കരുതുന്നു. ഖണ്ഡനം ആന്തരികഭാഗത്തു മാത്രമേ കാണുന്നുള്ളു, പാരപ്പോഡിയങ്ങളും ശൂകങ്ങളും കാണുന്നില്ല. ഏകലിംഗാശ്രയികളാണ്. ഉദാ. പോളിഗോര്‍ഡിയസ് (Polygordius). ഓളിഗോക്കീറ്റയെയും പോളിക്കീറ്റയെയും ചേര്‍ത്ത് കീറ്റോപ്പോഡ (Chaetopoda) എന്നും പറയാറുണ്ട്.


'''ജാതിവൃത്തം'''. മൊളസ്കുകള്‍ (Mollusca), എക്കൈനോഡേമുകള്‍ (Echinoderm) എന്നിവയുടെ മുന്‍ഗാമികള്‍ അനലിഡുകളായിരുന്നുവെന്നു പരക്കെ സമ്മതിക്കപ്പെടുന്നുണ്ടെങ്കിലും, ഇവയുടെ പൂര്‍വികര്‍ ആരാണെന്ന് വ്യക്തമല്ല. ജീവാശ്മീയ (palaeontological) തെളിവുകള്‍ ഒട്ടുംതന്നെ ഇക്കാര്യത്തില്‍ ലഭ്യമല്ല.


അനലിഡുകളുടെ മുന്‍തലമുറയെപ്പറ്റി പ്രധാനമായും രണ്ടു സിദ്ധാന്തങ്ങളാണുള്ളത്: ട്രോക്കോഫോര്‍ സിദ്ധാന്തവും (Trocho phore theory) ടര്‍ബലേറിയന്‍ സിദ്ധാന്തവും (Turbellarian theory). ആദ്യത്തെ സിദ്ധാന്തം അനുസരിച്ച് അനലിഡുകള്‍ ട്രോക്കോഫോര്‍സമരായ പൂര്‍വികരില്‍ നിന്നും ഉദ്ഭവിച്ചതാണെന്ന് അനുമാനിക്കപ്പെടുന്നു. ഒരു പ്രത്യേക പൂര്‍വജീവിയുടെ അഭാവത്തില്‍ ട്രോക്കോസൂണ്‍ (Trochozoon) എന്ന ഭാവനാ ജീവിയെ ഇവയുടെ പൂര്‍വികനായി കരുതുന്നു. ഇതിന് പോളിഗോര്‍ഡിയസിന്റെ ലാര്‍വയുമായി നല്ല സാമ്യമുള്ളതായി പറയപ്പെടുന്നതിനാല്‍, ട്രോക്കോഫോര്‍ ലാര്‍വയുള്ള ജീവികളുടെയെല്ലാം മുന്‍ഗാമിയായി ഇതിനെ ഗണിക്കുന്നു. എന്നാല്‍ മറ്റു ചില ശാസ്ത്രജ്ഞര്‍ ട്രോക്കോസൂണ്‍, ടീനോഫോര്‍ (ctenophore) പോലെയുള്ള പൂര്‍വികരില്‍നിന്നും ഉദ്ഭവിച്ചതായി കരുതുന്നു.


ടര്‍ബലേറിയന്‍ സിദ്ധാന്തമാകട്ടെ, ഇവ ടര്‍ബലേറിയ പോലെയുള്ളവയില്‍നിന്നും ഉണ്ടായതായി കണക്കാക്കുന്നു. ഇവയിലെ സര്‍പ്പിള വിദളനം (spiral cleavage) ഇതിനൊരു തെളിവായി ചൂണ്ടിക്കാണിക്കപ്പെടുന്നു. കൂടാതെ ടര്‍ബലേറിയയുടെ ഭ്രൂണവളര്‍ച്ചയുടെ പല വിശദാംശങ്ങളും, ലാര്‍വകളും അനലിഡുകളുടേതുമായി സാമ്യം വഹിക്കുന്നു. മറ്റു പല ദേഹാവയവങ്ങളുടെ (നാഡീവ്യൂഹം, ആദിവൃക്ക, ലാര്‍വയുടെ ഘടന) രീതിയിലെല്ലാം തന്നെ ഐകരൂപ്യം കാണാം. ഈ തെളിവുകളെല്ലാം ടര്‍ബലേറിയന്‍ സിദ്ധാന്തത്തിന് പിന്‍ബലം നല്കുന്നു. 


(ഡോ. കെ.എം. അലക്സാണ്ടര്‍, സ.പ.)
[[Category:ജന്തുശാസ്ത്രം]]