==കേളിപ്പാത്രം==

അത്യുത്തരകേരളത്തിലെ ഹൈന്ദവഗൃഹങ്ങളിലെല്ലാം മണിയും കിലുക്കിക്കൊണ്ട് ഭിക്ഷയ്ക്കു വരുന്ന ഒരുവേഷം. 'ചോയി' (യോഗി) സമുദായക്കാരാണ് 'കേളിപ്പാത്ര'ത്തിന്റെ വേഷമണിയുന്നത്. തലയില്‍ ഓങ്കാരമുടി, സര്‍പ്പഫണം മുതലായ അലങ്കാരങ്ങള്‍ ധരിച്ചിരിക്കും. പട്ടാണ് ഉടുക്കുന്നത്. ശരീരം മുഴുവന്‍ ഭസ്മലേപനവും ചെയ്തിരിക്കും. ഒരു കൈയില്‍ മണിയും ചൂരല്‍ക്കോലും മറു കൈയില്‍ ഭിക്ഷാപാത്രവുമുണ്ടായിരിക്കും. ഏതെങ്കിലും ശിവക്ഷേത്ര സന്നിധിയില്‍ വച്ചാണ് വേഷം ധരിക്കുന്നത്. വേഷമണിഞ്ഞാല്‍  അഴിക്കുന്നതുവരെ മിണ്ടുകയില്ല. ഉച്ച തിരിയുന്നതുവരെ മാത്രമേ ഭിക്ഷയ്ക്കു സഞ്ചരിക്കുകയുള്ളൂ; പിന്നീട് വേഷമഴിക്കും.
  
ബ്രഹ്മാവിന്റെ തലയറുത്ത പാപം തീരുവാന്‍ ഭിക്ഷാടനത്തിനിറങ്ങിയ ശിവനെന്ന സങ്കല്പത്തിലാണ് 'കേളിപ്പാത്രം' ഗ്രാമങ്ങള്‍ തോറും സഞ്ചരിക്കുന്നത്. ഭിക്ഷാപാത്രം ബ്രഹ്മകപാലമാണത്രേ. യോഗിവേഷമണിഞ്ഞു സഞ്ചരിക്കുന്നതുകൊണ്ടാകാം ആ സമുദായത്തിന് യോഗി (ചോയി) എന്നു പേരുണ്ടായത്.

(ഡോ. എം.വി. വിഷ്ണുനമ്പൂതിരി)