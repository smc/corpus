== കാലവിചാരം ==

കേരള ബ്രാഹ്മണരുടെയിടയില്‍ നിലവിലിരുന്ന ഒരു വ്യഭിചാരക്കുറ്റ വിചാരണാസമ്പ്രദായം. അന്തര്‍ജനത്തിനോ കന്യകയ്‌ക്കോ അടുക്കളദോഷ (വ്യഭിചാരദോഷം)മുണ്ടെന്നു സംശയം തോന്നിയാല്‍ വിചാരണയ്‌ക്കുശേഷം ഭ്രഷ്‌ടുകല്‌പിച്ചു സമുദായത്തില്‍നിന്നു പുറന്തള്ളിയിരുന്ന ഈ പതിവിന്‌ "സ്‌മാര്‍ത്തവിചാരം' എന്നും പേരുണ്ട്‌. കാലവിചാരത്തിന്റെ ഘട്ടങ്ങള്‍ താഴെപ്പറയും പ്രകാരമാണ്‌.

ഒരു തറവാട്ടില്‍ വ്യഭിചാരദോഷമുണ്ടെന്നു സംശയമുണ്ടായാല്‍ ആ തറവാട്ടിലെ ഒരംഗം ആദ്യമായി അമ്പലത്തില്‍ച്ചെന്ന്‌ ഇണങ്ങരോടും (ബന്ധുക്കള്‍) ഓതിക്കോനോടും (മലയാള ബ്രാഹ്മണ പുരോഹിതര്‍) വിവരമറിയിക്കും. പിന്നീട്‌ അയാള്‍ നാലഞ്ചു ഇണങ്ങരോടുകൂടിച്ചെന്ന്‌ നാടുവാഴിയെയും വിവരം അറിയിക്കും. നാടുവാഴി നിയോഗിക്കുന്ന നാലു മീമാംസകന്മാരുടെയും സ്‌മാര്‍ത്തന്റെയും രാജപ്രതിനിധിയായ ഒരു ബ്രാഹ്മണന്റെയും (കോയിമ്മ) മേല്‍നോട്ടത്തിലാണ്‌ കാലവിചാരം നടക്കുക.

ആദ്യമായി മൂന്നുദിവസം വരെ നീണ്ടുനില്‌ക്കുന്ന വെള്ളാട്ടി (ദാസി) വിചാരം ആണ്‌. അടുക്കളദോഷമുണ്ടെന്നു ശങ്കിക്കപ്പെട്ടവളും ദാസിയും കോയിമ്മയും സ്‌മാര്‍ത്തനും മാത്രമേ വിചാരണസ്ഥലത്ത്‌ ഉണ്ടാകാവൂ. ദാസിയെ സ്വകാര്യമായി വിളിച്ചു ചോദ്യംചെയ്യാറുമുണ്ട്‌. വെള്ളാട്ടിവിചാരംകൊണ്ട്‌ ഫലമുണ്ടായാലും ഇല്ലെങ്കിലും നാലാംദിവസം അടുക്കളദോഷമുണ്ടെന്നു സംശയിക്കപ്പെട്ടവളെ അകത്താക്കി പട്ടിണികിടത്തും.

ഗൃഹസ്ഥന്റെ സംശയവും ദാസിവിചാരത്തിന്റെ ഫലവും ഒന്നാണെങ്കില്‍ അന്തര്‍ജനത്തെ അഞ്ചാംപുരയിലാക്കും. വിചാരണയ്‌ക്കുമുമ്പേ അഞ്ചാംപുരയില്‍  വസിക്കുന്ന അന്തര്‍ജനത്തെ "സാധനം' എന്നാണ്‌ വിളിക്കുന്നത്‌.

പിന്നീടാണ്‌ അമ്പലത്തില്‍വച്ച്‌ സ്‌മാര്‍ത്തവിചാരം നടത്തുന്നത്‌. "സാധനം' ഇരിക്കുന്ന സ്ഥലത്തിന്‌ തൊട്ടടുത്ത മുറിയില്‍ അവള്‍ കാണാത്തവിധം സ്‌മാര്‍ത്തനും മീമാംസകനും മറ്റും ഇരിക്കും. രാജപ്രതിനിധി തലയില്‍ ഒരു വസ്‌ത്രമിട്ടിരിക്കും. സ്‌മാര്‍ത്തന്‍ സാധനത്തെ വിചാരണ ചെയ്യുമ്പോള്‍ ചോദ്യം ശരിയാകുന്നില്ലെങ്കില്‍ രാജപ്രതിനിധി തലയിലുള്ള വസ്‌ത്രം താഴെയിടും. ചോദ്യം ശരിയല്ലെന്ന്‌ ഈ സൂചനകൊണ്ടു സ്‌മാര്‍ത്തന്‍ മനസ്സിലാക്കുന്നു. സംശയം തീരുന്നതുവരെയോ, ദോഷം സമ്മതിക്കുന്നതുവരെയോ വിചാരം തുടരും.

സ്‌മാര്‍ത്തന്‍ അന്തര്‍വിചാരവും ബഹിര്‍വിചാരവും നടത്തേണ്ടതുണ്ട്‌. സാധനത്തോടു നേരിട്ടുചോദ്യംചെയ്യലാണ്‌ "അന്തര്‍വിചാരം'; പുറമേയുള്ള ലോകവാര്‍ത്ത ഗ്രഹിക്കലാണ്‌ "ബഹിര്‍വിചാരം'. അതുരണ്ടും ഒത്തുവരണം. പല അവസ്ഥയും പല ദിവസം മാറിമാറി ചോദിക്കും. വാക്കുകളില്‍ വ്യത്യാസം വരുന്നുണ്ടോ എന്നു ശ്രദ്ധിക്കണം; പറയുമ്പോഴുള്ള ഭാവവും നോക്കണം. സത്യം തുറന്നുപറയുന്നില്ലെങ്കില്‍ ചതുരുപായവും പ്രയോഗിക്കാറുണ്ട്‌. ദോഷം സമ്മതിച്ചാല്‍ ആദ്യത്തെ ജാരന്‍ ആരെന്നുചോദിക്കും. ജാരന്മാരുടെ പേര്‌ "സാധന'ത്തെക്കൊണ്ടുതന്നെ പറയിക്കാറുണ്ട്‌. ഇതാണ്‌ "സ്വരൂപംചൊല്ലല്‍'. പിന്നീട്‌ സാധനത്തിന്റെ കുടയും വളയും വെപ്പിക്കുകയും അതു വെള്ളാട്ടിയെടുത്ത്‌ തറവാട്ടില്‍ എത്തിക്കുകയും ചെയ്യുന്നു.

ക്ഷേത്രത്തില്‍വച്ചു നടത്തുന്ന "കഴകവിചാര'ത്തിന്റെ അന്ത്യത്തില്‍ നാടുവാഴികൂടി പങ്കെടുക്കേണ്ടതുണ്ട്‌. എല്ലാവര്‍ക്കും അത്താഴമൂട്ടിയശേഷം വിചാരം തുടങ്ങുന്നു. സദസ്സില്‍വച്ച്‌ ഗണപതിസരസ്വതിസ്‌തുതികള്‍ക്കുശേഷം സ്‌മാര്‍ത്തന്‍ സ്‌മൃതിചൊല്ലും. രാജധര്‍മത്തെയും സ്‌ത്രീധര്‍മത്തെയും സ്‌മാര്‍ത്തന്‍ വിവരിക്കും. പിന്നീട്‌ "സാധനം' "സ്വരൂപം ചൊല്ലിയ'ത്‌ അപ്രകാരം തന്നെ വഴിപോക്കന്‍ ബ്രാഹ്മണനെക്കൊണ്ട്‌ വിളിച്ചുപറയിക്കണം.

ദോഷംതെളിഞ്ഞ "സാധന'ത്തെ കൈകൊട്ടി പുറത്താക്കി ആ രാജ്യത്തുതന്നെ എവിടെയെങ്കിലും വസിപ്പിക്കും. പിന്നീട്‌ പടിയടച്ചു ഉദകവിച്ഛേദം ചെയ്‌തു (കുടുംബബന്ധം വേര്‍പെടുത്തി) പിണ്ഡം വച്ച്‌, ജ്ഞാതികൃച്ഛ്‌റം, സ്‌മാര്‍ത്തകൃച്ഛ്‌റം, കോയിമ്മകൃച്ഛ്‌റം തുടങ്ങിയ പ്രായശ്ചിത്ത ദക്ഷിണകള്‍ ചെയ്യുന്നു. പിന്നീട്‌ അടുക്കളദോഷമുണ്ടായ തറവാട്ടില്‍വച്ച്‌ സ്വജനങ്ങളെല്ലാം ഒന്നിച്ചിരുന്ന്‌ ശുദ്ധഭോജനം കഴിക്കുന്നതോടെ ചടങ്ങുകള്‍ അവസാനിക്കുന്നു. സാമുദായികവും വൈദികവുമായ ഈ ആചാരം ഇന്നു നിലവിലില്ല. കാലവിചാരത്തില്‍പ്പെട്ട സ്‌ത്രീകളെയും ദോഷംപെട്ട കുട്ടികളെയും ചാക്യാന്മാരുടെയും മറ്റും ജാതിയില്‍ ചേര്‍ക്കുക പതിവുണ്ടായിരുന്നു. നോ: സ്‌മാര്‍ത്തവിചാരം

(ഡോ. എം.വി. വിഷ്‌ണുനമ്പൂതിരി)