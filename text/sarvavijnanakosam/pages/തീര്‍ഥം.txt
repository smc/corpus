തീര്‍ഥം  

മന്ത്രം കൊണ്ടോ ആരാധനാവിഗ്രഹങ്ങളില്‍ അഭിഷേകം ചെയ്തു കൊണ്ടോ മഹാത്മാക്കളുടെ പ്രാര്‍ഥനകൊണ്ടോ പവിത്രമാക്കപ്പെട്ട ജലം. ഹൈന്ദവ വിശ്വാസത്തിലും ആരാധനയിലും പ്രധാന സ്ഥാനമുണ്ട് തീര്‍ഥജലത്തിന്. ക്ഷേത്രങ്ങളില്‍ തൊഴുതു കഴിഞ്ഞാല്‍ ആരാധനാവിഗ്രഹങ്ങളില്‍ അഭിഷേകം ചെയ്ത ജലമായ തീര്‍ഥം പ്രസാദത്തോടൊപ്പം മേല്‍ശാന്തി ഭക്തര്‍ക്കു നല്കുന്നു. വിഗ്രഹത്തില്‍ ചാര്‍ത്തിയ ചന്ദനവും പൂവുമാണ് പ്രസാദം. ഹൈന്ദവ ആചാരപ്രകാരം തീര്‍ഥം വലതുകൈ കൊണ്ടു വാങ്ങുകയും മൂന്നുരു നാരായണനാമം ജപിച്ച് സേവിക്കുകയും വേണം. അതോടൊപ്പം അല്പം മുഖത്തും തലയിലും തളിക്കുകയാണ് സാധാരണയായി ഭക്തര്‍ ചെയ്യുന്നത്.

  തീര്‍ഥത്തിന് വളരെയേറെ വ്യാഖ്യാനങ്ങളുണ്ട്. 'നിപാനാഗമ യോസ്തീര്‍ഥമൃഷിജുഷ്ടജലേ ഗുരൌ' എന്ന് അമരകോശത്തില്‍ പറയുന്നു. കിണറ്റിനരികിലുള്ള നീര്‍ത്തൊട്ടി, ശാസ്ത്രം, ഋഷികളാല്‍ സേവിക്കപ്പെട്ട ഗംഗയുടേയും മറ്റും തീര്‍ഥജലം എന്നിവയേയും ഗുരുവിനേയും തീര്‍ഥം എന്നു പറയുന്നു.

  പാപങ്ങളെ തരണം ചെയ്യിക്കുന്നത് തീര്‍ഥം എന്ന് വ്യാഖ്യാ നിക്കപ്പെടുന്നു. 'ജനായൈഃതരന്തി ദുഃഖാനി തീര്‍ഥാനി' - ജനങ്ങള്‍ ഏതൊന്നുകൊണ്ട് ദുഃഖങ്ങളെ കടന്നുപോകുന്നുവോ അത് തീര്‍ഥം എന്നര്‍ഥം. മനസ്സിന്റെ തികഞ്ഞ വിശുദ്ധി തീര്‍ഥങ്ങള്‍ക്കും തീര്‍ഥമാണെന്ന് മഹാഭാരതത്തില്‍ പറയുന്നു. എല്ലാ കളങ്കത്തേയും കഴുകിക്കളയുന്നതാണ് തീര്‍ഥം എന്നാണ് സങ്കല്പം. തീര്‍ഥജലമായാലും പുണ്യസ്ഥാനത്തെ തീര്‍ഥസ്നാനമായാലും ഒരുപോലെ പുണ്യമായി കരുതപ്പെടുന്നു. വിഗ്രഹങ്ങളില്‍ അഭിഷേകം ചെയ്യുന്ന പാല്‍, നെയ്യ്, പുഷ്പം, ജലം എന്നിവ തീര്‍ഥമായി കണക്കാക്കപ്പെടുന്നു. തീര്‍ഥത്തിന് പുണ്യസ്ഥാനം എന്നും അര്‍ഥമുണ്ട്.

(അജിത്കുമാര്‍ എ.എസ്.)