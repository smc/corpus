=നിര്‍മിതികേന്ദ്രം=

ചെലവുകുറഞ്ഞതും പരിസ്ഥിതിക്കിണങ്ങുന്നതുമായ കെട്ടിടനിര്‍മാണ സാങ്കേതികവിദ്യകള്‍ പകര്‍ന്നുകൊടുക്കുന്നതിനും നടപ്പിലാക്കുന്നതിനുമായുള്ള കേരള സര്‍ക്കാര്‍ സ്ഥാപനം. 

പ്രാദേശികമായി ലഭിക്കുന്ന വസ്തുക്കളെ ഉപയോഗപ്പെടുത്തല്‍, അനുയോജ്യമായ സാങ്കേതികവിദ്യയുടെ ഉപയോഗം, നിര്‍മാണപ്രവര്‍ത്തനങ്ങളിലെ ഗുണഭോക്തൃപങ്കാളിത്തം, ആധുനികവും പരമ്പരാഗതവുമായ നിര്‍മാണരീതികളുടെ സംയോജനം എന്നിവയെ അടിസ്ഥാനപ്പെടുത്തിയാണ് നിര്‍മിതിയുടെ പ്രവര്‍ത്തനങ്ങള്‍. ഇത്തരം പ്രവര്‍ത്തനരീതിക്ക് മതിയായ പ്രചാരണം കൊടുക്കുകവഴി ഒരു പുതിയ നിര്‍മാണസംസ്കാരം തന്നെ സൃഷ്ടിക്കാനായി.

1985-ല്‍ കേരളത്തെ നടുക്കിയ വെള്ളപ്പൊക്കക്കെടുതികളില്‍പ്പെട്ടവര്‍ക്ക് ആശ്വാസം നല്കുന്നതിലുണ്ടായ പോരായ്മകള്‍ അന്നത്തെ കൊല്ലം ജില്ലാ കളക്ടറായിരുന്ന ശ്രീ. സി.വി. ആനന്ദബോസിന് നേരിട്ട് ബോധ്യപ്പെടുകയും, കെടുതികളില്‍പ്പെട്ടവര്‍ക്ക് ഭൂമിയും ധനസഹായവും നല്കുന്നതിനൊപ്പം പാര്‍പ്പിടവും നിര്‍മിച്ചു നല്കണമെന്ന ആശയം മുന്നോട്ടുവയ്ക്കുകയും ചെയ്തു. ഈ ആശയത്തിന്റെ സാക്ഷാത്കാരത്തിനായാണ് നിര്‍മിതികേന്ദ്രത്തിന് രൂപംകൊടുത്തത്.

സര്‍ക്കാരിന്റെ വികസനപ്രവര്‍ത്തനങ്ങളുടെ ഏകോപനം, സന്നദ്ധസംഘടനകളുടെയും സാങ്കേതികവിദഗ്ധരുടെയും കൂട്ടായ്മ, പദ്ധതികളില്‍ ഗുണഭോക്താക്കളുടെ നേരിട്ടുള്ള പങ്കാളിത്തം തുടങ്ങിയവ ഈ സമീപനത്തിന് പ്രോത്സാഹനമേകി. ചെലവുകുറഞ്ഞ സാങ്കേതികവിദ്യകള്‍ ഉപയോഗപ്പെടുത്തുന്നതിനുവേണ്ടി ഗവേഷണ, വികസന സ്ഥാപനങ്ങളുടെ ഉപദേശം തേടി. പ്രാദേശികമായി ലഭിക്കുന്ന നിര്‍മാണ വസ്തുക്കളുടെ ഗുണനിലവാരം, പ്രത്യേകതകള്‍ എന്നിവ കണ്ടെത്തുന്നതിനായി ബി.ഐ.എസ്. (ബ്യൂറോ ഒഫ് ഇന്ത്യന്‍ സ്റ്റാന്റേര്‍ഡ്സ്)മായി ഒത്തുചേര്‍ന്നു നിര്‍മിതികേന്ദ്രം പ്രവര്‍ത്തിച്ചു. കൊല്ലം ജില്ലയിലെ പദ്ധതി വിജയിച്ചതോടെ കെട്ടിടനിര്‍മാണ പ്രവര്‍ത്തനങ്ങള്‍ ഏകോപിപ്പിക്കാനും ആശയങ്ങള്‍ പ്രാവര്‍ത്തികമാക്കാനും, പ്രചരിപ്പിക്കുന്നതിനുമായി ജില്ലകള്‍തോറും നിര്‍മാണകേന്ദ്രങ്ങള്‍ സ്ഥാപിച്ചു. 

[[Image:nirmithi 2.png]]

നിര്‍മാണ പ്രവര്‍ത്തനങ്ങള്‍ക്കായി കഴിവുറ്റ തൊഴിലാളികളെ വാര്‍ത്തെടുക്കുന്നതിനുവേണ്ടി നിര്‍മിതികേന്ദ്രം പരിശീലന പരിപാടികള്‍ സംഘടിപ്പിക്കുന്നുണ്ട്. കല്പണി, ആശാരിപ്പണി, പ്ലംബിങ്, വൈദ്യുതീകരണം, ഇന്റീരിയര്‍ ഡിസൈന്‍, ലാന്‍ഡ് സ്കേപ്പിങ് തുടങ്ങിയ മേഖലകളില്‍ സംഘടിപ്പിച്ച പരിശീലന പരിപാടികളില്‍ സ്ത്രീകള്‍ക്ക് മുന്‍ഗണന നല്കിവരുന്നു. ഇത് നിര്‍മാണ മേഖലയില്‍ നിലനിന്നിരുന്ന ലിംഗപരമായ അസന്തുലിതാവസ്ഥ കുറയ്ക്കാനും സ്ത്രീപങ്കാളിത്തം ഉയര്‍ത്താനും സഹായിച്ചു. 

സാങ്കേതികവിദ്യയെ പരീക്ഷണശാലയില്‍നിന്നും പുറത്തേക്ക് കൊണ്ടുവരാന്‍ (Lab to field) ഈ പ്രസ്ഥാനം സഹായകമായി. നിര്‍മാണച്ചെലവ് 30 ശതമാനത്തോളം കുറച്ചുകൊണ്ട് ഭവനനിര്‍മാണരംഗത്ത് ഒരു കുതിച്ചുചാട്ടംത്തന്നെ നടത്തി നിര്‍മിതികേന്ദ്രം ഇന്ത്യയിലെ മറ്റ് സംസ്ഥാനങ്ങള്‍ക്ക് മാതൃകയായി. നിര്‍മിതിയുടെ സമീപനങ്ങള്‍ 'കേരള മോഡല്‍' എന്ന പേരില്‍ 1988-ല്‍ ദേശീയ ഭവനനയത്തില്‍ ഉള്‍പ്പെടുത്തുകയുണ്ടായി. ജില്ലാതല കേന്ദ്രങ്ങളുടെ ഏകോപനത്തിനായി തലസ്ഥാനത്ത് സംസ്ഥാന ഭവനനിര്‍മാണവകുപ്പുമന്ത്രി ചെയര്‍മാനായി കേരള സംസ്ഥാന നിര്‍മിതികേന്ദ്രം രൂപവത്കരിക്കപ്പെട്ടു.

[[Image:nirmithi 1.png]]

മഹാരാഷ്ട്രയിലെ ലാത്തൂരില്‍ ഭൂകമ്പാനന്തരം നടന്ന ഭവനനിര്‍മാണത്തില്‍ നിര്‍മിതികേന്ദ്രം രൂപകല്പന ചെയ്ത ഭൂകമ്പ പ്രതിരോധ മോഡലുകളാണ് ഉപയോഗിച്ചത്. ഗുജറാത്തിലെ കച്ച് ഭൂകമ്പം, 2004-ലെ സുനാമി ദുരന്തം തുടങ്ങി ദേശീയ ദുരന്തങ്ങളുടെ പുനരധിവാസപ്രക്രിയകളില്‍ നിര്‍മിതികേന്ദ്രങ്ങള്‍ സജീവ പങ്കുവഹിച്ചിട്ടുണ്ട്.

ജില്ലാകേന്ദ്രങ്ങളുടെ ഏകോപനം, സംസ്ഥാന, കേന്ദ്ര സര്‍ക്കാരുകള്‍ രൂപം നല്കുന്ന വിവിധ പദ്ധതികളുടെ നടത്തിപ്പ്, നൂതന സാങ്കേതികരീതികള്‍ വികസിപ്പിച്ചെടുക്കല്‍ എന്നിവയോടൊപ്പം സര്‍വകലാശാലകള്‍, അന്താരാഷ്ട്ര സെമിനാര്‍ കോംപ്ളക്സുകള്‍ എന്നിവ പോലുള്ള ബൃഹത് പദ്ധതികളുടെ നടത്തിപ്പിലും ആവിഷ്കാരത്തിലും സംസ്ഥാന നിര്‍മിതികേന്ദ്രം പങ്കാളിയാകുന്നുണ്ട്. ഫലപ്രദമായരീതിയില്‍ ചെലവുകുറഞ്ഞതും പരിസ്ഥിതിക്കിണങ്ങുന്നതുമായ സാങ്കേതികവിദ്യയുടെ (സേഫ് ടെക്നോളജിയുടെ) ഗവേഷണകേന്ദ്രം സംസ്ഥാന നിര്‍മിതികേന്ദ്രത്തില്‍ പ്രവര്‍ത്തിച്ചുവരുന്നു. ഇപ്പോള്‍ പ്രസ്തുത സ്ഥാപനം അന്താരാഷ്ട്ര നിലവാരമുള്ള ഒരു ഗവേഷണ സ്ഥാപനമായ (ലാറിബേക്കര്‍ ഇന്റര്‍നാഷണല്‍ സ്കൂള്‍ ഒഫ് ഹാബിറ്റാറ്റ് സ്റ്റഡീസ്) അംഗീകരിക്കപ്പെട്ടിട്ടുണ്ട്. ക്രമേണ, ദേശീയതലത്തില്‍ അറിയപ്പെട്ടതോടെ മറ്റു സംസ്ഥാനങ്ങളിലും കേന്ദ്ര സഹായത്തോടെ നിര്‍മിതി കേന്ദ്രങ്ങള്‍ പ്രവര്‍ത്തിച്ചു തുടങ്ങിയിട്ടുണ്ട്.