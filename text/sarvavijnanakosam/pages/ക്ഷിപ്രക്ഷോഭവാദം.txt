==ക്ഷിപ്രക്ഷോഭവാദം==

==Catastrophism==

ആകസ്മികവും അല്പായുസ്സും എന്നാല്‍ കുറെയൊക്കെ ആവര്‍ത്തനവും തീവ്രവുമായ പ്രധാന ഭൂമണ്ഡലസംഭവങ്ങളെയും ഭൂവല്ക്കത്തിലെ എല്ലാ പ്രധാന സവിശേഷതകളെയും ബന്ധപ്പെടുത്തുന്ന ഒരു ജിയോളജീയ സിദ്ധാന്തം.
   
പ്രാചീന-മധ്യകാലഘട്ടങ്ങളില്‍ ക്ഷിപ്രക്ഷോഭവാദസിദ്ധാന്തം ജിയോളജിക സങ്കല്പങ്ങളില്‍ ആധിപത്യം പുലര്‍ത്തിയിരുന്നു. മനുഷ്യന്റെ ഗ്രഹണപടുതയ്ക്കതീതമായ ചില ശക്തികളുടെ പ്രവര്‍ത്തനഫലമായുണ്ടാകുന്നതാണ് ക്ഷിപ്രക്ഷോഭവാദ സംഭവങ്ങളെന്നു വിശ്വസിച്ചിരുന്നതിനാല്‍ ഇവ പ്രകൃത്യതീതം, ഗൂഢം എന്നൊക്കെയാണ് വിശേഷിപ്പിക്കപ്പെട്ടുപോന്നത്. ഭൂഗര്‍ഭത്തില്‍ പെട്ടെന്നുണ്ടാകുന്ന പ്രതികൂല മാറ്റങ്ങളും നവമായ സൃഷ്ടികളും മൂലമാണ് ഭൂപ്രകൃതിയില്‍ വ്യതിയാനങ്ങളുണ്ടാകുന്നതെന്നാണ് ഈ സിദ്ധാന്തപ്രകാരമുള്ള വിശ്വാസം. പ്രശസ്ത ഫ്രഞ്ച് പ്രകൃതിശാസ്ത്രജ്ഞനായ ഷോര്‍ഷസ് ക്യൂവീര്‍ (1769-1832) ആയിരുന്നു ഈ സിദ്ധാന്തത്തിന്റെ പ്രധാനപ്രയോക്താവ്. 'പര്‍വത രൂപീകരണം, സമുദ്രങ്ങളുടെ കടന്നുകയറലും പിന്‍വാങ്ങലും, ജീവജാലങ്ങളുടെ പരിണാമ-അസ്തമനങ്ങള്‍ തുടങ്ങി താളനിബദ്ധമായ പ്രകൃതിസ്പന്ദങ്ങളുടെ പരമ്പരയാണ് ഭൂതത്ത്വശാസ്ത്ര ചരിത്രം' എന്നാണ് ക്യൂവറിന്റെ വീക്ഷണത്തെ 20-ാം ശതകത്തില്‍ വിശകലനം ചെയ്തിരിക്കുന്നത്. പുരാജീവിവിജ്ഞാനത്തില്‍ (Palaeontology), അനുക്രമമായ സ്തരതലങ്ങളില്‍ കാണപ്പെടുന്ന ഫോസിലുകള്‍ തമ്മിലുള്ള വ്യത്യാസം ആവര്‍ത്തിച്ചുണ്ടാകുന്ന ജല-ഹിമപ്രളയങ്ങളുടെയും നവീന സൃഷ്ടികളുടെയും ഉത്പന്നമാണെന്ന് ഈ സിദ്ധാന്തം സമര്‍ഥിക്കുന്നു. ഒരു പ്രത്യേക മേഖലയില്‍ കാണപ്പെടുന്ന ശിലകളുടെ പാര്‍ശ്വവും ലംബവുമായ പരമ്പരയ്ക്കു പുറമേ വലനങ്ങള്‍ (folds), ഭ്രംശങ്ങള്‍ (faults) എന്നിവയും ആകസ്മിക-പ്രകൃതിക്ഷോഭങ്ങളുമായി ബന്ധപ്പെട്ടിരിക്കുന്നു.
   
ക്ഷിപ്രക്ഷോഭവാദത്തിനുള്ള ഒരു ഏകാന്തര കാഴ്ചപ്പാട് ഏകരൂപതാവാദസിദ്ധാന്തം (Uniformitarianism) ആയിരുന്നു. 1850-ഓടെ ഏകരൂപതാവാദം ഒരു ഭൂതത്ത്വശാസ്ത്രസിദ്ധാന്തമെന്ന നിലയില്‍നിന്ന് ക്ഷിപ്രക്ഷോഭവാദത്തെ പൂര്‍ണമായി പിന്തള്ളിക്കഴിഞ്ഞിരുന്നു. ഈ വ്യതിയാനം ജിയോളജിയെ ഒരു ആധുനികശാസ്ത്രമെന്ന നിലയില്‍ വികസിക്കുന്നതിനു സഹായിക്കുകയും ചെയ്തു.

(ഡോ. എം. സന്തോഷ്)