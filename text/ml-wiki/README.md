This is text content extracted from Malayalam wikipedia dump dated 2023-04-02 using [WikiExtractor][1].
It is then cleaned up for common spelling mistakes.

[1]: https://github.com/attardi/wikiextractor

```bash
wget https://dumps.wikimedia.org/mlwiki/latest/mlwiki-latest-pages-articles.xml.bz2
python -m wikiextractor.WikiExtractor  mlwiki-latest-pages-articles.xml.bz2
```

