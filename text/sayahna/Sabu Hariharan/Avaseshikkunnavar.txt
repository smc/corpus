‘കുറച്ചു് നേരമായി ഞാൻ ശ്രദ്ധിക്കുന്നു, നിനക്കു് കഴിഞ്ഞ തവണ കണ്ടപ്പോഴുള്ള…ആ ഒരു ഉന്മേഷമില്ലല്ലോ…’
രാമകൃഷ്ണൻ പറഞ്ഞതു് ഞാൻ ശരി വെച്ചു. പതിവില്ലാതെ ഞാൻ ഒരു കാര്യം—ഒരൊറ്റ കാര്യത്തെക്കുറിച്ചു് മാത്രം തുടർച്ചയായി ചിന്തിച്ചു കൊണ്ടിരിക്കുന്നു. ഒരു വലിയ വൃത്തത്തിലൂടെ നിർത്താതെ ഓടുന്ന പോലെയാണതു്. കുറച്ചു് കഴിയുമ്പോൾ, എവിടെ നിന്നാണു് ഓട്ടമാരംഭിച്ചതെന്നു് പോലും തിരിച്ചറിയാനാകാതെ പോകുന്ന അവസ്ഥ! ഇതാദ്യമായിട്ടൊന്നുമല്ല, ഇത്തരമൊരു അവസ്ഥയിലൂടെ ഞാൻ കടന്നു് പോകുന്നതു്. ഇടയ്ക്കിടെ ഇതു് സംഭവിക്കും. അപ്പോഴൊക്കെ ഞാൻ മൗനി ബാബ ആയിപ്പോവും! എന്തു് ചെയ്തു കൊണ്ടിരിക്കുന്നു, എവിടേക്കു് പോകുന്നു എന്നിങ്ങനെയുള്ള കാര്യങ്ങളൊക്കെ പാടെ വിസ്മരിച്ചു പോകും. അപ്പോൾ തോന്നും, ചുറ്റുമുള്ള ലോകം അവസാനിക്കാൻ പോവുകയാണെന്നു്. ചിലപ്പോൾ ഞാൻ തന്നെ അവസാനിക്കാൻ പോവുകയാണെന്നും.
രാമകൃഷ്ണനും ഞാനും വർഷങ്ങൾക്കു് മുൻപു് ഒരേ ഇഞ്ചിനീയറിംഗ് കോളേജിൽ ഒന്നിച്ചു് പഠിച്ചതാണു്. അന്നു് പണിത സൗഹൃദമതിൽ ഇന്നും, ഇതാ ഈ നിമിഷം വരേയ്ക്കും വിള്ളലേതുമില്ലാതെ നിലനിൽക്കുന്നു. കാലചക്രം രണ്ടിടങ്ങളിലേക്കു് ചുഴറ്റി എറിഞ്ഞപ്പോഴും വർഷത്തിലൊരിക്കൽ അല്ലെങ്കിൽ രണ്ടു് വർഷം കൂടുമ്പോൾ പരസ്പരം ചെന്നു് കാണുക എന്ന കാര്യം വിട്ടുവീഴ്ച്ചയില്ലാതെ ഞങ്ങൾ പാലിച്ചു കൊണ്ടിരിക്കുന്നു. ഈ സന്ദർശനങ്ങൾ ഒരു ‘ഊർജ്ജസംഭരണപ്രക്രിയ’യുടെ ഭാഗമാണു്. അങ്ങോട്ടുമിങ്ങോട്ടും ഉർജ്ജം പകരാനുള്ള അവസരം! ഇപ്പോൾ ഞാനവന്റെ കോഴിക്കോടുള്ള വീട്ടിലാണു്. കോഴിക്കോടു് ജോലി ഉറപ്പിച്ചപ്പോൾ അവൻ അവിടെ നിന്നു് തന്നെ തനിക്കു് പറ്റിയ ഒരു ഇണയേയും കണ്ടെത്തി. കുടി ഉറപ്പിച്ചു. ഇപ്പോൾ സ്വസ്ഥം റിട്ടയർമെന്റ് ജീവിതം. കോഴിക്കോടു് ചെല്ലുമ്പോൾ മിഠായിത്തെരുവു്, മാനാഞ്ചിറ മൈതാനം, ഗോതീശ്വരം കടപ്പുറം… അങ്ങനെ പലയിടത്തും ഞങ്ങൾ നടക്കാൻ പോകും. സമയത്തേക്കുറിച്ചുള്ള വേവലാതികളൊന്നുമില്ലാതെ കാഴ്ച്ചകൾ കണ്ടു് നടക്കും. പഴയ കാര്യങ്ങൾ അയവിറക്കുകയും, പുതിയ കാര്യങ്ങളെ മുൻവിധികളില്ലാതെ വിചാരണ ചെയ്യുകയും ചെയ്യും. അവൻ അവന്റെ ചുവന്ന ആൾട്ടോ കാറിൽ എന്നെ കോഫി ഹൗസിലും, തളി ക്ഷേത്രത്തിലും കൊണ്ടു പോവും. ഈ പതിവുകളൊക്കെയും ഞങ്ങൾ ആചാരമനുഷ്ഠിക്കുന്ന നിഷ്ഠയോടെ ചെയ്യാറുണ്ടു്.
കാപ്പികുടി കഴിഞ്ഞു് വിശ്രമിക്കുമ്പോൾ പെട്ടെന്നു് ഓർത്തെടുത്തതു് പോലെ രാമൻ ചോദിച്ചു,
‘എഡാ, നമ്മുടെ കോളേജിൽ പഠിച്ചിരുന്ന റോബർട്ടിനെ ഓർമ്മയുണ്ടോ?’
‘ഏതു് റോബർട്ട്?’ ആ ചോദ്യം എന്റെ മുഖത്തു് തെളിഞ്ഞു വന്നെന്നു് തോന്നുന്നു.
അവൻ ഒന്നു രണ്ടു് കാര്യങ്ങൾ കൂടി പറഞ്ഞു് എന്റെ ഓർമ്മയിലേക്കു് വെളിച്ചം വിതറാൻ ശ്രമം നടത്തി.
രണ്ടു് വട്ടം മിന്നിയ ശേഷം ഓർമ്മ തെളിഞ്ഞു.
‘ഓ… ആ ജാവ ബൈക്കിൽ വന്നിരുന്ന… മെക്കിലുണ്ടായിരുന്ന… അല്ലെ?’
‘ങാ… അവൻ തന്നെ… ഞാനിന്നാള് അളകാപുരിയിൽ വെച്ചു് കണ്ടു… ബാറിൽ നിന്നിറങ്ങുമ്പോൾ’
അതും പറഞ്ഞു് അവൻ ചിരിച്ചു.
രാമൻ ചിരിക്കുമ്പോൾ ഞാൻ റോബർട്ടിനെ കുറിച്ചു് ഓർക്കുകയായിരുന്നു. പ്ലേബോയ്… കൂട്ടത്തിൽ രേഷ്മയേയും. ഒരേ ബ്രാഞ്ചിൽ അല്ലായിരുന്നെങ്കിലും അവരുടെ കഥ സെൻസേഷണലായിരുന്നു. രേഷ്മയുടെ അപകടമരണം… റെയിൽ ക്രോസ്സ് ചെയ്യുമ്പോൾ ശ്രദ്ധിക്കാതിരിക്കാൻ തീരെ ചെറിയ കുട്ടിയൊന്നുമല്ലായിരുന്നല്ലോ അവൾ. കോളേജിൽ എല്ലാവരും ഒത്തുകൂടി ദുഃഖമാചരിച്ചതു് വീണ്ടും തെളിഞ്ഞു. അതൊരു സൂയിസൈഡ് ആയിരുന്നുവെന്നു് അന്നും ഇന്നും ഞാൻ വിശ്വസിക്കുന്നു. കുറഞ്ഞപക്ഷം ഞാനെങ്കിലും.
‘അവനിപ്പോ സിങ്കപ്പൂരിലാ… ഇവിടെ ഏതോ റിലേറ്റീവിന്റെ വെഡ്ഡിങ്ങിനു വന്നതാ…’
ഞാൻ തലയാട്ടിക്കൊണ്ടിരുന്നു.
എന്റെ നിശ്ശബ്ദത രാമൻ ശ്രദ്ധിച്ചിട്ടുണ്ടാവണം.
‘നീ… രേഷ്മയെ കുറിച്ചാവും ആലോചിക്കുന്നതു് അല്ലെ?’
ഞാൻ സമ്മതിച്ചു.
‘നിന്റെ തിയറി എനിക്കറിയാം… പക്ഷേ… റോബർട്ടിനെ ഒരു വലിയ… കുറ്റവാളിയായിട്ടൊന്നും കാണാൻ എനിക്കു് പറ്റില്ല… ഒരു അഫയറ് പോയെന്നും വെച്ചു് സൂയിസൈഡ് ചെയ്യേണ്ട കാര്യമൊന്നുമില്ല… എനിക്കതൊരു ആക്സിഡന്റ് ആയിട്ടേ തോന്നിയിട്ടുള്ളൂ’
അവൻ നയം വ്യക്തമാക്കി.
‘ഓരോരുത്തരും ബന്ധങ്ങൾക്കു് കൊടുക്കുന്ന വില ഒരേ പോലെ ആവില്ല’
‘അഫയർ പോയതൊന്നുമാവില്ല കാരണം… അതിന്റെ ഇൻസൾട്ട് താങ്ങാൻ അവൾക്കായിട്ടുണ്ടാവില്ല’
‘ഒരുപക്ഷേ, അതിനു് ശേഷമവൾക്കു് വീണ്ടുമൊരാളെ അത്രയ്ക്കും തീക്ഷ്ണമായി സ്നേഹിക്കാൻ കഴിയുമോന്നു് സംശയം തോന്നിട്ടുണ്ടാവും’
വർഷങ്ങൾക്കു മുൻപു് ഞാൻ നിരത്തിയ അതേ വാദങ്ങൾ വീണ്ടും അവതരിപ്പിക്കാനോ ആതേ മാനസികപീഡ ഒരിക്കൽ കൂടി അനുഭവിക്കാനോ തയ്യാറല്ലാത്തതു് കൊണ്ടു് മൗനം പാലിച്ചു.
പകരം ഒരിക്കലും എന്റെ ചിന്തകളിൽ കടന്നുവരാത്ത, തികച്ചും ബാലിശമായ, യുക്തിരഹിതമായൊരു വാചകം എങ്ങനെയോ നാവിൽ നിന്നും തെന്നി വീണു.
‘ഏയ്… അതു് ചിലപ്പോൾ… ഒരു ആക്സിഡന്റ് തന്നെ ആയിരിക്കും…’
എന്റെ അനുവാദമില്ലാതെ എന്നിൽ നിന്നും പുറത്തു് ചാടിയ ജീവനില്ലാത്ത വാക്കുകൾ.
ആ സമയം മറ്റൊരാളെക്കുറിച്ചു് ഞാൻ ആലോചിക്കുകയായിരുന്നു. വെറും ഒറ്റത്തവണ മാത്രം കണ്ടുമുട്ടിയ ഒരാളെക്കുറിച്ചു്.
എന്റെ ഉന്മേഷക്കുറവിനെക്കുറിച്ചുള്ള രാമന്റെ ഉത്കണ്ഠ ഞാൻ ശമിപ്പിച്ചതു് വൈകിട്ടു് മാനാഞ്ചിറ മൈതാനത്തിൽ വെച്ചാണു്. രണ്ടു് കാലങ്ങളിലായി നടന്ന രണ്ടു് സംഭവങ്ങൾ. പരസ്പരബന്ധമുണ്ടെന്നു് തോന്നിപ്പിക്കുന്നവ. അതാണെന്നെ ചിന്താവൃത്തത്തിലൂടെ നിർത്താതെ ചുറ്റിക്കുന്നതെന്ന കാര്യം അവനോടു് വെളിപ്പെടുത്തി. രണ്ടു് കാര്യങ്ങളും യാദൃച്ഛികമായിരിക്കാം. എങ്കിലും ചിലപ്പോൾ ചില യാദൃച്ഛികതകൾ നമ്മളെ തന്നെ ഉന്നം വെച്ചു് വരുന്നതായി തോന്നാറില്ലേ? യാദൃച്ഛികം എന്നു് തോന്നലിനെ അപ്രസക്തമാക്കും വിധം? ഉദാഹരണത്തിനു് വാട്സപ്പിൽ ഒരു ഗ്രൂപ്പിൽ, ചെറുപ്പക്കാർ ക്രീം കേക്ക് പരസ്പരം മുഖത്തു് വാരി തേച്ചു് പിറന്നാൾ ആഘോഷിക്കുന്നതിന്റെ വീഡിയോ കിട്ടുന്ന ദിവസം തന്നെ മറ്റൊരു ഗ്രൂപ്പിൽ, ഭിക്ഷ യാചിക്കുന്ന തെരുവു ബാലികയ്ക്കു് ഭക്ഷണപ്പൊതി കൊടുക്കുന്ന അജ്ഞാതനെക്കുറിച്ചുള്ള വീഡിയോ കിട്ടുക, രാഷ്ട്രീയ കൊലപാതകം നടത്തിയ ആൾക്കു് ജാമ്യം അനുവദിച്ചു കിട്ടി എന്ന വാർത്ത വായിച്ച ദിവസം തന്നെ തൊട്ടടുത്തുള്ള വീട്ടിൽ മോഷണശ്രമത്തിനിടയിൽ പിടിയിലായ ആളെ കെട്ടി വെച്ചു് നാട്ടുകാർ പൊതിരെ തല്ലുന്നതു് കാണേണ്ടി വരിക. പ്രത്യക്ഷത്തിൽ സംഭവങ്ങൾക്കൊന്നും ഒരു ബന്ധവും ഉണ്ടാവില്ല. എന്നാൽ രണ്ടും ഒരേ ദിവസം, ഒന്നിനു് പിറകെ ഒന്നു് എന്ന മട്ടിൽ മുന്നിലേക്കു് വരുമ്പോൾ, ഒക്കെയും അദൃശ്യനായ ആരോ ആസൂത്രണം ചെയ്തു് കാഴ്ച്ചയുടെ മുന്നിലേക്കു് നീക്കി വെച്ചതാണെന്ന പ്രതീതി ഉണ്ടാവുകയും ഞാൻ ചിന്താവൃത്തത്തിൽ കുടുങ്ങി പോവുകയും ചെയ്യും! ഇതൊക്കെയും എന്നെ മാത്രം തേടി വരുന്നതെന്തിനാണു്? അതേക്കുറിച്ചാവും പിന്നീടുള്ള ചിന്ത! പണ്ടു് മുതൽക്കെ പ്രഹേളികൾ പരിഹരിച്ചു് ശീലമില്ലാത്തതിനാൽ, ഇതൊക്കെയും ചെറിയ സ്വൈരക്കേടൊന്നുമല്ല എനിക്കു് സമ്മാനിക്കുന്നതു്.
എല്ലാ തവണയും ഞാൻ ബസ്സിലാണു് തിരുവനന്തപുരത്തു നിന്നും കോഴിക്കോടേക്കു വരിക. അതു് നമ്മുടെ ബസ്സ് സർവ്വീസിനോടുള്ള ആരാധനയോ, കോർപ്പറേഷൻ രക്ഷപെടണം എന്ന അനുകമ്പാപൂർണ്ണമായ പരിഗണനയോ കൊണ്ടല്ല. യാത്ര ട്രെയിനിലായാൽ, മനുഷ്യവാസം അധികമില്ലാത്ത പ്രദേശങ്ങളിലൂടെ കൂവി പാഞ്ഞു് അതങ്ങു് പോവും! ദൂരക്കാഴ്ച്ചകളാണധികവും. സൗന്ദര്യാരാധകനായ എനിക്കു് അതത്ര ഇഷ്ടമുള്ള കാര്യമല്ല. മനുഷ്യരെയൊക്കെ കണ്ടു്, ജീവിതം കുറച്ചു കൂടി അടുത്തു് കാണാൻ പാകത്തിൽ സഞ്ചരിക്കാനുള്ള സൗകര്യമുള്ളപ്പോൾ എന്തിനു് ആ അവസരം നഷ്ടപ്പെടുത്തണം? അങ്ങനെ ബസ്സിൽ കുലുങ്ങിയും കുതിച്ചും, ഇടയ്ക്കിടെയുള്ള മണിയടിശബ്ദം ആസ്വദിച്ചും ഞാൻ വരികയായിരുന്നു. അയാൾ എവിടെ നിന്നാണു് കയറിയതു്? ശരിക്കു് ഓർക്കുന്നില്ല. വിൻഡോയ്ക്കരികിലുള്ള സീറ്റിൽ ഇരുന്നതു് കൊണ്ടു് ബസ്സിലേക്കു് അയാൾ കയറുന്നതു് ഞാൻ കണ്ടിരുന്നു. തോളിൽ ഒരു ബാഗ്. മുണ്ടും ഷർട്ടും വേഷം. എന്റെ അരികിലാണു് അയാൾ വന്നു് ഇരിപ്പുറപ്പിച്ചതു്.
സഹയാത്രികൻ സന്തോഷവും ആത്മവിശ്വാസവുമുള്ള ഒരാളാണെന്നു് ഒറ്റനോട്ടത്തിൽ തോന്നി. വളരെ ഉത്സാഹപൂർവ്വം അയാളെന്നെ നോക്കി സൗഹാർദ്ദം നിറഞ്ഞ ചിരി സമ്മാനിച്ചു. ബസ്സിൽ കയറുന്ന ഒരാളുടെ പതിവു് ചേഷ്ടകളെനിക്കു് മനഃപാഠമാണു്. കഴുത്തും മുഖവും തൂവാലയെടുത്തു് തുടയ്ക്കുക, വെറുതെ വാച്ചിൽ സമയം നോക്കുക, ചുറ്റിലും ഇരിക്കുന്നവരെ അവർ കാണുന്നില്ല എന്ന ഭാവത്തിൽ നോക്കുക. അതൊക്കെയും ഞാനാസ്വദിക്കാറുണ്ടു്. എന്റെ അരികിലിരുന്ന ആൾ എന്നോടു് ചോദിക്കുമായിരിക്കും—‘ഇന്നു് ചെലപ്പൊ മഴ പെയ്യുമായിരിക്കും… അല്ലെ?’ അല്ലെങ്കിൽ ‘തൃശൂരിലേക്കായിരിക്കും?’ അതുമല്ലെങ്കിൽ ‘ഈ സ്പീഡില് പോയാൽ സമയത്തു് എത്തുവോ എന്തോ?’
ആത്മഗതം കലർന്ന അത്തരമൊരു പതിവു് ചോദ്യവും പ്രതീക്ഷിച്ചു് ഞാൻ ഇരുന്നു.
‘നമ്മളെങ്ങോട്ടാ?’
‘കോഴിക്കോടു്’
ഓ! അത്രയും ദൂരമോ?—ഭൂമിയുടെ മറുഭാഗത്തേക്കു് പോകുന്ന ഒരാളെ കാണുന്നതു് പോലെയൊരു ആശ്ചര്യം ആ മുഖത്തു് ഒരു നിമിഷം തെളിഞ്ഞു് മറഞ്ഞു.
ഇങ്ങോട്ടു് പരിചയപ്പെട്ട സ്ഥിതിക്കു് അങ്ങോട്ടും ലോഹ്യാന്വേഷണമുണ്ടായില്ലെങ്കിൽ അതു് മര്യാദകേടായി പോവും. പേരും, പോകേണ്ട ഇടവും ചോദിച്ചറിഞ്ഞു.
അയ്യപ്പൻ കോട്ടയത്തേക്കു് പോവുകയാണു്. അപ്പോൾ കോട്ടയം വരെ മിണ്ടിയും പറഞ്ഞും ഇരിക്കാൻ ഒരാളായി. ചിലരെ കണ്ടാൽ നമുക്കു് തോന്നാറില്ലേ ആ വ്യക്തി ഒരു നല്ല കേൾവിക്കാരനായിരിക്കുമെന്നു്? ചിലരെ കണ്ടാൽ നല്ലൊരു സംസാരപ്രിയനെന്നും. എന്നെ പലരും ഒരു കേൾവിക്കാരനാണെന്നു് ധരിച്ചു പോകുന്നതിന്റെ കാരണം ഇതുവരെയും പിടികിട്ടിയിട്ടില്ല. അടുക്കൽ വന്നിരിക്കുന്നതു് ഒരു കേൾവിക്കാരനാണെങ്കിൽ ഞാനും ആ വ്യക്തിയും വലഞ്ഞു പോവുകയേ ഉള്ളൂ. യാത്ര മുഴുവനും മൗനം പങ്കുവെയ്ക്കേണ്ടി വരും. അയ്യപ്പൻ അല്പനേരം കൊണ്ടു് തന്നെ എന്നെ, അയാളുടെ അടുത്ത സുഹൃത്തുക്കളുടെ പട്ടികയിൽ ചേർത്തെന്നു് തോന്നി. ദീർഘകാല പരിചയമുള്ളതു് പോലെ സംസാരിച്ചു തുടങ്ങി. പുള്ളിക്കാരൻ കോട്ടയത്തേക്കു് പോകുന്നതു് ഒരു പ്രത്യേക ദൗത്യത്തിനാണു്. ഒരു പഴയ സുഹൃത്തിനെ കൂട്ടിക്കൊണ്ടു വരുവാൻ. വെറുതെയല്ല, ഇനിയുള്ള കാലം കൂടെ താമസിപ്പിക്കുവാൻ! സ്വാഭാവികമായും അതെക്കുറിച്ചു് കൂടുതലറിയാനുള്ള ഉത്കണ്ഠയുണ്ടായി. ഒരു മനുഷ്യന്റെ ഉള്ളിലുള്ള കാഴ്ച്ചയുടെ ആഴവും പരവും വലിപ്പവുമൊന്നും ഒരിക്കലും പുറംകാഴ്ച്ചകൾക്കുണ്ടാവില്ലെന്നുറപ്പുള്ളതു് കൊണ്ടു് തൊട്ടടുത്തിരിക്കുന്ന മനുഷ്യനിലേക്കു് ഞാൻ ശ്രദ്ധ കേന്ദ്രീകരിച്ചു.
അയാൾ കഥ പറഞ്ഞു തുടങ്ങി.
‘ഞാനും അവനും സ്കൂള് തൊട്ടേ ഒന്നിച്ചു് പഠിച്ചതാ. പഠിക്കാൻ വല്ല്യ മെടുക്കനൊന്നുമല്ലായിരുന്നു—എന്നെ പോലെ! പക്ഷേ, അവനും ഞാനും എങ്ങനെയോ തട്ടീം മുട്ടീം എല്ലാം പാസ്സായി. ചെറിയൊരു ജോലി അവൻ ഒപ്പിക്കുകയും ചെയ്തു. എന്റെ അപ്പനു് ഒരു ബിസിനസ്സ് ഉണ്ടായിരുന്നതു് കൊണ്ടു് ഞാൻ അതുമായിട്ടങ്ങു് കൂടി. എന്റെ കല്ല്യാണം കഴിഞ്ഞിട്ടായിരുന്നു അവന്റെ കല്ല്യാണം. നല്ല ഒരു പെൺകുട്ടി. പക്ഷേ, അവനു് കുട്ടികളൊന്നും ഉണ്ടായില്ല. ഒരു ദിവസം എന്തോ ഒരു കാരണം കൊണ്ടു് അവന്റെ ഭാര്യ പിണങ്ങി പോയി. പിന്നീടാണറിഞ്ഞതു് അവള് അവൾടെ പഴയ ഇഷ്ടക്കാരന്റെ കൂടെ ഓടിപ്പോയതാണെന്നു്. ആ ദിവസം ഞാൻ മറക്കില്ല മാഷെ… അന്നു് വരെ കാണാത്ത ഒരുത്തനായിട്ടാണു് അവൻ എന്റെയടുത്തു് വന്നതു്. അങ്ങനെ അവൻ കരയണതു് ഞാൻ മുമ്പു് കണ്ടിട്ടേയില്ലായിരുന്നു. ഭാര്യ മരിച്ചു പോയാ മനുഷ്യര് കരഞ്ഞു പോവുമായിരിക്കും… പക്ഷേ, വേറൊരുത്തന്റെ കൂടെ പോയാ ആരെങ്കിലും കരയുവോ?! അവനെ സമാധാനിപ്പിക്കാൻ നോക്കിയെങ്കിലും നടന്നില്ല… കുറച്ചു് നാള് കഴിഞ്ഞപ്പോ…’
‘ഒരു മിനിട്ടേ…’ അതും പറഞ്ഞു് അയ്യപ്പൻ കഥ പറച്ചിൽ നിർത്തി, ബാഗ് തുറന്നു് ഒരു കുപ്പി എടുത്തു. ആ പ്ലാസ്റ്റിക് കുപ്പിയിലെ ജീരക വെള്ളം രണ്ടു് കവിൾ അകത്താക്കിയപ്പോൾ അയാളുടെ മുഖത്തു് മങ്ങിത്തുടങ്ങുകയായിരുന്ന ഉന്മേഷവെട്ടം വീണ്ടും തെളിഞ്ഞു വന്നു.
ഞാൻ ‘എന്നിട്ട്?’ എന്ന ഭാവത്തിൽ കാത്തിരുന്നു ആ നിമിഷമത്രയും.
പറഞ്ഞതിന്റെ പാതി പറയും പോലെ അയാൾ തുടർന്നു,
‘എന്നിട്ടു് എന്തു് പറയാനാ? അവനാള് ആകെയങ്ങു് മാറി. ആരോടും മിണ്ടാട്ടമില്ലാതായി. ജോലിക്കു് ശരിക്കു് പോകാതായി. താടീം മുടീം ഒക്കെ വളർത്തി ഒരു പ്രാന്തനെ പോലെ ആയി. അവളുമായിട്ടു് ബന്ധം വിടുവിച്ചിട്ടു് വേറൊരു കല്ല്യാണം കഴിക്കാൻ ഞാനവനോടു് പലവട്ടം പറഞ്ഞു നോക്കി. എവിടെ? കേക്കണ്ടെ? പിന്നെ ഒരു ദെവസം എന്റെ അടുത്തു് വന്നു പറഞ്ഞു—‘ഞാൻ കോട്ടത്തു് പോവാണൂ്’ എന്നു്. എന്തിനെന്നോ? പ്രാന്തിനു് ചികിത്സിക്കാൻ! ആരെങ്കിലും സ്വയം പ്രാന്താണെന്നും പറഞ്ഞു് പ്രാന്താശൂത്രീല് കേറി ചെല്ലുവോ? എന്നോടു്… കൂടെ ചെല്ലാൻ പറഞ്ഞതാ പക്ഷേ, ഞാൻ പോയില്ല. അവനോടു് പോണ്ടെന്നു് ഒരുപാടു് തവണ പറഞ്ഞതാ… കേട്ടില്ല… അങ്ങനെ പോയ അവനാ ഇന്നലെയെന്നെ വിളിച്ചു് ‘നിനക്കെന്നെ തിരികെ വിളിച്ചോണ്ടു് പോവാൻ പറ്റുവോടാ?’ എന്നു് ചോദിച്ചതു്. അവന്റെ പ്രാന്തൊക്കെ മാറിയെന്നാ പറയുന്നതു്. ഏതാണ്ടൊരു പത്തു് വർഷം അവൻ അവിടെ കെടന്നിട്ടുണ്ടാവും… ഇപ്പൊ… അവന്റെ സൂഖേടൊക്കെ മാറീട്ടൊണ്ടാവും…’
ആ ഒരു നിമിഷം ഓർമ്മകളെന്നെ മിന്നൽവേഗത്തിൽ വർഷങ്ങൾക്കു് പിന്നിലേക്കു് വലിച്ചെടുത്തു കൊണ്ടു പോയി. ഞാൻ ഞെട്ടിത്തരിച്ചു് ഇരുന്നു.
‘നിങ്ങടെ കൂട്ടുകാരന്റെ പേരെന്താ?’ ഞാൻ ആകാംക്ഷയോടെ ചോദിച്ചു.
‘ശിവൻ… ശിവൻകുട്ടി…’
‘അയാളെ വിട്ടു് പോയ… ആ പെണ്ണിന്റെ പേരു് പാർവ്വതി എന്നാണോ?’
‘ങാ… അതു് മാഷിനെങ്ങനെ അറിയാം?!’ അയ്യപ്പന്റെ കണ്ണുകൾ വിടർന്നു് വലുതാവുന്നതു് ഞാൻ കണ്ടില്ലെന്നു് നടിച്ചു.
എനിക്കറിയാമായിരുന്നു, അയ്യപ്പന്റെ ശിവൻകുട്ടിയെ എനിക്കും പരിചയമുണ്ടെന്നു് പറഞ്ഞാൽ അയാളെന്നല്ല, ആരും വിശ്വസിക്കുകയില്ലെന്നു്.
കഥ ഇത്രയും ആയപ്പോൾ രാമന്റെ ഉച്ചത്തിലുള്ള ചിന്ത കേട്ടു.
‘ശരിക്കും… ഈ ശിവൻകുട്ടീടെ ഭ്രാന്തു് മാറിയിട്ടുണ്ടാവുമോ?’
തൊട്ടടുത്ത നിമിഷം അവനാ ചോദ്യമുപേക്ഷിച്ചു് എന്നോടു് ചോദിച്ചു,
‘അല്ല, നിനക്കെങ്ങനെ ഈ ശിവൻകുട്ടിയെ അറിയാം?’
‘നീ ചെലപ്പൊ മറന്നു് കാണും… ഒരെട്ടു് പത്തു് വർഷം മുമ്പു് ഇതേ പോലെ നിന്നെ കാണാൻ വന്നപ്പോ ഈ പറയുന്ന ശിവൻകുട്ടിയെ ഞാൻ ബസ്സിൽ വെച്ചു് പരിചയപ്പെട്ടിരുന്നു… അന്നു് ഞാനയാളുടെ കാര്യം നിന്നോടു് പറഞ്ഞിട്ടുമുണ്ടു്… ശിവനും പാർവ്വതിയും… ശരിക്കും ജീവിതത്തിൽ പാർവ്വതി ശിവനെ ഉപേക്ഷിച്ചു് പോയി എന്നൊക്കെ പറഞ്ഞു് നീ ചിരിച്ചതൊക്കെ എനിക്കോർമ്മയുണ്ടു്’
രാമൻ മറവിരോഗം ബാധിച്ചവനെ പോലെ ഇരുന്നു. ആകാശത്തേക്കു് നോക്കി അതൊക്കെയും ഓർത്തെടുക്കാനൊരു ശ്രമം നടത്തുന്നതു് കണ്ടു. പിന്നീടു് ശ്രമം ഉപേക്ഷിച്ചു് എന്റെ നേർക്കു് നോക്കി. ആ സമയം ഒരു പന്തു് എന്റെ മുന്നിലേക്കു് ഉരുണ്ടു് വന്നതോ, ആ പന്തെടുത്തു് അതിന്റെ പിന്നാലെ വന്ന ചെറിയ പെൺകുട്ടിക്കു് ഞാൻ കൊടുത്തതോ അവൻ ശ്രദ്ധിച്ചിട്ടുണ്ടാവില്ല.
‘നീ… ശിവൻകുട്ടിയെ കണ്ട കാര്യം ഒന്നൂടെ പറഞ്ഞെ’
‘അപ്പോ ഞാനന്നു് പറഞ്ഞതൊന്നും നിനക്കിപ്പൊ ഓർമ്മയില്ല?’
‘പക്ഷേ, നീ മറന്നില്ലല്ലോ…’
മറക്കാതിരിക്കാൻ കാരണമുണ്ടു്. ശരീരത്തിന്റെ അസ്വാസ്ഥ്യങ്ങളെ പൊതുവെ ആരും അവഗണിക്കാറില്ല. എന്നാൽ മനസ്സിനൊരു പോറലു് സംഭവിച്ചാൽ അതു് മറയ്ക്കാനായിരിക്കും ആരും ആവതും ശ്രമിക്കുക. അങ്ങനെ ഒരു പൊതുചിന്തയ്ക്കു് നടുവിൽ ജനിച്ചു വളർന്ന ഒരാൾ സ്വന്തം അസുഖം തിരിച്ചറിയുകയും സ്വയം ചികിത്സയ്ക്കായി പോവുകയും ചെയ്യുക എന്നതു് അത്ര പെട്ടെന്നാരും മറന്നു പോകില്ലല്ലോ.
ശിവൻകുട്ടിയെ ഓർത്തെടുക്കാൻ ശ്രമിച്ചു. മഴത്തുള്ളികൾ നിറഞ്ഞ ചില്ലുജനാലയിലൂടെ കാണുന്ന കാഴ്ച്ച പോലെ അവ്യക്തമായിരുന്നു ആ മുഖം ഓർത്തെടുക്കാനാദ്യം ശ്രമിച്ചപ്പോൾ. മുഷിഞ്ഞ വസ്ത്രങ്ങൾക്കുള്ളിൽ കുടുങ്ങി പോയെന്നു് തോന്നിപ്പിക്കും വിധം മുഷിഞ്ഞ ഒരു രൂപം പതിയെ തെളിഞ്ഞു. അയാളൊരു ബാഗ് മുറുക്കെ ചേർത്തു് പിടിച്ചിരുന്നു. നരച്ചു തുടങ്ങിയ താടിരോമങ്ങളിലൂടെ വിരലോടിച്ചു്, ചിരിക്കുകയാണോ കരയുകയാണോ എന്നു് തിരിച്ചറിയാൻ കഴിയാത്ത വിധത്തിൽ സ്വന്തം കഥ പറയുന്ന ശിവൻകുട്ടിയെ എനിക്കു് കാണാനായി. അയാൾ ഇടയ്ക്കിടെ വിതുമ്പുകയും, എന്തൊക്കെയോ പിറുപിറുക്കുകയും ചെയ്യുന്നുണ്ടായിരുന്നു. മുൻവശത്തെ സീറ്റിന്റെ പിന്നിൽ അയാൾ നഖം കൊണ്ടു് പോറിയപ്പോൾ, അതു് കണ്ടു് ഞാൻ ഞെളിപിരി കൊണ്ടു. ഓർമ്മയിലേക്കു് മുഴുവൻ ശ്രദ്ധയും കൂട്ടിപ്പിടിച്ചു് വെച്ചിട്ടുപോലുമെനിക്കു് അയാളന്നു് പറഞ്ഞ ഏതാനും വാക്കുകൾ മാത്രമേ ഇഴപിരിച്ചെടുക്കാനായുള്ളൂ.
‘അവള് പോയി സാറെ… ഞാനവളെ ഒന്നു വഴക്കു പറഞ്ഞിട്ടു കൂടിയില്ല അറിയോ?… ആ എന്നെ ആണു് അവള് കളഞ്ഞിട്ടു് പോയതു്…’
ഒരാളെ വെറുക്കാൻ കൂടി കഴിയാതെ കഷ്ടപ്പെടുന്ന ഒരു മനുഷ്യനെ ആദ്യമായിട്ടായിരുന്നു ഞാൻ കാണുന്നതു്. വെറുക്കാനെത്ര എളുപ്പമാണു്! പത്രങ്ങളിൽ കാണുന്ന ഏതെങ്കിലും ദുഷിച്ച വാർത്തയിലെ ഒരു വരി വായിച്ചു നോക്കൂ. തൊട്ടടുത്ത നിമിഷം ആ വാർത്തയിലെ വ്യക്തിയെ നമ്മൾ പൂർണ്ണമായും വെറുത്തു കഴിഞ്ഞിരിക്കും. അത്രയ്ക്കും എളുപ്പമാണതു്. ലോകത്തിലേക്കും വെച്ചു് ഏറ്റവും ലാഘവത്തോടെ ചെയ്യാവുന്ന പ്രവൃത്തി. അരക്ഷണം പോലും വേണ്ട അതിനു്. പക്ഷേ, അതൊന്നും തന്നെ ശിവൻകുട്ടിയെന്ന മനുഷ്യനെ ബാധിച്ചിരുന്നില്ല. അയാൾ അതിനായി മനസ്സിനെ പാകപ്പെടുത്തുകയോ പരിശീലിപ്പിക്കുകയോ ചെയ്തിട്ടുണ്ടാവില്ല. വെറുപ്പിനെക്കുറിച്ചുള്ള ചിന്തകൾ തന്ന സ്വൈര്യക്കേടു് അയാളെ മറക്കാതിരിക്കാൻ കാരണമായി. പുരാണകഥയിലെ ശിവനും പാർവ്വതിയും എനിക്കു് ആ ദിവസം എന്നന്നേയ്ക്കുമായി നഷ്ടമായി. ജീവിതകഥയിലെ ശിവനെ ഞാനുള്ളിൽ പ്രതിഷ്ഠിച്ചു. കാലപ്പഴക്കം കൊണ്ടു് ക്ഷേത്രച്ചുവരുകൾ തകർന്നടിഞ്ഞിട്ടുണ്ടാവാം. പക്ഷേ, മൂർത്തി അപ്പോഴും, എപ്പോഴും അവിടെ ഉണ്ടായിരുന്നു. ക്രൂരതയുടെ പര്യായമുഖങ്ങൾ പത്രവാർത്തകളിൽ പ്രത്യക്ഷപ്പെടുമ്പോഴൊക്കെ, ഒരു മറുവാദമെന്ന നിലയിൽ അയാളുടെ മുഖം, ഓർമ്മകൾ എന്റെ മുന്നിലേക്കെടുത്തു വെച്ചു തന്നു. ഒരുപക്ഷേ, മനുഷ്യനിലുള്ള വിശ്വാസം നഷ്ടപ്പെടാതിരിക്കാൻ എന്റെ അബോധമനസ്സു് സ്വയം കണ്ടുപിടിച്ച ഒരു മാർഗ്ഗമാവാമതു്.
രാമനു് പിന്നെയും സംശയങ്ങളുണ്ടായിരുന്നു.
‘അല്ല… എനിക്കു് മനസ്സിലാവണില്ല… ഈ ഭ്രാന്തു് മാറിയോ ഇല്ലയോന്നു് അത്ര ഉറപ്പില്ലാത്ത ഒരുത്തനെ… എന്തിനാ ഒരാള് വീട്ടിലേക്കു് വിളിച്ചോണ്ടു് പോണതു്?’
ഭ്രാന്തില്ലെന്നു് സ്വയം നല്ല നിശ്ചയമുള്ള എനിക്കും രാമൻ ചോദിച്ച അതേ ചോദ്യം ചോദിക്കാനുണ്ടായിരുന്നു. മര്യാദയുടെ ലക്ഷ്മണരേഖ മറികടക്കാൻ മടിയുള്ളതു് കൊണ്ടു് ചോദിച്ചില്ല എന്നേയുള്ളൂ. എന്നാൽ ചോദിക്കാതെ തന്നെ അയ്യപ്പൻ അതിനു് ഉത്തരം തന്നിരുന്നു എന്നോർക്കുമ്പോൾ അതിശയം.
വീണ്ടും അയ്യപ്പന്റെ പതിഞ്ഞ ശബ്ദം കേട്ടു.
‘മാഷെ… പ്രസവത്തിലാണെന്റെ ഭാര്യ പോയതു്… ഒരു പെങ്കൊച്ചിനേം തന്നു് അവള് പോയപ്പോ… ഞാൻ ശിവൻകുട്ടിയെ ഓർത്തു… പണ്ടു് അവനെന്റെ മുന്നിലിരുന്നു് കരഞ്ഞതൊക്കെ ഓർത്തു… ആ ദെവസം പെട്ടെന്നു് ഞാനൊറ്റയ്ക്കായി പോയി. എന്റെ മോളെ… അമ്മയില്ലാത്ത ദുഖം അറിയിക്കാതെ… എനിക്കാവുന്ന പോലെ നന്നായി വളർത്തി… കെട്ടിച്ചും വിട്ടു… മോള് വീട്ടീന്നു് പോയപ്പോ… ഞാൻ വീണ്ടും ഒറ്റയ്ക്കായി… അപ്പോഴും ഞാൻ ശിവൻകുട്ടിയെ ഓർത്തു… മിന്നിയാന്നു് അവനെന്നെ വിളിച്ചപ്പോ ഞാൻ ശരിക്കും അന്തിച്ചു പോയി… ഇത്ര നാള് കഴിഞ്ഞിട്ടും…അവന്… എന്റെ ഫോൺ നമ്പറ് മാത്രേ ഓർമ്മയുള്ളൂ എന്നു് പറഞ്ഞപ്പോ… അവനെ ഞാനെങ്ങനെ ഉപേക്ഷിക്കും?… അവനെ വിളിച്ചോണ്ടു് വരണം… പഴയ കാലമൊക്കെ ഓർത്തെടുത്തു് പറയാൻ ഇപ്പൊ എന്തോ… എനിക്കു് അവൻ മാത്രേ ഉള്ളൂ എന്നൊരു തോന്നൽ… ആർക്കറിയാം… അവനു് അതൊക്കെ ഓർമ്മയുണ്ടോ എന്തോ…’
അതും പറഞ്ഞു് പുറത്തേക്കു് നോക്കി സ്വപ്നം കാണും പോലെ ഇരുന്ന മനുഷ്യനെ ഞാൻ വീണ്ടും കണ്ടു.
രാമൻ ഒന്നും മിണ്ടിയതേയില്ല. കഥ പറഞ്ഞു കഴിഞ്ഞപ്പോൾ പ്രപഞ്ചം മുഴുക്കെയും നിറഞ്ഞിരിക്കുന്ന ശൂന്യതയുടെ ഒരു ഭാഗമായി ഞാൻ മാറിക്കഴിഞ്ഞതു് പോലെ തോന്നി. പതിയെ ഞാൻ വൃത്തത്തിലൂടെ വീണ്ടും സഞ്ചരിച്ചു തുടങ്ങി.
കുറെ നേരം കഴിഞ്ഞു് രാമന്റെ ശബ്ദം, എന്നെ ചിന്താവൃത്തത്തിനു് പുറത്തേക്കു് വലിച്ചിട്ടു. താത്കാലികമോചനം.
‘നേരമിരുട്ടി… വാ… പോവാം…’
ഞാൻ മുകളിലേക്കു് നോക്കി.
വ്യക്തമായി കണ്ടു, അങ്ങിങ്ങായി തെളിഞ്ഞു് നില്ക്കുന്ന ചില നക്ഷത്രങ്ങൾ.
ഇല്ല, തീർത്തും ഇരുട്ടായിട്ടില്ല.
ഞങ്ങളിരുവരും എഴുന്നേറ്റു.

ജനനം: 1972 ൽ.
സ്വദേശം: തിരുവനന്തപുരം.
അമ്മ: പി. ലളിത.
അച്ഛൻ: എം. എൻ. ഹരിഹരൻ.
കെമിസ്ട്രിയിൽ ബിരുദവും, കമ്പ്യൂട്ടർ സയൻസിൽ ബിരുദാനന്തര ഡിപ്ലോമയും. സോഫ്റ്റ് വെയർ എഞ്ചിനീയർ. വായന, എഴുത്തു്, യാത്ര, ഭക്ഷണം എന്നിവയിൽ താത്പര്യം.
പുരസ്കാരം: നന്മ സി വി ശ്രീരാമൻ കഥാമത്സരം 2020 ഒന്നാം സമ്മാനം.
കഴിഞ്ഞ പത്തു് വർഷങ്ങളായി ന്യൂ സീലാന്റിൽ ഭാര്യയും മകനുമൊത്തു് താമസം.
ഭാര്യ: സിനു
മകൻ: നന്ദൻ
