
തീ പിടിച്ച
ചാണകപ്പച്ചയ്ക്കു മേലെ
ഇരിയ്ക്കുമ്പോൾ
ഇരയ്ക്കുള്ളിൽ
മിണുങ്ങും വായ്ക്കകത്തുള്ള
തണു സ്പർശം.
എല്ലു തട്ടിയുരയുന്ന
കല്ലുവെച്ച നുണയ്ക്കുള്ളിൽ
മരവിച്ചു് മയങ്ങുന്ന
വെറുമിറച്ചി.
മുഖം മൂടി കരിമ്പട
കിടത്തത്തിൽ
കിടുങ്ങുന്ന
മടുപ്പിന്റെ
ഉടൽ കോച്ചും
പടു താളം;
നെടു നിദ്ര;
നാട്യമുദ്ര.
ഒരു ചൂടും
ഉൾച്ചേരാതമരുന്ന
നിറധ്യാന നിർമമത
നിവരാത്ത വർത്തമാനം.
ശൈത്യമൊറ്റ ഋതുവായി,
തിളയ്ക്കാത്തൊരാത്മബോധം
പകപ്പാണിന്നകം മേനി
പതുങ്ങട്ടെ വെയിൽ ചീളു്.

ഒരു തെറ്റിന്നൂരാക്കുടുക്കിൽ പെട്ടൊരാൾ
നിരതെറ്റും നോട്ടം ഉഴറി വീങ്ങുമ്പോൾ
വിറച്ച ശ്വാസത്തിൻ ഗതി വായിൽ തള്ളി,
കുറുനാവിന്നുഴൽ കറ പതയുമ്പോൾ
കടും കെട്ടായേറ്റമുരഞ്ഞു് തേയുന്ന
ഉടൽ ഞരക്കങ്ങൾ, പിടി കിട്ടായ്മകൾ
മടുപ്പുടുത്തതിൻ ഒറ്റപ്പെടൽ,
മരുനടുക്കതിൽ തെന്നി കിനിയുമാർദ്രത;
ഒടുക്കമെത്തുന്ന ഉടലലിവുകൾ
നിലം തൊടാകാലും കുലുങ്ങും ചില്ലയും
ഇലകരിഞ്ഞതാമിതളലിവായി, കഴൽ നിലച്ചോടെ കരഞ്ഞിടറുന്നു,
ചിരം പലനിറച്ചേലായ് അടർന്നതീ വിധം…
അരിച്ചുകാർന്നതിൻ കടും പച്ചക്കാമ്പിന്നരികു
പറ്റുന്നൂ ഒരു നിലാ തുള്ളി…

തീണ്ടാരിപ്പെണ്ണുങ്ങൾ
തുണിയലക്കി
നടുനിവർത്തിക്കുഴയാറില്ല.
ലോലമേനി തഴുകി
പായൽ ജലം
ഇക്കിളിപൂണ്ടില്ല.
ആമ്പൽ വള്ളികൾക്കാശ്വസിപ്പിക്കാൻ
ഇല്ലായിരുന്നു
വെള്ളത്തെക്കീറിയ
കെെതോലമുള്ളിൽ കുരുങ്ങിയ
നീൾമുടി നാരുകൾ.
ആണുങ്ങൾ മാത്രമിറങ്ങുന്ന
ഒരൊറ്റക്കടവിന്റെ വിയർപ്പിൽ
ദേവശുദ്ധിയിൽ
നാണമില്ലാതെ
ഏകാന്തതയോടു് മല്ലിട്ടു
കളരിക്കുളം.
തന്നിലേക്കു് ചാഞ്ഞ
മരോട്ടിച്ചില്ലയിലെ കായ് വടിവു് കണ്ടു്
നെടുവീർപ്പിട്ടു എന്നും കടവു്.
ഒന്നു് കാണാനോ തൊടാനോ ആവാത്ത
വഷളൻ തോന്നലിൽ
കെെതരിച്ചു് നിന്നു…
നേരിട്ടു,
ഒറ്റ മുനപ്പുമായ്
തീ പടർത്തും
ഉൾവേവൊതുക്കി
കുളത്തെ, വിശുദ്ധിയെ
പരുക്കൻ നേരുമായ്;
ഉപമയെ വെന്നു
മരോട്ടിക്കായ്.

പകർപ്പെടുത്തു വായിക്കും
കൃത്യമാണെന്ന ഭാവത്തിൽ
മനോവിചാരങ്ങളപ്പടി
പുനർവായിക്കും ചെടിപ്പിലും
എങ്കിലും ചില വാക്കുകൾ
വിടവിളക്കിച്ചിരിച്ചിടും
അർത്ഥമില്ലായ്മയ്ക്കിടം നോക്കി
തെന്നിമാറിച്ചരിച്ചിടും
മൂർച്ചയുള്ള ചില്ലുകൾ, ചില
കോറലേല്പിച്ച മൃദുക്കളും
പാടി നീട്ടിയതൊക്കെയും
ചതച്ചിടും ധ്വനി മര്യാദയും
കേറി മാറിയ നിലകളിൽ
വാക്യമേകും വടുക്കളും
മനോനില തകർത്തിട്ടു്
പനിക്കും ചിഹ്നിതങ്ങളും
വിട്ടു പോയതൊക്കെയും
ചേർത്തുവെക്കുന്നടുപ്പിലും
പുകയാലൂതി കൺനീറ്റും
സ്വരം, ചപ്പില മാലകൾ
പകർപ്പെടുത്തു് വായിക്കും
കൃത്യമാണെന്ന ഭാവങ്ങൾ
പക കോറും പൊരുളാകും
സ്ഖലിതം മുദ്ര കൊണ്ടിടും…

സിംഹപ്രതാപം ബലപ്പെട്ട പല്ലു്
മെലിവും തൊഴുത്തുമില്ലാത്ത ഗജമദപ്പെരുമ
മടയിറങ്ങിയും മുരളും പുലിനഖകൂർമ
പുതപ്പിട്ടിരുളിൽ പതുങ്ങും കരടിക്കറുപ്പു്.
വാലാൽ കുരുക്കും വഷളനിളികൾ,
കൺദൈന്യമിറ്റി ചിതറുന്ന ഭീതികൾ,
ഓരിയിട്ടാർക്കും സൂത്രത്തരങ്ങൾ,
ചതിച്ചോരനക്കുന്ന ആർത്തികൾ.
പൊടുന്നനെ ചാടുന്നതേ തെന്നറിയാതെ
ഇരുളിൽ വിളറിയാന്തും ഇളം നിലാചീന്തു്
മനസ്സിന്റെ പച്ചിലക്കാടു് നീക്കി
അകപ്പെട്ട തെറ്റിൽ
ചെകിട്ടത്തടിച്ചു ചിവീടിൻ തുളക്കൽ.
വായ് പൊത്തി
കൺപൊത്തി നരിച്ചീർ കരിശ്ശീല.
അജ്ഞാതവാസമായ് കാന്താരതാരകം.

തൃശൂർ ജില്ലയിലെ കാക്കശ്ശേരി ദേശത്തു് ജനിച്ചു. ആനുകാലികങ്ങളിൽ കവിതകളും ലേഖനങ്ങളും എഴുതുന്നു. കാലടി ശ്രീശങ്കരാചാര്യ സംസ്കൃതസർവ്വകലാശാലയിൽ നിന്നു് എം. എ. മലയാളം ഒന്നാം റാങ്കോടെ വിജയിച്ചു. കലിക്കറ്റ് യൂണിവേഴ്സിറ്റിയിൽ നിന്നു് എം. ഫിൽ. ഇപ്പോൾ മലപ്പുറം ജില്ലയിലെ പാലപ്പെട്ടി ഗവ. സ്കൂളിൽ ഹയർസെക്കന്ററി മലയാളം അധ്യാപകൻ.
ചിത്രങ്ങൾ: വി. മോഹനൻ
