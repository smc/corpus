“ഇതു് കുന്നോളമുണ്ടല്ലോ?”
കിടപ്പുമുറിയിലേക്കു് കടന്നു വന്ന അയാൾ ചോദിച്ചു.
രണ്ടലമാരയിലേയും പഴയ തുണികളൊക്കെ വാരി കിടക്കയിലിട്ടിരിക്കുകയായിരുന്നു അവൾ. കുന്നിനു് കീഴെ വിഷണ്ണയായി താടിക്കു് കൈയ്യും കൊടുത്തിരിക്കുകയാണു്.
“ഒന്നെന്നെ സഹായിക്കുമോ… വേണ്ടതും വേണ്ടാത്തതും മാറ്റിവെക്കാൻ”
“ഓ ഇനി മാറ്റിവെക്കാനൊന്നും നിക്കണ്ട. മുഴുവനും ആർക്കെങ്കിലും കൊടുത്തേക്കു്. അല്ലെങ്കിലങ്ങു് കത്തിക്കാം” അപ്പോൾ രൂക്ഷമായി അവൾ അയാളെ നോക്കി.
“പറ്റുമെങ്കില് ചെയ്യ്. അല്ലെങ്കില് പോ… ഇതെനിക്കു് ഒറ്റക്കു് കഴിയാത്ത പണിയൊന്നുമല്ല.” അയാൾ കുന്നിനു് മറുവശത്തു് മുഖാമുഖമായി കട്ടിലിലിരുന്നു.
കിടപ്പുമുറിയിൽ രണ്ടു് പഴയ അലമാരകളാണുണ്ടായിരുന്നതു്. അതിലാണു് നാലു പേരുടെ വസ്ത്രങ്ങളൊക്കെയും. പഴയതും പുതിയതും ഇപ്പോൾ ഇട്ടു കൊണ്ടിരിക്കുന്നതുമെല്ലാം. ഇടയ്ക്കിടെ പൂങ്കോത അമ്മാ എന്നു് വിളിച്ചും കൊണ്ടു് വായ മുഴുവൻ മുറുക്കിച്ചുവപ്പിച്ചു വരും. ഊരിലേക്കു് പോകുന്നതിന്റെ തൊട്ടുമുമ്പാണു് വരവു്. ദീപാവലിക്കോ പൊങ്കലിനോ തൊട്ടുമുമ്പു്. നാലഞ്ചു് സാരി പൊതിഞ്ഞു കെട്ടി അവൾ പൂങ്കോതയ്ക്കു് കൊടുക്കും. “ഇവ്ടത്തെ സേട്ടന്റെ പേന്റും ഷർട്ടും എന്റെ മൂത്ത മകനു് സൈസ് കറക്ട്” എന്നും പറഞ്ഞു് പൂങ്കോത അടുത്ത നടപടിക്കു് കാത്തുനിൽക്കും. അവൾ അകത്തുചെന്നു് അയാളോടു് പറയും: “നിങ്ങളിടാത്ത ആ മഞ്ഞ ഷർട്ട്, അര ടൈറ്റായപേന്റ്… അതാ കോതക്കു് കൊടുക്കട്ടെ…?”
പതിവുപോലെ അയാളവളെ രൂക്ഷമായി നോക്കും. എന്നിട്ടു് കനം കുറച്ചു് പറയും: “എടീ… ഒരാൾക്കു് കൊടുക്കുമ്പോൾ നല്ലതു് കൊടുക്കണം. അല്ലാതെ കൊറേക്കാലം ഉട്ത്തു് തേഞ്ഞതല്ല”
“എന്നാ നിങ്ങളെല്ലാം അങ്ങു് പുഴുങ്ങി തിന്നു്. അല്ലേത്തന്നെ അലമാരേല് ഒരിഞ്ചു് സ്ഥലം ബാക്കീല്ല. ഒന്നൊഴിവാകട്ട്ന്നു് വിചാരിക്കുമ്പോ…”
പതിവുപോലെ അയാൾ നിശ്ശബ്ദനാവും.
“നീയെന്തെങ്കിലുമാക്കു്…” എന്നും പറഞ്ഞു് ഏതെങ്കിലും പുസ്തകത്തിൽ മുഖം മറക്കും.
“ഇതു് പുതിയതാണു്. കഴിഞ്ഞ വിഷൂനു് വാങ്ങിച്ചതു്. ഇതു് ഞാൻ ഒറ്റത്തവണയേ ഉടുത്തിട്ടുള്ളൂ വില കൂടിയതാണു് കേട്ടാ. ഈ പട്ടുസാരീടെ കോന്തലയ്ക്കു് കൊറച്ചു് ചായമിളകീന്നേ ഉള്ളൂ. നിനക്കുടുത്താൽ നല്ല രസമായിരിക്കും. അതിന്റെ ബ്ലൗസുണ്ടോന്നു നോക്കട്ടെ” എന്നിങ്ങനെ കൊടുക്കുന്ന സാധനത്തിന്റെ മഹത്വം വിളമ്പുമ്പോൾ” ആമാ ആമയെന്നു് പൂങ്കോത തലയിളക്കിക്കൊണ്ടിരിക്കും. അവൾ കൊടുത്ത സാരികളിൽ ഏതെങ്കിലും ഒന്നു പോലും പൂങ്കോത ഒരിക്കലെങ്കിലും ഉടുത്തതായി കണ്ടിട്ടില്ല. അതെന്തുകൊണ്ടാണെന്നു് അവൾ തന്നെ ഒരിക്കൽ പൂങ്കോതയോടു് ചോദിക്കുന്നതും കേട്ടു.
“അതു് ചേച്ചീ… എന്റെ സേട്ടന്റെ മോള് സുന്ദരി. നല്ല പഠിപ്പുള്ളവള്. അവർക്കു് കൊടുക്കും. പിന്നെ…”
ഊരിൽ പോയി വരുമ്പോളൊക്കെ ഒരു സഞ്ചി നിറയെ നിലക്കടല പൂങ്കോത അവൾക്കു് കൊണ്ടു കൊടുക്കും. ചിലപ്പോൾ പച്ച ചോളം. ഒന്നു രണ്ടു തവണ കടലമിഠായി. വിശേഷം പറഞ്ഞു് ചായേം കുടിച്ചങ്ങു പോവും. ആരെങ്കിലും എന്തെങ്കിലും കൊടുക്കുന്നതു് അവൾക്കിഷ്ടമാണു്. ആർക്കെങ്കിലും എന്തെങ്കിലും കൊടുക്കുന്നതും.
പഴന്തുണികളുടെ കുന്നിൽ നിന്നും ഒരു കുട്ടിയുടുപ്പെടുത്തു് നീട്ടി അവള് ചോദിച്ചു.
“ഇദോർമ്മയ്ണ്ടാ…”
ഞാനവളുടെ കൈയിലെ കുഞ്ഞുടുപ്പിലേക്കു് നോക്കി എന്തോ ഓർത്തു.
“അമ്മൂന്റതോ അതോ അപ്പൂന്റതോ?”
“പതിനെട്ടു് കൊല്ലായി. പറശ്ശിനീല് അമ്മൂസിനു് ചോറൂണു് കൊടുക്കാൻ കൊണ്ടുപോയപ്പോൾ ഉടുപ്പിച്ചതു്”. അതും പറഞ്ഞു് അവളാ കുഞ്ഞുടുപ്പു് മാറ്റിവെച്ചു.
“അമ്മു ഇന്നു് വിളിക്കില്ലേ…”
“ബുധനാഴ്ച, പെണ്ണു് വിളിക്കാണ്ട്… ഇന്നു് വീഡിയോ കാള് ചെയ്യുമ്പോ ഈ ഉടുപ്പു് കാണിച്ചു കൊടുത്തിട്ടു് ഓർമ്മയുണ്ടോന്നു് ചോദിക്കണം. ചോറൂണു് നേരത്തു് മൂത്രമൊഴിച്ചു് അലമ്പാക്കിയതു് ഓർമ്മയുണ്ടോന്നു്”
“വേണ്ട വേണ്ട. റൂമില് വേറെം പിള്ളേരു് കാണും. അവർക്കെടേല് പെണ്ണിനെ നാറ്റിക്കണ്ട”
“ആട പെണ്ണിനു് നാറ്റൂല്ല. ഈട അച്ഛനാ നാറ്റം”
അവൾ അടുത്ത ഉടുപ്പിലേക്കു കടന്നു. “ഇതു് അപ്പൂന്റെ, ഇതു് അമ്മൂസിന്റെ, ഇതെന്റെ, ഇതു് നിങ്ങടെ…” എന്നും പറഞ്ഞു് അവൾ തുണികളൊന്നായി മാറ്റി വെക്കാൻ തുടങ്ങി. അയാളുടെ രണ്ടു മൂന്നു് ഷർട്ടും പേന്റ്സും എടുത്തു് മാറ്റിവെക്കുന്നതിനിടയിൽ പറഞ്ഞു “ഇതു് ആ വെള്ളപ്പൊക്കന്നും ഭൂമികുലുക്കന്നും പറഞ്ഞു വരുന്നോർക്കു് കൊടുക്കാം”
“അതെന്താ നിന്റെ പൂങ്കോതയുടെ ചെക്കനതു് പാകമാവാതായോ…?” അയാൾ ചോദിച്ചു.
“ഇല്ല. ഇല്ല… നിങ്ങടെ കുപ്പായൊന്നും ഞാനോക്കു് കൊടുക്കൂല. മോനു് കൊടുക്കാനാണെന്നും പറഞ്ഞു് ഓള് വാങ്ങി കൊണ്ടുപോയ ആ ചോപ്പില് കറപ്പും വെളുപ്പും കള്ളിയുള്ള കുപ്പായമില്ലേ… അതു് ഓള് ഈയിട റോഡുപണിക്കിട്ടിനു്. ഞാൻ നേരിട്ടു് കണ്ടതാ…”
“അതിനെന്താ നീ കൊടുത്തതല്ലേ”
“അതോളെ മോനല്ലേ… ഓളെന്തിനാ അതിട്ടേ…”
ഏതോ ഗൂഢഭാവത്തിൽ അവൾ മുഖം കുനിച്ചപ്പോൾ അയാളുടെ ഉള്ളിൽ ഒരു ചിരി പൊട്ടി. ഇരുപത്തിയഞ്ചു് വർഷം പഴക്കമുള്ള ആകെ പിഞ്ഞിയ ഒരു കുപ്പായം അവളിപ്പോഴും രഹസ്യമായി സൂക്ഷിക്കുന്നുണ്ടു്. ഒരിക്കൽ അയാളെടുത്തു് ദൂരെക്കളയാൻ ഒരുങ്ങിയതാണു്. അവൾ വിട്ടില്ല. അവളെ പെണ്ണുകാണാൻ ചെന്നപ്പോ ഇട്ട കുപ്പായമാണത്രേ. അതിലാണു് വീണുപോയതെന്നു്. വിവാഹശേഷം വളരെ കുറച്ചു നാളെ പിരിഞ്ഞിരുന്നിട്ടുള്ളൂ. അയാളില്ലാത്ത ദിവസങ്ങളിൽ ആ കുപ്പായവും മുഖത്തിട്ടാണത്രെ ഉറങ്ങിയിരുന്നതു്.
“വട്ട്…”
അയാളതു് അൽപ്പം ഉറക്കെയാണു് പറഞ്ഞതു്.
“വട്ടന്നെ… ചിലവട്ട്കള് നല്ല സുഖമുള്ള വട്ട്കളാണു്. നിങ്ങളിങ്ങനെ കഥാന്നും പറഞ്ഞു് സങ്കൽപ്പിച്ചുണ്ടാക്കുന്നില്ലേ. അദെന്താ വട്ടല്ലേ…”
അയാളതിനു് മറുപടിയൊന്നും പറഞ്ഞില്ല. അവളാവട്ടെ കൊടുത്തു് ഒഴിവാക്കാനുള്ള കൂട്ടത്തിൽ തുണികൾ പെറുക്കി അടുക്കി വെച്ചു കൊണ്ടിരുന്നു.
“ഈ ചെക്കനെത്രയാ പേന്റും ഷർട്ടും. പെണ്ണിനു പോലും ഇത്രേം ഡ്രസ്സെടുട്ത്തിട്ടില്ല. അല്ലേലും പെണ്ണു് നിങ്ങളെപ്പോലയാണല്ലോ. ഫാഷനൊന്നും ഇഷ്ടല്ലല്ലാ. മാലേം വളേം ഇടാത്ത പെമ്പിള്ളേര് കാണുമോ. അതോണ്ടു് അച്ഛൻ രക്ഷപ്പെട്ടു്. സ്വർണ്ണത്തിനു് കാശു് കൊട്ക്കണ്ടല്ലാ…”
അയാൾ ആരോടെന്നില്ലാതെ ചിരിച്ചു.
“മോൾക്കു് വേണ്ടേല് അതു് ഭാര്യക്കു് വാങ്ങി കൊട്ക്വ. അതെങ്ങനെയാണു്. ഉള്ള പൊന്നും കൂടി കൊണ്ടുപോയെങ്ങനെ പണയം വെക്കാം എന്നു് ചിന്തിച്ചു് നടക്കുകയല്ലേ ഓരോരുത്തർ.”
അയാൾക്കു് പെട്ടെന്നു് പണയം വെച്ച പൊന്നിന്റെ കാര്യങ്ങൾ ഓർമ്മ വന്നു. ബാങ്കിൽ നിന്നു വന്ന കടലാസുകൾ അട്ടിയട്ടിയായി കിടക്കുന്നുണ്ടു്. വീടിന്റെ ലോണുണ്ടു്, സ്വർണ്ണലോൺ രണ്ടോ മൂന്നോ ഉണ്ടു്. വിളിച്ച കുറിയുടെ കടലാസ് വേറെയും.
“അവനു് ഈ മാസം ശമ്പളം കിട്ടി തൊടങ്ങില്ലേ. കിട്ടിയ ഉടൻ പൈസ അയച്ചു തരാൻ നീ പറഞ്ഞിട്ടില്ലേ”
ചോദിച്ചതിനു് മറുപടി പറയാതെ അവൾ പെട്ടെന്നൊരു ഉടുപ്പെടുത്തുയർത്തി. കാലുറയും കൂപ്പായവും ഒന്നിച്ചു ചേർന്ന, നാട കൊണ്ടു് ബന്ധിക്കുന്ന ഒന്നു്. പതുപതുത്ത വെൽവെറ്റിന്റെ സുഖമുള്ള ഒന്നു്.
“എന്നെക്കുറിച്ചു് പണ്ടൊരു കഥയെഴ്തീട്ടില്ലേ. ആ കഥ ആകാശവാണീല് വായിച്ചപ്പോൾ കിട്ടിയ ചെക്ക് മാറീട്ടു് വാങ്ങിച്ചതു്. എനിക്കുമന്നു് ഒരു വെൽവെറ്റിന്റെ ബ്ലൗസെടുത്തു തന്നിരുന്നു. റെഡിമെയ്ഡ്”
“ഓ എനിക്കോർമ്മയില്ല”
“എങ്ങനെ ഓർമ്മയ്ണ്ടാവാനാ. ഇപ്പോ സേവ കുറച്ചധികമല്ലേ. സന്തോഷം വരുമ്പോ കുടി. സങ്കടം വരുമ്പോ കുടി. എന്റെ മുത്തപ്പാ ഇവറെ ഓർമ്മ മുഴുവനും അരിച്ചരിച്ചു പോയീന്നാ തോന്ന്ന്നതു്”
ഇതപ്പൂസിന്റതു്, ഇതമ്മൂസിന്റതു് എന്നുരുവിട്ടു കൊണ്ടു് അവൾ കുഞ്ഞുടുപ്പുകൾ ഓരോന്നായി അടുക്കി വെക്കാൻ തുടങ്ങി.
കട്ടിലിൽ നിറയെ പഴയ വസ്ത്രങ്ങളുടെ കൂനകൾ പ്രത്യക്ഷപ്പെട്ടു തുടങ്ങി.
“ഞാനിവിടെ ഒറ്റയ്ക്കു് കഷ്ടപ്പെടുന്നതു് കണ്ടില്ലേ. നിങ്ങക്കു് എനിക്കൊരു കട്ടനിട്ടു് തന്നൂടെ” അവൾ അയാളെ നോക്കി.
അവിടെ നിന്നും എഴുന്നേറ്റു പോവാനൊരു വഴി പരതുകയായിരുന്നു അയാളും. അവിടെത്തന്നെയിരുന്നാൽ പഴയ തുണിയുടെ മണമടിച്ചു് തുമ്മാൻ തുടങ്ങും. അയാൾ പതുക്കെ എഴുന്നേറ്റു.
അടുക്കളയിലേക്കു നടക്കുന്നതിനിടയിൽ, ഈ വീടു് കുറച്ചു് വലിയ വീടായിപ്പോയി എന്ന തോന്നലയാൾക്കുണ്ടായി. ഇടനാഴിയിലൂടെ നടക്കുമ്പോൾ വല്ലാത്തൊരേകാന്തത വന്നു് കൈ പിടിച്ചു. അല്ലേലും രണ്ടു പേർക്കു് മുഖത്തോടു മുഖം നോക്കിയിരിക്കാൻ എന്തിനാണു് ഇത്ര വലിയ വീടു്. നാടു് വിട്ടു് പോയ മക്കളൊന്നും തിരിച്ചു വരാൻ കൂട്ടാക്കാത്ത കാലമാണിതു്. വീടവർക്കു് വല്ലപ്പോഴും സന്ദർശിക്കാനുള്ള ഒരിടം മാത്രമായി തീരുന്നതിനെ പറ്റി ഈയിടെ ആരോ എഴുതിയതു് വായിച്ചതോർത്തു.
അവൾക്കിഷ്ടമുള്ള കടുപ്പത്തിൽ ചായ തിളക്കുമ്പോൾ കൊറിക്കാനെന്തെകിലുമുണ്ടോ എന്നയാൾ ഭരണി പരതി. അറിമുറുക്കു്, മിക്സ്ച്ചർ, കായ ഉപ്പേരി… മക്കൾക്കോ അതിഥികൾക്കോ വേണ്ടി തേങ്ങയും ശർക്കരയും ചേർത്തു് അവളിടയ്ക്കിടെ ഉണ്ടാക്കാറുള്ള ഉണ്ട… അങ്ങനെ എന്തൊക്കെയോ കൊണ്ടു് ഭരണികൾ എപ്പോഴും നിറഞ്ഞിരിക്കുന്നതാണു് പതിവു്. അടുക്കളയിൽ കയറുമ്പോഴൊക്കെ മക്കൾ ഭരണി തുറക്കും. അവൾ കാണാതെ അയാളും.
ഡബ്ബകളൊക്കെ കാലിയായിരുന്നു. താനതൊക്കെ വാങ്ങിച്ചിട്ടു് കുറച്ചായല്ലോ എന്നു് അയാൾക്കു് പെട്ടെന്നോർമ്മ വന്നു. അവൾ പറയുന്നതുപോലെ ഓർമ്മകൾ അലിഞ്ഞിലാതാവുകയാണോ.
രണ്ടു് ഗ്ലാസ് ചായയുമായി കിടപ്പുമുറിയിലേക്കു കടക്കുമ്പോൾ ഭംഗിയായി അടുക്കി വെച്ച തുണികളൊക്കെയും അവൾ അലമാരയിൽ തന്നെ തിരിച്ചു വെക്കുകയായിരുന്നു.
“ഇതെന്താടോ. കൊടുത്തൊഴിവാക്കാമെന്നു പറഞ്ഞിട്ടു് വീണ്ടും വാരി നിറക്കുകയാണോ…? നിന്റെയൊരു കാര്യം”
അയാൾ അവൾക്കു നേരെ ചായ ഗ്ലാസ് നീട്ടിക്കൊണ്ടു് ചോദിച്ചു.
“ഇല്ല.”
ഒന്നും കൊടുക്കുന്നില്ല. ആർക്കും കൊടുക്കുന്നില്ല”
അതും പറഞ്ഞു് മുഖമുയർത്തിയപ്പോൾ അവളുടെ കണ്ണുകൾ നിറഞ്ഞിരിക്കുന്നതു കണ്ടു.
“എന്തു പറ്റി…?” അയാൾ ചോദിച്ചു.
“ഒന്നും കൊടുക്കുന്നില്ല. ആർക്കും കൊടുക്കുന്നില്ല”
ചായ ഊതിയൂതി കുടിക്കുന്നതിനിടയിൽ അയാളെ നോക്കാതെ അവൾ പറഞ്ഞു.
“അലമാര നിറയെ എന്തൊക്കെയോ മണങ്ങൾ… അതു് ഞാൻ ആർക്കും കൊടുക്കില്ല.”

കണ്ണൂർ ജില്ലയിലെ വളപട്ടണത്തു് ജനനം. അച്ഛൻ കെ. നാരായണൻ, അമ്മ ടി. കാർത്യായനി. രാമവിലാസം എൽ. പി. സ്കൂൾ, വളപട്ടണം ഗവ: ഹൈസ്ക്കൂൾ, കണ്ണൂർ എസ്. എൻ. കോളേജ്, തലശ്ശേരി ബ്രണ്ണൻ കോളേജ്, മാനന്തവാടി ബി. എഡ് സെന്റർ എന്നിവിടങ്ങളിൽ വിദ്യാഭ്യാസം.
കണ്ണൂർ ആകാശവാണിയിൽ പ്രോഗ്രാം കോംപിയറായും, വിവിധ പ്രാദേശിക ചാനലുകളിൽ അവതാരകനായും പ്രവർത്തിച്ചിരുന്നു. കുറേക്കാലം സമാന്തര കോളേജുകളിൽ അധ്യാപകനായി. പ്രൊഫഷണൽ ഫോട്ടോഗ്രാഫറാണു്. അദൃശ്യനായ കോമാളി, തീ അണയുന്നില്ല, ബിനാമി, ഒരു ദീർഘദൂര ഓട്ടക്കാരന്റെ ജീവിതത്തിൽ നിന്നും, മായാ ജീവിതം, സമകാലം (കഥകൾ) സാമൂഹ്യപാഠം, മഴനനഞ്ഞ ശലഭം, പുളിമധുരം, ഭൂതത്താൻ കുന്നിൽ പൂ പറിക്കാൻ പോയ കുട്ടികൾ (ബാലസാഹിത്യം) ജീവിതത്തോടു ചേർത്തുവെച്ച ചില കാര്യങ്ങൾ (അനുഭവം, ഓർമ്മ) ദൈവമുഖങ്ങൾ (നാടകം) ‘Ammu and the butterfly’ എന്ന പേരിൽ മഴനനഞ്ഞ ശലഭം ഇംഗ്ലീഷിലേക്കു് മൊഴിമാറ്റം ചെയ്യപ്പെട്ടിട്ടുണ്ടു്. അബുദാബി ശക്തി അവാർഡ് (1992) ഭാഷാ പുരസ്ക്കാരം (2003) പി. ടി. ഭാസ്ക്കര പണിക്കർ അവാർഡ് (2014) ഭീമ രജതജൂബിലി പ്രത്യേക പുരസ്ക്കാരം (2015) സാഹിത്യ അക്കാദമി അവാർഡ് (2018) പ്രാദേശിക ദൃശ്യമാധ്യമ പുരസ്ക്കാരം, കേരള സ്റ്റേറ്റ് ബയോഡൈവേർസിറ്റി ബോർഡിന്റെ ഫോട്ടോഗ്രാഫി അവാർഡ് (2017) കണ്ണാടി സാഹിത്യ പുരസ്ക്കാരം (2019) പ്രൊഫ. കേശവൻ വെള്ളിക്കുളങ്ങര ബാല ശാസ്ത്രസാഹിത്യ അവാർഡ് (2019) എന്നിവ ലഭിച്ചിട്ടുണ്ടു്. കേരള ഫോക് ലോർ അക്കാദമിയുടെ ഡോക്യുമെന്ററി പുരസ്ക്കാരം (2020). സമഗ്ര സംഭാവനയ്ക്കുള്ള സതീർത്ഥ്യ പുരസ്ക്കാരം (2020). കേരള സർക്കാർ പബ്ലിക്ക് റിലേഷൻ വകുപ്പിന്റെ മിഴിവു്-2021 ഷോർട്ട് ഫിലിം അവാർഡ് എന്നിവയും ലഭിച്ചിട്ടുണ്ടു്. ഷോർട്ട് ഫിലിം ഡോക്യുമെന്ററി വിഭാഗങ്ങളിലായി പതിനഞ്ചിലേറെ സിനിമകൾ ചെയ്തിട്ടുണ്ടു്.
ഭാര്യ: നിഷ, മക്കൾ: വൈഷ്ണവ്, നന്ദന
